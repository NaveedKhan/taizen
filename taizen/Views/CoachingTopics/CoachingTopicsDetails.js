import React, {Component} from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  StatusBar,
  TouchableOpacity,
  ScrollView, Alert
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {
  Container,
  Header,
  Tab,
  Tabs,
  TabHeading,
  Icon,Toast,
  Thumbnail, Spinner
} from 'native-base';
import firebase from 'firebase';
import {connect} from 'react-redux';
import {CustomHeaderWithText} from '../CommonViews';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

class CoachingTopicsDetails extends Component {
  constructor(props) {
    super(props);
    // ...
    this.state = {
      suggestedTopics:['Mindfulness','Comuunincation','Team-Building'],
      selectedTopicCategory: 'LEADERSHIP',
      tabColor:'#66c7c0',
      catData: [
        {
          catName: 'LEADERSHIP',
          topics: [
            'Networking',
            'Risk Taking',
            'Communication Skills',
            'dasdasd',
          ],
        },
        {
          catName: 'PERFORMANCE',
          topics: [
            'Networking2',
            'Risk Taking2',
            'Communication Skills2',
            'asdasd',
          ],
        },
        {
          catName: 'WELLBEING',
          topics: [
            'Networking3',
            'Risk Taking3',
            'Communication Skills3',
            'asdasas',
          ],
        },
      ],
      coachingTopicsArr:[],
      selectedCategoryTopics:[],
      isloading:false,
      isUpdatingUserData:false
    };
  }

  retriveCoachingTopics()
  {
    debugger;
    var firebaseConfig = {
      apiKey: "AIzaSyDlfb35etfsO24wlrghJ_dzpgIRSPhff94",
      authDomain: "taizen-cf195.firebaseapp.com",
      databaseURL: "https://taizen-cf195.firebaseio.com",
      projectId: "taizen-cf195",
      storageBucket: "taizen-cf195.appspot.com",
      messagingSenderId: "814929083198",
      appId: "1:814929083198:web:9465639048338ee3654a6f",
      measurementId: "G-3S2TR3Z0W1"
    };
    var me = this;
    // Initialize Firebase
    !firebase.apps.length ? firebase.initializeApp(firebaseConfig): firebase.app();
    debugger;
    var db2 = firebase.firestore();
    firebase.firestore().settings({experimentalForceLongPolling: true});
    var result = []
    db2.collection("coachingTopics")//"1a392I0XGAWkS6TjOBvLArg9psB2")
    .get()
    .then(function(querySnapshot) {
      debugger;  
      querySnapshot.forEach(function(doc) {
          debugger;
          var rec= {coachingTopicsId:doc.id,...doc.data()};
             result.push(rec)
            // doc.data() is never undefined for query doc snapshots
            ///console.log(doc.id, " => ", doc.data());
        });
        debugger;
me.setState({isloading:false,coachingTopicsArr:result});
    })
    .catch(function(error) {
      debugger;
      me.setState({isloading:false});
        console.log("Error getting documents: ", error);
    });

  }

  componentDidMount() {
   // const {catData} = this.state;

    this.setState({selectedTopicCategory: 'LEADERSHIP'});
//this.retriveCoachingTopics();
  }
  
  calcualtelength(textstring) {
    return textstring.length;
  }

  renderButtonRow(button1Name, button2Name) {
    debugger;
    var length1 = this.calcualtelength(button1Name) * 10;
    var length2 = this.calcualtelength(button2Name) * 10;
    var twoButtonsAArow = false;

    if (parseInt(length1 + length2) <= width * 0.99) {
      twoButtonsAArow = true;
    }

    if (twoButtonsAArow) {
      return this.renderTowButtonsARow(button1Name, button2Name);
    } else {
      if ((button2Name == '')) {
        return this.renderTopicButtons(button1Name);
      } else {
        return this.renderOneButtonsARow(button1Name, button2Name);
      }
    }
  }

  renderOneButtonsARow(button1Name, button2Name) {
    debugger;
    return  <View >
        {this.renderTopicButtons(button1Name)}
        {this.renderTopicButtons(button2Name)}
      </View>
    
  }

  renderTowButtonsARow(button1Name, button2Name) {
    return  <View style={{flexDirection: 'row'}}>
        {this.renderTopicButtons(button1Name)}
        {this.renderTopicButtons(button2Name)}
      </View>    
  }

  onPressTabButton(buttonName,tabNumber) {
    debugger;
    var tabContainerColor = tabNumber == 1 ? '#66c7c0' : tabNumber ==2 ? '#f9ac8b' : '#4bb868';
    this.setState({selectedTopicCategory: buttonName,tabColor:tabContainerColor});
  }

  renderTabButton(buttonName,tabNumber) {
    var backgroundcolor =
      buttonName == this.state.selectedTopicCategory ? 'white' : 'transparent';
    var tabWidth = width * 0.95;
    return (
      <TouchableOpacity
        onPress={() => this.onPressTabButton(buttonName,tabNumber)}
        style={{
          borderRadius: height * 0.066 * 0.49,
          width: tabWidth * 0.33,
          height: height * 0.060,
          borderWidth: 0,
          borderColor: '#39393a',
          backgroundColor: backgroundcolor,
          shadowOpacity: 0.7,
          shadowRadius: 3,
          shadowOffset: {
            height: 1,
            width: 1,
          },
          //android
          // elevation: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text
          numberOfLines={2}
          style={{color: this.state.tabColor, fontFamily: 'Ariel', fontSize: 13,fontWeight:'900'}}>
          {buttonName}
        </Text>
      </TouchableOpacity>
    );
  }

MapSuggestedTopicsIntoButtons()
{
const {suggestedTopics} = this.state;
var numberOfRows = Math.ceil(suggestedTopics.length / 2);
      var numberArray = [];
      for (var j = 0; j < numberOfRows; j++) {
        numberArray.push(j);
      }
      return numberArray.map((num) => {
        var button1Text = suggestedTopics[parseInt(num * 2 + 0)];
        var button2Text =
          parseInt(num * 2 + 1) == suggestedTopics.length
            ? ''
            : suggestedTopics[parseInt(num * 2 + 1)];
        return <View key={num}>
                {this.renderButtonRow(button1Text, button2Text)}
        </View> 
      });
}

  MapTopicsIntoButtons() {
    debugger;
    const {catData, selectedTopicCategory} = this.state;
    const {coachingTopicsArr} = this.props;
    var selectedCat = coachingTopicsArr.filter((aa) => aa.type.toLowerCase() == selectedTopicCategory.toLowerCase());
    if (selectedCat.length > 0) {
     return selectedCat.map(el=>
        <View style={{marginTop:10,padding:10}}>
          <Text style={{fontSize:14,fontWeight:'900'}}>{el.title}</Text>
          <Text style={{fontSize:13,fontWeight:'normal'}}>{el.detail}</Text>
        </View>
        );
     // var buttonsTextArray = selectedCat.topics;
      // var numberOfRows = Math.ceil(selectedCat.length / 2);
      // var numberArray = [];
      // for (var j = 0; j < numberOfRows; j++) {
      //   numberArray.push(j);
      // }
      // return numberArray.map((num) => {
      //   var button1Text = selectedCat[parseInt(num * 2 + 0)].title;
      //   var button2Text =
      //     parseInt(num * 2 + 1) == selectedCat.length
      //       ? ''
      //       : selectedCat[parseInt(num * 2 + 1)].title;
      //   return <View key={num}>
      //           {this.renderButtonRow(button1Text, button2Text)}
      //   </View> 
      // });
    }
  }

  onPressTopicButton(topicname)
  {
    debugger;
   const  {selectedCategoryTopics} =  this.state;
   var newList = selectedCategoryTopics;
   var item = selectedCategoryTopics.find(aa=>aa == topicname);
  
   if(item)
   {
     newList= newList.filter(aa=>aa != topicname)
   }else
   {
     if(newList.length <5)
         newList.push(topicname);
         else
         Toast.show({
          text: 'Limit Exceed!',
          buttonText: 'Okay',
          position: 'bottom',
          type: 'danger',
          duration: 2000,
        });
   // newList = selectedCategoryTopics;
   }

   if(newList.length > 5)
   {
   Toast.show({
        text: 'Limit Exceed!',
        buttonText: 'Okay',
        position: 'bottom',
        type: 'danger',
        duration: 2000,
      });
     // this.setState({selectedCategoryTopics:selectedCategoryTopics});
   }else
   {
     this.setState({selectedCategoryTopics:newList});
   }
  }

  renderTopicButtons(buttonName) {
    debugger;
   // var length = this.calcualtelength(buttonName) * 14;
   const {selectedCategoryTopics} = this.state;
   var isSelected = selectedCategoryTopics.includes(buttonName);
    return buttonName == '' ? (
      <View />
    ) : (
      <TouchableOpacity
      onPress={()=> this.onPressTopicButton(buttonName)}
        style={{
          margin: 5,
      //    width: length,
          paddingLeft:10,paddingRight:10,
          height: height * 0.06,
          borderWidth: 1,
          borderRadius: 3,
          borderColor: isSelected ? 'black':'gray',
          backgroundColor: 'transparent',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text
          numberOfLines={2}
          style={{color: isSelected ? 'black':'gray', fontFamily: 'Ariel', fontSize: 13}}>
          {buttonName}
        </Text>
      </TouchableOpacity>
    );
  }
  renderDoneButton(buttonText)
  {
    return this.state.isUpdatingUserData ? <Spinner style={{alignSelf:'center',margin:20}}/> :  <TouchableOpacity onPress={()=> this.updateUserDataOnFirebase()}
    style={{
      width: width * 0.95,
      height: height * 0.08,
      borderRadius:3,
      backgroundColor: '#4ab865',
      alignItems: 'center',
      justifyContent: 'center',
    
    }}>
    <Text style={{  color:'#fff',
      fontSize:18}}>{buttonText}</Text>
  </TouchableOpacity>
  }

  updateUserDataOnFirebase() {
var me=this;
me.setState({isUpdatingUserData:true});
    const {user} = this.props;
    const {selectedCategoryTopics} = this.state;
    const {id} = user;
    //const topicChosen = user.profileSteps ? (user.profileSteps.topicChosen?true:false):false;
    const assessment = user.profileSteps ? (user.profileSteps.assessment?true:false):false;
    const coachAssigned = user.profileSteps ? (user.profileSteps.coachAssigned?true:false):false;
    console.log('id is', id);
    var db = firebase.firestore();
    firebase.firestore().settings({experimentalForceLongPolling: true});
    var washingtonRef = db
      .collection('users')
      .doc(id);

    // Set the "capital" field of the city 'DC'
    return washingtonRef
      .update({
        profileSteps:
        {
          coachAssigned:coachAssigned,
          topicChosen:true,
          assessment:assessment
        },
        selectedTopics:selectedCategoryTopics
      })
      .then(function () {
        debugger;
        me.setState({isUpdatingUserData:false});
        //Alert.alert('Success','Session Booked Successfully');
        Actions.reset('onBoarding');
      })
      .catch(function (error) {
        // The document probably doesn't exist.
        debugger;
        me.setState({isUpdatingUserData: false});
        console.error('Error updating document: ', error);
      });
  }


  render() {
    return (
      <View
        style={{flex: 1, backgroundColor: 'transparent', alignItems: 'center'}}>
        <StatusBar backgroundColor={'#4db79b'} barStyle="light-content" />
        <CustomHeaderWithText text="Learn More About Topics" />
        <ScrollView scrollEnabled>
        {/* <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            margin: 10,
            marginTop: 20,
          }}>
          <Text
            style={{
              width: width * 0.9,
              textAlign: 'center',
              fontSize: 18,
              fontFamily: 'Ariel',
            }}>
            Based on your assessment, we suggest the following coacing topics to
            start with:
          </Text>
        </View> */}

        {/* <View style={{width: width * 0.95, borderWidth: 0,marginTop:15}}>
          {this.MapSuggestedTopicsIntoButtons()}
        </View> */}
        <View
          style={{
            flex: 1,
            borderWidth: 0,
            width: width * 0.95,
            marginTop: 20,
            alignItems: 'center',
          }}>
         

          <View
            style={{
              borderWidth: 0,
              height: height * 0.060,
              backgroundColor: this.state.tabColor,
              margin: 10,
              width: width * 0.95,
              borderRadius: height * 0.066 * 0.5,
              overflow: 'hidden',
            }}>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'transparent',
                height: height * 0.060,
                borderRadius: height * 0.066 * 0.5,
              }}>
              {this.renderTabButton('LEADERSHIP',1)}
              {this.renderTabButton('PERFORMANCE',2)}
              {this.renderTabButton('WELLBEING',3)}
            </View>
          </View>
          <View
            style={{
              alignContent: 'flex-start',
              alignItems: 'flex-start',
              width: width * 0.99,
            
            }}>
{
this.state.isloading ? <Spinner style={{alignSelf:'center',margin:20}}/>
: this.MapTopicsIntoButtons()
}
            
          </View>
        </View>
        </ScrollView>     
      </View>
    );
  }
}

//export default CoachingTopics;
const mapStateToProps = ({auth}) => {
  const {user,selectedCoach,upComingSessions} = auth;

  return {user,selectedCoach,upComingSessions};
};
export default connect(mapStateToProps, null)(CoachingTopicsDetails);

function chnagepage() {
  Actions.login();
}
