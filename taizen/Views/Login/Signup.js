import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  Alert,
} from 'react-native';
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Label,
  Spinner,
  Toast,
} from 'native-base';
import firebase from 'firebase';
import {connect} from 'react-redux';
import {loginUser, UpdateUserFormData,singupUser} from '../../actions';
import {Actions} from 'react-native-router-flux';
import styles from '../../Assets/Css/style';
import { CustomHeaderWithText } from '../CommonViews';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isloading: false,
      username: 'christie@gmail.com',
      password: '123456',
    };
    // ...
  }
  componentDidMount() {
    debugger;
    // this.state={username:'',password:''};
    // Initialize Firebase
    // setTimeout(chnagepage, 2000);
    //chnagepage();t
  }

  updateFormData(fieldname, value) {
    this.props.UpdateUserFormData({prop: fieldname, value: value});
  }

  getUserData(uid, me) {
    debugger;
    var db = firebase.firestore();
    firebase.firestore().settings({experimentalForceLongPolling: true});
    var docRef = db.collection('users').doc(uid);

    docRef
      .get()
      .then(function (doc) {
        Toast.show({
          text: 'data fetched!',
          buttonText: 'Okay',
          position: 'bottom',
          type: 'danger',
          duration: 4000,
        });
        debugger;
        if (doc.exists) {
          console.log('Document data:', doc.data());
          var data = doc.data();
          if (data.userType == 'Coach') {
            if (data.workCalendar) {
              me.GoToCoachDashboardPage(me);
            } else {
              me.GoToCoachOnBoardPage(me);
            }
          } else {
            me.GoToOnBoardPage(me);
          }
        } else {
          // doc.data() will be undefined in this case
          console.log('No such document!');
        }
      })
      .catch(function (error) {
        debugger;
        Toast.show({
          text: 'data fetched error!',
          buttonText: 'Okay',
          position: 'bottom',
          type: 'danger',
          duration: 4000,
        });
        console.log('Error getting document:', error);
      });
  }

  sendFirebaseCall() {
    debugger;
    var me = this;
    const {username, password} = this.state; // {username:"arsalankhan@gmail.com",password:"0346asdff"};
    this.setState({isloading: true});
    //this.addusrdata();
    firebase
      .auth()
      .signInWithEmailAndPassword(username, password)
      .then((user) => {
        debugger;
        const userid = user.user.uid;
        me.getUserData(userid, me);
        me.setState({isloading: false});
        // me.getUserInfo(userid,me)
      })
      .catch((error) => {
        debugger;
        console.log(error);
        me.setState({isloading: false});
        Toast.show({
          text: 'Wrong password!',
          buttonText: 'Okay',
          position: 'bottom',
          type: 'danger',
          duration: 4000,
        });
      });
  }

  onPressLoginButton() {
    //this.setState({isloading: true});
    debugger;
    const {username, password,firstName,lastName,jobTitle,DeviceTocken} = this.props;
    console.log('password is');
    var result = [];
    if (firstName == '') {
      result.push('First Name is required ');
    }

    if (lastName == '' ) {
      result.push('Last Name is required ');
    }

    if (jobTitle == '') {
      result.push('Job title is required ');
    }

    if (username == '') {
      result.push('Please provide email address! ');
    } else {
      if (!username.includes('@') || !username.includes('.')) {
        result.push('Please provide valid email address! ');
      }
    }

    if (password == '' || password.length < 6) {
      result.push('Please provide password with atleaset 6 characters! ');
    }
    if (result.length < 1) {
      console.log('validation donw');
      this.props.singupUser({email: username, password: password,firstName:firstName,lastName:lastName,jobTitle:jobTitle,DeviceTocken:DeviceTocken});
    } else {
      console.log('validation error is', result.length);
      Alert.alert('Attention!', result.toString());
    }
  }

  GoToCoachDashboardPage(me) {
    me.setState({isloading: false});
    Actions.coachDashboard();
  }

  GoToCoachOnBoardPage(me) {
    me.setState({isloading: false});
    Actions.coachOnBoarding();
  }

  GoToOnBoardPage(me) {
    me.setState({isloading: false});
    Actions.onBoarding();
  }

  renderCloseButton(onpressButton, buttontext) {
    return (
      <TouchableOpacity
        style={{
          // width: width * 0.23,
          //height: height * 0.045,
          borderBottomWidth: 0,
          //borderRadius: 5,
          margin: 30,
          marginLeft:10,
          borderBottomColor: '#4ab865',
          borderColor: 'gray',
          // marginRight: width * 0.1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#fff',
          alignSelf: 'flex-start',
        }}
        onPress={onpressButton}>
        <Text style={{fontSize: 14, color: '#4ab865', fontWeight: 'bold'}}>
          {buttontext}
        </Text>
      </TouchableOpacity>
    );
  }

  swapButtonAndSpinner() {
    return this.props.loading ? (
      <Spinner />
    ) : (
      <TouchableOpacity
        style={{
          width: width * 0.97,
          height: height * 0.08,
          borderWidth: 0,
          borderRadius: 3,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#4ab865',
        }}
        onPress={() => this.onPressLoginButton()}>
        <Text style={[styles.buttonText, {color: '#fff'}]}>SIGNUP</Text>
      </TouchableOpacity>
    );
  }

  onPasswordPress(text)
  {
    this.updateFormData('password', text)
    this.updateFormData('savedPassword', text)
  }

  render() {
    return (
      <Container>
        {this.renderCloseButton(()=> Actions.pop(),'Close')}
        <StatusBar translucent backgroundColor={'transparent'} barStyle="dark-content" />
        <Content style={{margin:10}}>
          <Form>
            <View style={{alignItems: 'flex-start',
            width:width*0.6,padding:10,paddingLeft:5,
            marginTop: height * 0.01,marginBottom:0}}>
              <Text
                style={{
                  fontSize: 20,
                  fontWeight: '900',
                  fontFamily: 'OpenSans',
                }}>
                Register with you work email
              </Text>
            </View>
            <Item floatingLabel style={{marginLeft: 5, marginRight: 5}}>
              <Label style={{fontSize: 14}}>First Name</Label>
              <Input
                value={this.props.firstName}
                onChangeText={(text) => this.updateFormData('firstName', text)}
                style={{marginTop: 5}}
              />
            </Item>
            <Item floatingLabel style={{marginLeft: 5, marginRight: 5}}>
              <Label style={{fontSize: 14}}>LastName</Label>
              <Input
                value={this.props.lastName}
                onChangeText={(text) => this.updateFormData('lastName', text)}
                style={{marginTop: 5}}
              />
            </Item>
            <Item floatingLabel style={{marginLeft: 5, marginRight: 5}}>
              <Label style={{fontSize: 14}}>Job Title</Label>
              <Input
                value={this.props.jobTitle}
                onChangeText={(text) => this.updateFormData('jobTitle', text)}
                style={{marginTop: 5}}
              />
            </Item>
            <Item floatingLabel style={{marginLeft: 5, marginRight: 5}}>
              <Label style={{fontSize: 14}}>Work Email</Label>
              <Input
                value={this.props.username}
                onChangeText={(text) => this.updateFormData('username', text)}
                style={{marginTop: 5}}
              />
            </Item>
            <Item floatingLabel style={{marginLeft: 5, marginRight: 5}}>
              <Label style={{fontSize: 14}}>Password</Label>
              <Input
                value={this.props.password}
                secureTextEntry={true}
                onChangeText={(text) => this.onPasswordPress(text)}
                style={{marginTop: 5}}
              />
            </Item>
            <View style={{alignItems: 'center', marginTop: height * 0.08}}>
              {this.swapButtonAndSpinner()}
            </View>
          </Form>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = ({auth}) => {
  const {username, password,firstName,lastName,jobTitle, error, loading, DeviceTocken, DeviceID} = auth;

  return {username, password, error, loading, DeviceTocken, DeviceID,firstName,lastName,jobTitle};
};
export default connect(mapStateToProps, {loginUser,singupUser, UpdateUserFormData})(
  Signup,
);

function chnagepage() {
  Actions.login();
}
