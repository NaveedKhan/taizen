import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  Alert,
} from 'react-native';
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Label,
  Spinner,
  Toast,
} from 'native-base';
import {CustomHeaderWithText} from '../CommonViews';
import firebase from 'firebase';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import {loginUser, UpdateUserFormData} from '../../actions';
import {Actions} from 'react-native-router-flux';
import styles from '../../Assets/Css/style';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
class ForgetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isloading: false,
      username: 'christie@gmail.com',
      password: '123456',
    };
    // ...
  }
  componentDidMount() {
    debugger;

  }

  renderCloseButton(onpressButton, buttontext) {
    return (
      <TouchableOpacity
        style={{
          // width: width * 0.23,
          //height: height * 0.045,
          borderBottomWidth: 0,
          //borderRadius: 5,
          margin: 30,
          marginLeft:10,
          borderBottomColor: '#4ab865',
          borderColor: 'gray',
          // marginRight: width * 0.1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#fff',
          alignSelf: 'flex-start',
        }}
        onPress={onpressButton}>
        <Text style={{fontSize: 14, color: '#4ab865'}}>
          {buttontext}
        </Text>
      </TouchableOpacity>
    );
  }

  updateFormData(fieldname, value) {
    this.props.UpdateUserFormData({prop: fieldname, value: value});
  }

  forgotPassword = (Email) => {
    var me = this;
    firebase.auth().sendPasswordResetEmail(Email)
      .then(function (user) {
        Alert.alert('Please check your inbox for reset password instructions!')
        me.updateFormData('loading', false)
        me.updateFormData('username', '')
        
        console.log('sex');
      }).catch(function (e) {
        me.updateFormData('loading', false)
        var err = String(e) == '' ? 'no error ' : String(e);
        Alert.alert('Error!',err)
        console.log('error',e)
      })
  }


  onPressLoginButton() {
    //this.setState({isloading: true});
    const {username} = this.props;

    var result = []
    if(username =='')
    {
         result.push('Please enter you email address');
    }
    else{
      if(!username.includes('@') || !username.includes('.'))
      {
        result.push('Please provide valid email address! ');
      }
    }

    if(result.length > 0)
    {
           Alert.alert('Attention!',result.toString())
    }else
    {
      this.updateFormData('loading', true)
      this.forgotPassword(username);
    }

    debugger;
   
   // 
  }

 
  swapButtonAndSpinner() {
    return this.props.loading ? (
      <Spinner />
    ) : (
      <TouchableOpacity
        style={{
          width: width * 0.97,
          height: height * 0.08,
          borderWidth: 0,
          borderRadius: 3,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#4ab865',
        }}
        onPress={() => this.onPressLoginButton()}>
        <Text style={[styles.buttonText, {color: '#fff'}]}>RECOVER</Text>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <Container>
          {this.renderCloseButton(()=> Actions.pop(),'Close')}
        <StatusBar translucent backgroundColor={'transparent'} barStyle="dark-content" />
        <Content style={{margin:10}}>
          <Form>
            <View style={{alignItems: 'center', marginTop: height * 0.08}}>
              <Text style={{fontSize: 20, fontWeight: '900'}}>
                Forgot your password?
              </Text>
            </View>
            <Item floatingLabel style={{marginLeft: 5, marginRight: 5}}>
              <Label style={{fontSize: 14}}>Email</Label>
              <Input
                value={this.props.username}
                onChangeText={(text) => this.updateFormData('username', text)}
                style={{marginTop: 5}}
              />
            </Item>
            <View style={{alignItems: 'center', marginTop: height * 0.08}}>
              {this.swapButtonAndSpinner()}
            </View>
          </Form>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = ({auth}) => {
  const {username,savedPassword, password,newPassword, error, loading, DeviceTocken, DeviceID,user} = auth;

  return {username,savedPassword, password,newPassword, error, loading, DeviceTocken, DeviceID,user};
};
export default connect(mapStateToProps, {loginUser, UpdateUserFormData})(ForgetPassword);

function chnagepage() {
  Actions.login();
}
