import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  Alert,
} from 'react-native';
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Label,
  Spinner,
  Toast,
} from 'native-base';
import firebase from 'firebase';
import {connect} from 'react-redux';
import {CustomHeaderWithText} from '../CommonViews';
import AsyncStorage from '@react-native-community/async-storage';
import {loginUser, UpdateUserFormData} from '../../actions';
import {Actions} from 'react-native-router-flux';
import styles from '../../Assets/Css/style';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isloading: false,
      username: 'christie@gmail.com',
      password: '123456',
    };
    // ...
  }
  componentDidMount() {
    debugger;
    // this.state={username:'',password:''};
    // Initialize Firebase
    // setTimeout(chnagepage, 2000);
    //chnagepage();t
  }

  storeData = async (key, value) => {
    // debugger;
    try {
      await AsyncStorage.setItem(key, value);
    } catch (e) {
      // saving error
    }
  };


  updateFormData(fieldname, value) {
    this.props.UpdateUserFormData({prop: fieldname, value: value});
  }

  getUserData(uid, me) {
    debugger;
    var db = firebase.firestore();
    firebase.firestore().settings({experimentalForceLongPolling: true});
    var docRef = db.collection('users').doc(uid);

    docRef
      .get()
      .then(function (doc) {
        Toast.show({
          text: 'data fetched!',
          buttonText: 'Okay',
          position: 'bottom',
          type: 'danger',
          duration: 4000,
        });
        debugger;
        if (doc.exists) {
          console.log('Document data:', doc.data());
          var data = doc.data();
          if (data.userType == 'Coach') {
            if (data.workCalendar) {
              me.GoToCoachDashboardPage(me);
            } else {
              me.GoToCoachOnBoardPage(me);
            }
          } else {
            me.GoToOnBoardPage(me);
          }
        } else {
          // doc.data() will be undefined in this case
          console.log('No such document!');
        }
      })
      .catch(function (error) {
        debugger;
        Toast.show({
          text: 'data fetched error!',
          buttonText: 'Okay',
          position: 'bottom',
          type: 'danger',
          duration: 4000,
        });
        console.log('Error getting document:', error);
      });
  }

  reauthenticate = (currentPassword) => {
    console.log('reauthenticate method called');
    var user = firebase.auth().currentUser;
    var cred = firebase.auth.EmailAuthProvider.credential(
        user.email, currentPassword);
    return user.reauthenticateWithCredential(cred);
  }


  changePassword = (currentPassword, newPassword) => {
  debugger;
  var me=this;
  const {username, password} = this.props;
  const SigninModel = {UserName: username, Password: newPassword};
  console.log('change password methid called');
    this.reauthenticate(currentPassword).then(() => {
      console.log('user reauthenticated');
      var user = firebase.auth().currentUser;
      user.updatePassword(newPassword).then(() => {
       // me.storeData('userData',)
     console.log('password updated');
       
        this.reauthenticate(newPassword).then(() => {
          console.log('user reauthenticated after the password change');
          me.storeData('userData', JSON.stringify(SigninModel));
           console.log("Password updated! now updating table of user");
          //me.updateUserDataOnFirebase(me);
          me.updateFormData('loading', false)
          me.updateFormData('oldPassword', '') 
          me.updateFormData('newPassword', '') 
         // Actions.coachOnBoarding(); 
         Alert.alert(
          'Attention!',
          'Your password is change successfuly',
          [
            {
              text: 'OK',
              onPress: () =>  Actions.pop(),
            },
          ],
          {cancelable: false},
        );
        }).catch((error) => { 
          console.log(error);
          me.updateFormData('loading', false)
        });

       
      
      }).catch((error) => { 
        console.log(error);
        me.updateFormData('loading', false)
      });
    }).catch((error) => { 
      console.log(error);
      me.updateFormData('loading', false)
    });
  }

  sendFirebaseCall() {
    debugger;
    var me = this;
    const {username, password} = this.state; // {username:"arsalankhan@gmail.com",password:"0346asdff"};
    this.setState({isloading: true});
    //this.addusrdata();
    firebase
      .auth()
      .signInWithEmailAndPassword(username, password)
      .then((user) => {
        debugger;
        const userid = user.user.uid;
        me.getUserData(userid, me);
        me.setState({isloading: false});
        // me.getUserInfo(userid,me)
      })
      .catch((error) => {
        debugger;
        console.log(error);
        me.setState({isloading: false});
        Toast.show({
          text: 'Wrong password!',
          buttonText: 'Okay',
          position: 'bottom',
          type: 'danger',
          duration: 4000,
        });
      });
  }


  updateUserDataOnFirebase(me) {

    const {user} = this.props;
    console.log('user is',user);
    const {id} = user;
    console.log('user id is', id);
    var db = firebase.firestore();
    firebase.firestore().settings({experimentalForceLongPolling: true});
    var washingtonRef = db
      .collection('users')
      .doc(id);

    // Set the "capital" field of the city 'DC'
    return washingtonRef
      .update({
        isNewPasswordSet:true,
      })
      .then(function () {
        debugger;
        //me.setState({fetchingSession: false});
      //  Actions.reset('coachDashboard');
      me.updateFormData('loading', false) 
      Actions.coachOnBoarding();  
      console.log('Document successfully updated!');
      })
      .catch(function (error) {
        // The document probably doesn't exist.
        debugger;
        //me.setState({fetchingSession: false});
        me.updateFormData('loading', false)
        console.error('Error updating document: ', error);
      });
  }
  onPressLoginButton() {
    //this.setState({isloading: true});
    const {username, password,newPassword,oldPassword,savedPassword} = this.props;
    // if (username == '' || password == '') {
    //   Alert.alert('Attention!','Please fill the fields!');
    // } else {
    //   this.props.loginUser({email: username, password: password});
    // }
    console.log(`button pressed usernaeme is ${username} password ${password}
    old is ${oldPassword} and new is ${newPassword}
    `);
   // return;
    var result = []
    if(oldPassword =='' || oldPassword.length < 6)
    {
      result.push('Please enter  your verified old password');
    }else
    {
      if(savedPassword != oldPassword)
      {
        result.push('Please enter correct old password');
      }
    }
   

    if(newPassword =='')
    {
         result.push('Please enter new password wih atleast 6 characters');
    }
    else{
      if(newPassword.length < 6)
      {
        result.push('New password must be  atleast 6 characters');
      }
    }

    if(result.length > 0)
    {
           Alert.alert('Attention!',result.toString())
    }else
    {
      this.updateFormData('loading', true)
      this.changePassword(oldPassword,newPassword);
    }

    debugger;
    console.log(`button pressed usernaeme is ${username} password ${password}`);
   
   // 
  }

  GoToCoachDashboardPage(me) {
    me.setState({isloading: false});
    Actions.coachDashboard();
  }

  GoToCoachOnBoardPage(me) {
    me.setState({isloading: false});
    Actions.coachOnBoarding();
  }

  GoToOnBoardPage(me) {
    me.setState({isloading: false});
    Actions.onBoarding();
  }
  swapButtonAndSpinner() {
    return this.props.loading ? (
      <Spinner />
    ) : (
      <TouchableOpacity
        style={{
          width: width * 0.97,
          height: height * 0.08,
          borderWidth: 0,
          borderRadius: 5,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#4ab865',
        }}
        onPress={() => this.onPressLoginButton()}>
        <Text style={[styles.buttonText, {color: '#fff'}]}>CHANGE</Text>
      </TouchableOpacity>
    );
  }
  render() {
    return (
      <Container>
        <CustomHeaderWithText leftIcon={true} headerStyle={{height:height*0.15}}/>
        {/* <StatusBar backgroundColor={'#fff'} barStyle="dark-content" /> */}
        <StatusBar translucent backgroundColor="transparent" />
        <Content style={{margin:10}}>
          <Form>
            <View style={{alignItems: 'center', marginTop: height * 0.08}}>
              <Text style={{fontSize: 18, fontWeight: '900'}}>
                Change Password
              </Text>
            </View>
           
            <Item floatingLabel style={{marginLeft: 5, marginRight: 5}}>
              <Label style={{fontSize: 14}}>Old Password</Label>
              <Input
                value={this.props.oldPassword}
                secureTextEntry={true}
                onChangeText={(text) => this.updateFormData('oldPassword', text)}
                style={{marginTop: 5}}
              />
            </Item>

            <Item floatingLabel style={{marginLeft: 5, marginRight: 5}}>
              <Label style={{fontSize: 14}}>New Password</Label>
              <Input
                value={this.props.newPassword}
                secureTextEntry={true}
                onChangeText={(text) => this.updateFormData('newPassword', text)}
                style={{marginTop: 5}}
              />
            </Item>
            <View style={{alignItems: 'center', marginTop: height * 0.08}}>
              {this.swapButtonAndSpinner()}
            </View>
          </Form>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = ({auth}) => {
  const {username,savedPassword, password,newPassword,oldPassword, error, loading, DeviceTocken, DeviceID,user} = auth;

  return {username,savedPassword, password,newPassword,oldPassword, error, loading, DeviceTocken, DeviceID,user};
};
export default connect(mapStateToProps, {loginUser, UpdateUserFormData})(ChangePassword);

function chnagepage() {
  Actions.login();
}
