import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  Alert,PermissionsAndroid
} from 'react-native';
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Label,
  Spinner,
  Thumbnail,
  Toast,
} from 'native-base';
import firebase from 'firebase';
import {connect} from 'react-redux';
import ImagePicker from 'react-native-image-picker';
import {CustomHeaderWithText} from '../CommonViews';
import AsyncStorage from '@react-native-community/async-storage';
import {loginUser, UpdateUserFormData} from '../../actions';
import {Actions} from 'react-native-router-flux';
import styles from '../../Assets/Css/style';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
    imageUploading: false,
     loading:false,
      username: this.props.user ? this.props.user.email: "",
      firstName: this.props.user ? this.props.user.firstName: "",
      lastName: this.props.user ? this.props.user.lastName: "",
      Image_Http_URL : this.props.user ?  {uri: this.props.user.picture} : '',
      DownloadLink:''
    };
    // ...
  }
  async componentDidMount() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: 'Tender.pk App Camera Permission',
          message:
            'Tender.pk  App needs access to your camera ' +
            'so you can take awesome pictures.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the camera');
      } else {
        console.log('Camera permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  storeData = async (key, value) => {
    // debugger;
    try {
      await AsyncStorage.setItem(key, value);
    } catch (e) {
      // saving error
    }
  };
  getUserData = (uid,me)=> {
    debugger;
    console.log('now gettting user detail again !');
    var db = firebase.firestore();
    firebase.firestore().settings({experimentalForceLongPolling: true});
    var docRef = db.collection('users').doc(uid);
  
    docRef
      .get()
      .then(function (doc) {
        debugger;
  
        if (doc.exists) {
          me.updateFormData('user',doc.data())
          me.setState({isloading: false});  
          Alert.alert('Success','Profile information updated successfully');
         // loginUserSucess(dispatch, doc.data(), SigninModel);
         console.log('user detail  successfully get!');
        } else {
          Toast.show({
            text: 'data fetched error!',
            buttonText: 'Okay',
            position: 'bottom',
            type: 'danger',
            duration: 4000,
          });
        }
      })
      .catch(function (error) {
        debugger;
        Toast.show({
          text: 'data fetched error!',
          buttonText: 'Okay',
          position: 'bottom',
          type: 'danger',
          duration: 4000,
        });
        console.log('Error getting document:', error);
       
      });
  }
  

  updateFormData(fieldname, value) {
    this.props.UpdateUserFormData({prop: fieldname, value: value});
  }



  updateUserImagePathOnFirebase(me,imageUrl) {

    const {user} = this.props;
   // console.log('user is',user);
    const {id} = user;
    console.log('update image path called');
    var db = firebase.firestore();
    firebase.firestore().settings({experimentalForceLongPolling: true});
    var washingtonRef = db
      .collection('users')
      .doc(id);

    // Set the "capital" field of the city 'DC'
    return washingtonRef
      .update({
        picture:imageUrl
       // firpistName:true,
      })
      .then(function () {
        debugger;  
        console.log('user picture value updated!');
        var updateduser = user;
        updateduser.picture = imageUrl;
        me.updateFormData('user',updateduser);
        me.setState({imageUploading:false});
        // me.getUserData(id,me);
 
      })
      .catch(function (error) {
        // The document probably doesn't exist.
        debugger;
        //me.setState({fetchingSession: false});
       // me.updateFormData('loading', false)
       me.setState({imageUploading: false});
        console.error('Error updating document: ', error);
      });
  }



  updateUserDataOnFirebase(me) {

    const {user} = this.props;
    console.log('user is',user);
    const {id} = user;
    console.log('user id is', id);
    var db = firebase.firestore();
    firebase.firestore().settings({experimentalForceLongPolling: true});
    var washingtonRef = db
      .collection('users')
      .doc(id);

    // Set the "capital" field of the city 'DC'
    return washingtonRef
      .update({
        firstName:me.state.firstName,
        lastName:me.state.lastName,
       // firpistName:true,
      })
      .then(function () {
        debugger;  
        console.log('user successfully updated!');
        me.getUserData(id,me);
 
      })
      .catch(function (error) {
        // The document probably doesn't exist.
        debugger;
        //me.setState({fetchingSession: false});
       // me.updateFormData('loading', false)
       me.setState({isloading: false});
        console.error('Error updating document: ', error);
      });
  }

  onPressLoginButton() {
    const {username, password,newPassword,oldPassword} = this.props;
    const {firstName,lastName}  = this.state;
    var result = []
if(firstName == '')
{
  result.push('Please enter First Name!')
}
if(lastName == '')
{
  result.push('Please enter Last Name!')
}
    console.log(`button pressed firstname is ${firstName} lastName ${lastName}
    `);
 
    if(result.length > 0)
    {
      Alert.alert('Missing Information',result.toString())
    }else
    {
      this.setState({isloading: true});
      this.updateUserDataOnFirebase(this);
    }
   
    debugger;
    console.log(`button pressed usernaeme is ${username} password ${password}`);
   
  }

  swapButtonAndSpinner() {
    return this.state.isloading ? (
      <Spinner />
    ) : (
      <TouchableOpacity
        style={{
          width: width * 0.97,
          height: height * 0.08,
          borderWidth: 0,
          borderRadius: 3,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#4ab865',
        }}
        onPress={() => this.onPressLoginButton()}>
        <Text style={[styles.buttonText, {color: '#fff'}]}>UPDATE</Text>
      </TouchableOpacity>
    );
  }

  async _pickImage () {

    this.setState({imageUploading:true});
   await this.selectPhotoTapped();
  }

  async selectPhotoTapped() {
    debugger;
    var me = this;
    const options = {
      quality: 0.5,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
        this.setState({imageUploading:false});
      }
      else if (response.error) {
        this.setState({imageUploading:false});
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        this.setState({imageUploading:false});
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };
         me.processImage(me,source.uri);
        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        // this.setState({

        //   ImageSource: source

        // });
      }
    });
  }

  async processImage(me,uri)
  {

    debugger;
    // this.handleImageUpload(pickerResult);
    //me.setState({isLoading:true})
     await this.uploadImageAsync(uri).then(response=>{
      console.log('image uploaded on firebase storage', response);
      me.updateUserImagePathOnFirebase(me,response);
      
     }).catch(error=>{
      console.log('failed', error);

      // me.showAlertError('image upload failed '+ error);
       me.setState({imageUploading:false});
     });
   // me.setState({isLoading:false})
    debugger; 

  }

  async  uploadImageAsync(uri) {
    // Why are we using XMLHttpRequest? See:
    var me=this;
    // https://github.com/expo/expo/issues/2402#issuecomment-443726662
    const blob = await new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.onload = function() {
        resolve(xhr.response);
      };
      xhr.onerror = function(e) {
        console.log(e);
        me.setState({imageUploading:false})
        me.showAlertError('Network Error')
        reject(new TypeError('Network request failed'));
      };
      xhr.responseType = 'blob';
      xhr.open('GET', uri, true);
      xhr.send(null);
    });
    let name = new Date().getTime() + "-media.png"
    const ref = firebase
      .storage()
      .ref()
      .child("assets/"+name);
    const snapshot = await ref.put(blob);
  
    // We're done with the blob, close and release it
    blob.close();
  
    return await snapshot.ref.getDownloadURL();
  }

renderEidPicButton()
{
  return this.state.imageUploading ? <Spinner style={{alignSelf:'center'}}/> :  <TouchableOpacity
  onPress={() =>  this._pickImage()}
  style={{
    padding:5,
    paddingRight:10,
    paddingLeft:10,
    height: height * 0.04,
    borderRadius: 2,
    borderWidth: 1,
    borderColor: '#c4c0c0',
    //marginTop: 5,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
   // alignSelf: 'center',
  }}>
  <Text style={{color: 'gray', fontSize: 13, fontWeight: 'bold'}}>
    {'CHANGE PICTURE'}
  </Text>
</TouchableOpacity>
}

  render() {
    const {user} = this.props;
    let Image_Http_URL = {uri: user.picture};
    return (
      <Container>
        <CustomHeaderWithText text="Edit Profile" leftIcon={true} headerStyle={{height:height*0.14}}/>
        {/* <StatusBar backgroundColor={'#fff'} barStyle="dark-content" /> */}
        <StatusBar translucent backgroundColor="transparent" />
        <Content style={{margin:10,backgroundColor:'transparent'}}>
          <Form style={{backgroundColor:'transparent',margin:10}}>
            <View style={{alignItems: 'center',backgroundColor:'transparent', 
            marginTop: height * 0.01,flexDirection:'row',width:width*1,marginBottom:10}}>
            <View style={{flex:3,backgroundColor:'transparent',alignItems:'center'}}>
            <Thumbnail large source={Image_Http_URL} />
            </View>
            <View style={{flex:7,backgroundColor:'transparent',justifyContent:'flex-start',alignItems:'flex-start'}}>
              {this.renderEidPicButton()}
            </View>
            </View>
           
            <Item floatingLabel style={{marginLeft: 5, marginRight: 5}}>
              <Label style={{fontSize: 15}}>First Name</Label>
              <Input
                value={this.state.firstName}
                onChangeText={(text) => this.setState({firstName:text})}
                style={{marginTop: 5}}
              />
            </Item>

            <Item floatingLabel style={{marginLeft: 5, marginRight: 5}}>
              <Label style={{fontSize: 15}}>Last Name</Label>
              <Input
                value={this.state.lastName}
                onChangeText={(text) => this.setState({lastName:text})}
                style={{marginTop: 5}}
              />
            </Item>

            <Item floatingLabel style={{marginLeft: 5, marginRight: 5}}>
              <Label style={{fontSize: 15}}>Email</Label>
              <Input
                value={this.state.username}
                editable={false}
                onChangeText={(text) => this.setState({username:text})}
                style={{marginTop: 5}}
              />
            </Item>
            <View style={{alignItems: 'center', marginTop: height * 0.08}}>
              {this.swapButtonAndSpinner()}
            </View>
          </Form>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = ({auth}) => {
  const {username, password,newPassword,oldPassword, error, loading, DeviceTocken, DeviceID,user} = auth;

  return {username, password,newPassword,oldPassword, error, loading, DeviceTocken, DeviceID,user};
};
export default connect(mapStateToProps, {loginUser, UpdateUserFormData})(EditProfile);

function chnagepage() {
  Actions.login();
}
