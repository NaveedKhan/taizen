import React, {Component} from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  Alert,
  Button,
} from 'react-native';
import ProgressBarAnimated from 'react-native-progress-bar-animated';
import {result} from '../../Assets/Source/report.json';
import {Actions} from 'react-native-router-flux';
import {
  Container,
  Header,
  Tab,
  Tabs,
  TabHeading,
  Icon,
  Toast,
  Thumbnail,
  Spinner,DeckSwiper
} from 'native-base';
import firebase from 'firebase';
import {connect} from 'react-redux';
import {CustomHeaderWithText} from '../CommonViews';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

class FirstResult extends Component {
  constructor(props) {
    super(props);
    // ...
    this.state = {
      resultArr:result,
      selectedPersonality:result[0],
      progress:83,
      progressWithOnComplete: 20,
      progressCustomized: 20,
      optionsData:[{id:0, isSelected:false,value:'Stronlgy Disagree'},{id:1, isSelected:false,value:'DisAgree'},{id:2, isSelected:false,value:'Neutral'}
    ,{id:3, isSelected:false,value:'Agree'},{id:4, isSelected:false,value:'Stronlgy Agree'}],
    cardArray:[{id:1, value:'card no 1',isSelected:false},{id:2, value:'card no 2',isSelected:false},{id:3, value:'card no 3',isSelected:false},{id:4, value:'card no 4',isSelected:false}]

    };
    this._deckSwiper = null;
  }



  componentDidMount() {
    // const {catData} = this.state;

    const {user} = this.props;
    const {resultArr} = this.state;
    let personalityCode = user.personalityCode;
    var resultPersonality = resultArr.find(aa=>aa.code == personalityCode);
    if(resultPersonality)
    {
      this.setState({selectedPersonality:resultPersonality})
    }
  }


  renderTabButton(buttonName, tabNumber) {
    var backgroundcolor =
      buttonName == this.state.selectedTopicCategory ? 'white' : 'transparent';
    var tabWidth = width * 0.95;
    return (
      <TouchableOpacity
        onPress={() => this.onPressTabButton(buttonName, tabNumber)}
        style={{
          borderRadius: height * 0.066 * 0.49,
          width: tabWidth * 0.33,
          height: height * 0.06,
          borderWidth: 0,
          borderColor: '#39393a',
          backgroundColor: backgroundcolor,
          shadowOpacity: 0.7,
          shadowRadius: 3,
          shadowOffset: {
            height: 1,
            width: 1,
          },
          //android
          // elevation: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text
          numberOfLines={2}
          style={{
            color: this.state.tabColor,
            fontFamily: 'Ariel',
            fontSize: 13,
            fontWeight: 'bold',
          }}>
          {buttonName}
        </Text>
      </TouchableOpacity>
    );
  }

  renderDoneButton(buttonText)
  {
    return this.state.isUpdatingUserData ? <Spinner style={{alignSelf:'center',margin:20}}/> :
      <TouchableOpacity onPress={()=> Actions.reset('onBoarding')}
    style={{
      width: width * 0.95,
      height: height * 0.08,
      borderRadius:3,
      backgroundColor: '#71a86d',
      alignItems: 'center',
      justifyContent: 'center',
    }}>
    <Text style={{  color:'#fff',
      fontSize:18}}>{buttonText}</Text>
  </TouchableOpacity>
  }

  renderReportButton(buttonText)
  {
    return this.state.isUpdatingUserData ? <Spinner style={{alignSelf:'center',margin:20}}/> :
      <TouchableOpacity onPress={()=> Actions.resultReport()}
    style={{
      width: width * 0.95,
      height: height * 0.06,
      borderRadius:3,
      backgroundColor: '#fc9999',
      alignItems: 'center',
      justifyContent: 'center',
    }}>
    <Text style={{  color:'#fff',
      fontSize:16}}>{buttonText}</Text>
  </TouchableOpacity>
  }

  renderCard()
  {
    const colorss = ['red', 'orange', 'yellow', 'purple', '#4ab865'];
    return  <View
    style={{
      backgroundColor: '#fff',
      width: width * 0.96,
      alignItems: 'center',
      padding: 10,
      borderRadius: 10,
      marginTop:height*0.12
    }}>
    {/* <Text style={{margin:10}}>In the past two months</Text> */}
    <Text style={{fontSize: 18, fontWeight: 'bold',textAlign:'center',marginTop:20}}>
      I have found it difficult to cope with stress
    </Text>

    <View style={{flexDirection: 'row', marginTop: 20}}>

        <View
          style={{
            flex: 4,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
     
     <View style={{ height: height * 0.18,width: height * 0.18,borderRadius:10,
            borderWidth:1,alignItems:'center',justifyContent:"center"}}>
            <TouchableOpacity
          onPress={()=>Alert.alert('sds')}
            style={{
              backgroundColor: 'orange',
              height: height * 0.16,
              width: height * 0.16,
              borderWidth: 0,
              borderRadius:10,
              margin:10,
              alignItems:'center',
              justifyContent:'center'
            }}>
               <Image source={require('../../Assets/mobile.png')} style={{resizeMode:'contain',height: height * 0.15,width: height * 0.15,alignSelf:'center'}}/>
            </TouchableOpacity>
            </View>
             <Text style={{fontSize:16,marginTop:5}}>The Pay</Text>
            </View>

            <View
          style={{
            flex: 4,
            alignItems: 'center',
            justifyContent: 'center',
          }}>

          
          <View style={{ height: height * 0.18,width: height * 0.18,borderRadius:10,
            borderWidth:1,alignItems:'center',justifyContent:"center"}}>
            <TouchableOpacity
          onPress={()=>Alert.alert('sds')}
            style={{
              backgroundColor:'#4db79b',
              height: height * 0.16,
              width: height * 0.16,
              borderWidth: 0,
              borderRadius:10,
              margin:10,
              alignItems:'center',
              justifyContent:'center'
            }}>
               <Image source={require('../../Assets/mobile.png')} style={{resizeMode:'contain',height: height * 0.15,width: height * 0.15,alignSelf:'center'}}/>
            </TouchableOpacity>
            </View>
             <Text style={{fontSize:16,marginTop:5}}>Creating/Building</Text>
            </View>

    </View>

   
  </View>
  }

  handleOptionsClick(el)
  {
    let {optionsData} = this.state;
    var newlist = optionsData;
    var id = el.id;
    newlist.forEach(function(items){
      items.isSelected = items.id == id
    });
     this.setState({optionsData:newlist});
  }

renderTextRow(header, title)
{
  return <View style={{backgroundColor:'#fff',height:height*0.12,width:width*1,borderBottomWidth:0.4, borderBottomColor:'gray', padding:10,justifyContent:'center'}}>
  <Text style={{fontSize:14,color:'gray'}}>
 {header}
  </Text>
  
  <Text style={{fontSize:16,fontWeight:'500',marginTop:5}}>
 {title}
  </Text>
  </View>
  
}

renderPlainTextRow(header, title)
{
  return <View style={{backgroundColor:'#fff',width:width*1,borderBottomWidth:0, borderBottomColor:'gray', padding:10,justifyContent:'center'}}>
  <Text style={{fontSize:15}}>
 {header.toUpperCase()}
  </Text>
  
  <Text  style={{fontSize:14,fontWeight:'500',marginTop:5,color:'gray',lineHeight:20}}>
 {title}
  </Text>
  </View>
  
}

renderPlainTextRowWithBottomBprder(header, title)
{
  return <View style={{backgroundColor:'#fff',width:width*1,borderBottomWidth:0.4, borderBottomColor:'gray',justifyContent:'center',paddingTop:10,paddingBottom:10}}>
  <Text style={{fontSize:14,marginLeft:10}}>
 {header}
  </Text>
  
  <Text  style={{fontSize:14,fontWeight:'500',marginTop:0,color:'gray',lineHeight:20,marginLeft:10,marginRight:10}}>
 {title}
  </Text>
  </View>
  
}

renderPlainTextRowWithoutSubheadWithBottomBorder(title)
{
  return <View style={{backgroundColor:'#fff',width:width*1,borderBottomWidth:0.4, borderBottomColor:'gray',justifyContent:'center',paddingTop:10,paddingBottom:10}}>

  
  <Text  style={{fontSize:14,fontWeight:'500',marginTop:0,color:'gray',lineHeight:20,marginLeft:10,marginRight:10}}>
 {title}
  </Text>
  </View>
  
}



renderProgreebarView(progressBarcolor,progressbarvalue, leftText,rightText,percentagevalue)
{
  return <View
  style={{
    backgroundColor: 'transparent',
    alignItems: 'center',
    width: width * 0.95,
    borderBottomWidth: 0,
    alignSelf:'center',
    marginBottom:7
  }}>
  <View
    style={{
      flexDirection: 'row',
      justifyContent: 'space-evenly',
      backgroundColor: 'transparent',
      alignSelf: 'stretch',
      paddingLeft:5,paddingRight:5
    }}>
    <View style={{flex: 3.3,justifyContent:'center',backgroundColor:'transparent'}}>
      <Text
        style={{
          backgroundColor: 'transparent',
          fontSize: 14,
          color: 'gray',
        }}>
        {leftText}
      </Text>
    </View>
    <View style={{flex: 3.3, justifyContent:'center'}}>
      <Text
        style={{
          backgroundColor: 'transparent',
          fontSize: 14,
          color: 'gray',
          textAlign:'center'
        }}>
        {'vs'}
      </Text>
    </View>
    <View style={{flex: 3.3}}>
      <Text
        style={{
          textAlign: 'right',
          backgroundColor: 'transparent',
          fontSize: 14,
          color: 'gray',
        }}>
        {rightText}
      </Text>
    </View>
  </View>
  <View style={{marginTop: 5, marginBottom: 10,backgroundColor:'#e9e9e9',borderRadius:20,overflow:'hidden',borderWidth:0}}>
    <ProgressBarAnimated
    
      width={width * 0.94}
      height={height*0.05}
      value={progressbarvalue}
      backgroundColorOnComplete="#6CC644"
     // maxValue={40}
      backgroundColor={progressBarcolor}
      onComplete={() => {
        Alert.alert('Hey!', 'onComplete event fired!');
      }}
    />
    <View style={{position:'absolute',justifyContent:'center',alignItems:'flex-start',width:width*0.92,backgroundColor:'transparent', height:height*0.05}}>
    <Text style={{color:'#fff',marginLeft:10}}>{percentagevalue}% </Text>
  </View>
  </View>
  
</View>
}
returnImage(personalitytype)
{
  debugger;
if(personalitytype.toLowerCase() == "The Friend".toLowerCase())
 {
  return require(`../../Assets/Personality/TheFriend.png`)
 }
 else if(personalitytype.toLowerCase() == "The Manager".toLowerCase())
 {
  return require(`../../Assets/Personality/TheManager.png`)
 }
 else if(personalitytype.toLowerCase() == "The Orderly".toLowerCase())
 {
  return require(`../../Assets/Personality/TheOrderly.png`)
 }
 else if(personalitytype.toLowerCase() == "The Entertainer".toLowerCase())
 {
  return require(`../../Assets/Personality/TheEntertainer.png`)
 }
 else if(personalitytype.toLowerCase() == "The Composer".toLowerCase())
 {
  return require(`../../Assets/Personality/TheComposer.png`)
 }
 else if(personalitytype.toLowerCase() == "The Conductor".toLowerCase())
 {
  return require(`../../Assets/Personality/TheConductor.png`)
 }

 else if(personalitytype.toLowerCase() == "The Troubleshooter".toLowerCase())
 {
  return require(`../../Assets/Personality/TheTroubleshooter.png`)
 }

 else if(personalitytype.toLowerCase() == "The Idealist".toLowerCase())
 {
  return require(`../../Assets/Personality/TheIdealist.png`)
 }

 else if(personalitytype.toLowerCase() == "The Magnet".toLowerCase())
 {
  return require(`../../Assets/Personality/TheMagnet.png`)
 }

 else if(personalitytype.toLowerCase() == "The Judge".toLowerCase())
 {
  return require(`../../Assets/Personality/TheJudge.png`)
 }

 else if(personalitytype.toLowerCase() == "The Energiser".toLowerCase())
 {
  return require(`../../Assets/Personality/TheEnergiser.png`)
 }
 
 else if(personalitytype.toLowerCase() == "The Mastermind".toLowerCase())
 {
  return require(`../../Assets/Personality/TheMastermind.png`)
 }
 
 else if(personalitytype.toLowerCase() == "The Captain".toLowerCase())
 {
  return require(`../../Assets/Personality/TheCaptain.png`)
 }
 
 else if(personalitytype.toLowerCase() == "The Creator".toLowerCase())
 {
  return require(`../../Assets/Personality/TheCreator.png`)
 }
  
 else if(personalitytype.toLowerCase() == "The Intellectual".toLowerCase())
 {
  return require(`../../Assets/Personality/TheIntellectual.png`)
 }
}

  render() {
    const barWidth = width * 0.95;
    const progressCustomStyles = {
      backgroundColor: 'red',
      borderRadius: 0,
      borderColor: 'orange',
    };
    const {selectedPersonality} = this.state;
    //var imageName = `../../Assets/Personality/${selectedPersonality.type}.png`;
   var imagename = this.returnImage(selectedPersonality.type);

    return (
      <View style={{flex: 1, backgroundColor: '#f7f7f7', alignItems: 'center'}}>
       <StatusBar translucent={true} barStyle="light-content" />
        {/* <CustomHeaderWithText leftIcon={true} text="COMPLETE LATER" /> */}
        
        <View
         style={{
               // backgroundColor: '#4db79b',
                alignItems: 'center',
              }}>
              <Image
              style={{width:width* 1,height:height*0.4}} source={imagename} />
              
              <View style={{position:'absolute',width:width*1}}>
              {/* <CustomHeaderTransparent headerStyle={{backgroundColor:'transparent'}} leftIcon={true} text={"The MasterMind".toUpperCase()} />  */}
              <View style={{flexDirection:'row'}}>
                <View style={{flex:2,paddingTop:20}}>
               <TouchableOpacity onPress={()=> Actions.pop()}>
               <Icon
            name="arrow-back"
            style={{color:'#fff', fontSize: 35}}
          />
              
               </TouchableOpacity>
              </View>

                <View style={{flex:6,paddingTop:20,alignItems:'center',justifyContent:'center'}}>
                    <Text style={{fontSize:20, color:'#fff',fontWeight:'900'}}>
                      {selectedPersonality.type.toUpperCase()}
                       </Text>
                </View>
                <View style={{flex:2,paddingTop:20}}>

                </View>
              </View>
              </View>

            </View>


        <ScrollView scrollEnabled>
          <View
            style={{
              flex: 1,
              borderWidth: 0,
              marginTop: -1,
              width: width * 1,
              alignItems: 'center',
              backgroundColor: 'transparent',
              justifyContent:'center'
            }}>


<View style={{borderWidth:0,padding:30,alignItems:'center',justifyContent:'center'}}>
<Text style={{fontSize:20,fontWeight:'900',textAlign:'center'}}>
YOUR RESULT
</Text>
</View>

{this.renderPlainTextRow('DESCRIPTION', selectedPersonality.description)}

<View style={{backgroundColor:'#fff',alignSelf:'stretch',marginTop:10,alignItems:'center',borderWidth:0}}>
          <Text style={{fontSize:15,margin:10}}>{'Big 5 persoality traits'.toUpperCase()}</Text>
          {this.renderProgreebarView('orange',86,'Extroverted','Interoverted',86)}
          {this.renderProgreebarView('red',50,'Extroverted','Interoverted',50)}
          {this.renderProgreebarView('#4ab865',70,'Extroverted','Interoverted',70)}
          {this.renderProgreebarView('blue',30,'Extroverted','Interoverted',30)}
 </View>



<View style={{backgroundColor:'#fff',alignSelf:'stretch',marginTop:10}}>
          <Text style={{fontSize:15,margin:10}}>{'HOW YOU MANAGE YOUR ENERGY'}</Text>
   {this.renderPlainTextRowWithoutSubheadWithBottomBorder(selectedPersonality.manageEnergy)}
 </View>

<View style={{backgroundColor:'#fff',alignSelf:'stretch',marginTop:10}}>
          <Text style={{fontSize:15,margin:10}}>{'Ennjoyment'}</Text>
   {this.renderPlainTextRowWithoutSubheadWithBottomBorder(selectedPersonality.enjoyment)}
 </View>

 <View style={{backgroundColor:'#fff',alignSelf:'stretch',marginTop:10}}>
          <Text style={{fontSize:15,margin:10}}>{'Process Information'}</Text>
   {this.renderPlainTextRowWithoutSubheadWithBottomBorder(selectedPersonality.processInformation)}
 </View>

 <View style={{backgroundColor:'#fff',alignSelf:'stretch',marginTop:10}}>
          <Text style={{fontSize:15,margin:10}}>{'Focus'}</Text>
   {this.renderPlainTextRowWithoutSubheadWithBottomBorder(selectedPersonality.focus)}
 </View>

 <View style={{backgroundColor:'#fff',alignSelf:'stretch',marginTop:10}}>
          <Text style={{fontSize:15,margin:10}}>{'Personal Values'}</Text>
   {this.renderPlainTextRowWithoutSubheadWithBottomBorder(selectedPersonality.personalValues)}
 </View>

 <View style={{backgroundColor:'#fff',alignSelf:'stretch',marginTop:10}}>
          <Text style={{fontSize:15,margin:10}}>{'Concern'}</Text>
   {this.renderPlainTextRowWithoutSubheadWithBottomBorder(selectedPersonality.concern)}
 </View>

 <View style={{backgroundColor:'#fff',alignSelf:'stretch',marginTop:10}}>
          <Text style={{fontSize:15,margin:10}}>{'OrganiseLife'}</Text>
   {this.renderPlainTextRowWithoutSubheadWithBottomBorder(selectedPersonality.organiseLife)}
 </View>

 <View style={{backgroundColor:'#fff',alignSelf:'stretch',marginTop:10}}>
          <Text style={{fontSize:15,margin:10}}>{'Preferences'}</Text>
   {this.renderPlainTextRowWithoutSubheadWithBottomBorder(selectedPersonality.preference)}
 </View>

 <View style={{backgroundColor:'#fff',alignSelf:'stretch',marginTop:10}}>
          <Text style={{fontSize:15,margin:10}}>{'Strengths'}</Text>
   {this.renderPlainTextRowWithoutSubheadWithBottomBorder(selectedPersonality.yourStrengths)}
 </View>

 <View style={{backgroundColor:'#fff',alignSelf:'stretch',marginTop:10}}>
          <Text style={{fontSize:15,margin:10}}>{'BestAt'}</Text>
   {this.renderPlainTextRowWithoutSubheadWithBottomBorder(selectedPersonality.youAreBest)}
 </View>

 <View style={{backgroundColor:'#fff',alignSelf:'stretch',marginTop:10}}>
          <Text style={{fontSize:15,margin:10}}>{'Oppertunity'}</Text>
   {this.renderPlainTextRowWithoutSubheadWithBottomBorder(selectedPersonality.opportunities)}
 </View>

 <View style={{backgroundColor:'#fff',alignSelf:'stretch',marginTop:10}}>
          <Text style={{fontSize:15,margin:10}}>{'Pitfall'}</Text>
   {this.renderPlainTextRowWithoutSubheadWithBottomBorder(selectedPersonality.pitfall)}
 </View>

 <View style={{backgroundColor:'#fff',alignSelf:'stretch',marginTop:10}}>
          <Text style={{fontSize:15,margin:10}}>{'Pitfall'}</Text>
   {this.renderPlainTextRowWithoutSubheadWithBottomBorder(selectedPersonality.pitfall)}
 </View>

 <View style={{backgroundColor:'#fff',alignSelf:'stretch',marginTop:10}}>
          <Text style={{fontSize:15,margin:10}}>{'KnowingYourself'}</Text>
   {this.renderPlainTextRowWithoutSubheadWithBottomBorder(selectedPersonality.knowingYourself)}
 </View>

 <View style={{backgroundColor:'#fff',alignSelf:'stretch',marginTop:10}}>
          <Text style={{fontSize:15,margin:10}}>{'CoreValues'}</Text>
   {this.renderPlainTextRowWithoutSubheadWithBottomBorder(selectedPersonality.coreValues)}
 </View>

 <View style={{backgroundColor:'#fff',alignSelf:'stretch',marginTop:10}}>
          <Text style={{fontSize:15,margin:10}}>{'Motivators'}</Text>
   {this.renderPlainTextRowWithoutSubheadWithBottomBorder(selectedPersonality.mainMotivators)}
 </View>

 <View style={{backgroundColor:'#fff',alignSelf:'stretch',marginTop:10}}>
          <Text style={{fontSize:15,margin:10}}>{'Relationship'}</Text>
   {this.renderPlainTextRowWithoutSubheadWithBottomBorder(selectedPersonality.relationshipStyle)}
 </View>

 <View style={{backgroundColor:'#fff',alignSelf:'stretch',marginTop:10}}>
          <Text style={{fontSize:15,margin:10}}>{'Communication'}</Text>
   {this.renderPlainTextRowWithoutSubheadWithBottomBorder(selectedPersonality.communicationStyle)}
 </View>

 <View style={{backgroundColor:'#fff',alignSelf:'stretch',marginTop:10}}>
          <Text style={{fontSize:15,margin:10}}>{'ConnectBy'}</Text>
   {this.renderPlainTextRowWithoutSubheadWithBottomBorder(selectedPersonality.connectBy)}
 </View>


 <View style={{backgroundColor:'#fff',alignSelf:'stretch',marginTop:10}}>
          <Text style={{fontSize:15,margin:10}}>{'Communicate'}</Text>
   {this.renderPlainTextRowWithoutSubheadWithBottomBorder(selectedPersonality.communicateBy)}
 </View>


 <View style={{backgroundColor:'#fff',alignSelf:'stretch',marginTop:10}}>
          <Text style={{fontSize:15,margin:10}}>{'AtWork'}</Text>
   {this.renderPlainTextRowWithoutSubheadWithBottomBorder(selectedPersonality.appearToOthers)}
 </View>


 <View style={{backgroundColor:'#fff',alignSelf:'stretch',marginTop:10}}>
          <Text style={{fontSize:15,margin:10}}>{'TeamWorkStyle'}</Text>
   {this.renderPlainTextRowWithoutSubheadWithBottomBorder(selectedPersonality.teamworkStyle)}
 </View>


 <View style={{backgroundColor:'#fff',alignSelf:'stretch',marginTop:10}}>
          <Text style={{fontSize:15,margin:10}}>{'LeadershipStyle'}</Text>
   {this.renderPlainTextRowWithoutSubheadWithBottomBorder(selectedPersonality.leadershipStyle)}
 </View>

 
 <View style={{backgroundColor:'#fff',alignSelf:'stretch',marginTop:10}}>
          <Text style={{fontSize:15,margin:10}}>{'TeamworkStrengths'}</Text>
   {this.renderPlainTextRowWithoutSubheadWithBottomBorder(selectedPersonality.teamworkStrengths)}
 </View>

 
 <View style={{backgroundColor:'#fff',alignSelf:'stretch',marginTop:10}}>
          <Text style={{fontSize:15,margin:10}}>{'LeadershipStrengths'}</Text>
   {this.renderPlainTextRowWithoutSubheadWithBottomBorder(selectedPersonality.leadershipStrengths)}
 </View>



{/* <View style={{backgroundColor:'#fff',alignSelf:'stretch',marginTop:10}}>
          <Text style={{fontSize:15,margin:10}}>{'weaknesses'.toUpperCase()}</Text>
          {this.renderPlainTextRowWithBottomBprder('The masterminf', 'you are enegetic,resourceful,and achiver and you' +
          'make thinfs happen and plan ahead.You are calam and controlled as well as responsible')}
               {this.renderPlainTextRowWithBottomBprder('The masterminf', 'you are enegetic,resourceful,and achiver and you' +
          'make thinfs happen and plan ahead.You are calam and controlled as well as responsible')}


{this.renderPlainTextRowWithBottomBprder('The masterminf', 'you are enegetic,resourceful,and achiver and you' +
          'make thinfs happen and plan ahead.You are calam and controlled as well as responsible')}
</View> */}





          </View>
        </ScrollView>
        {/* <View
    style={{
      height: height * 0.11,
      width: width * 0.95,
      backgroundColor: 'transparent',
      justifyContent: 'center',
      alignItems: 'center',
      borderTopColor:'gray',
      borderTopWidth:0.4
    }}>
   {this.renderDoneButton('DONE')}
  </View> */}
        
      </View>
    );
  }
}

//export default CoachingTopics;
const mapStateToProps = ({auth}) => {
  const {user, selectedCoach, upComingSessions} = auth;

  return {user, selectedCoach, upComingSessions};
};
export default connect(mapStateToProps, null)(FirstResult);

const styles = {
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    marginTop: 50,
    padding: 15,
  },
  buttonContainer: {
    marginTop: 15,
  },
  separator: {
    marginVertical: 30,
    borderWidth: 0.5,
    borderColor: '#DCDCDC',
  },
  label: {
    color: '#999',
    fontSize: 14,
    fontWeight: '500',
    marginBottom: 10,
  },
};

// onPress={this.increase.bind(this, 'progress', 20)}
