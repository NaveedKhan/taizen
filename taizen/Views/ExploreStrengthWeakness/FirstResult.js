import React, {Component} from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  Alert,
  Button,
} from 'react-native';
import {result} from '../../Assets/Source/personality.json';
import ProgressBarAnimated from 'react-native-progress-bar-animated';
import {Actions} from 'react-native-router-flux';
import {
  Container,
  Header,
  Tab,
  Tabs,
  TabHeading,
  Icon,
  Toast,
  Thumbnail,
  Spinner,DeckSwiper
} from 'native-base';
import firebase from 'firebase';
import {connect} from 'react-redux';
import {CustomHeaderWithText,CustomHeaderTransparent} from '../CommonViews';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

class FirstResult extends Component {
  constructor(props) {
    super(props);
    // ...
    this.state = {
      resultArr:result,
      selectedPersonality:result[0],
      progress:83,
      progressWithOnComplete: 20,
      progressCustomized: 20,
      optionsData:[{id:0, isSelected:false,value:'Stronlgy Disagree'},{id:1, isSelected:false,value:'DisAgree'},{id:2, isSelected:false,value:'Neutral'}
    ,{id:3, isSelected:false,value:'Agree'},{id:4, isSelected:false,value:'Stronlgy Agree'}],
    cardArray:[{id:1, value:'card no 1',isSelected:false},{id:2, value:'card no 2',isSelected:false},{id:3, value:'card no 3',isSelected:false},{id:4, value:'card no 4',isSelected:false}]

    };
    this._deckSwiper = null;
  }

  retriveCoachingTopics() {
    debugger;
    var firebaseConfig = {
      apiKey: 'AIzaSyDlfb35etfsO24wlrghJ_dzpgIRSPhff94',
      authDomain: 'taizen-cf195.firebaseapp.com',
      databaseURL: 'https://taizen-cf195.firebaseio.com',
      projectId: 'taizen-cf195',
      storageBucket: 'taizen-cf195.appspot.com',
      messagingSenderId: '814929083198',
      appId: '1:814929083198:web:9465639048338ee3654a6f',
      measurementId: 'G-3S2TR3Z0W1',
    };
    var me = this;
    // Initialize Firebase
    !firebase.apps.length
      ? firebase.initializeApp(firebaseConfig)
      : firebase.app();
    debugger;
    var db2 = firebase.firestore();
    firebase.firestore().settings({experimentalForceLongPolling: true});
    var result = [];
    db2
      .collection('coachingTopics') //"1a392I0XGAWkS6TjOBvLArg9psB2")
      .get()
      .then(function (querySnapshot) {
        debugger;
        querySnapshot.forEach(function (doc) {
          debugger;
          var rec = {coachingTopicsId: doc.id, ...doc.data()};
          result.push(rec);
          // doc.data() is never undefined for query doc snapshots
          ///console.log(doc.id, " => ", doc.data());
        });
        debugger;
        me.setState({isloading: false, coachingTopicsArr: result});
      })
      .catch(function (error) {
        debugger;
        me.setState({isloading: false});
        console.log('Error getting documents: ', error);
      });
  }

  componentDidMount() {
     const {user} = this.props;
     const {resultArr} = this.state;
     let personalityCode = user.personalityCode;
     var resultPersonality = resultArr.find(aa=>aa.code == personalityCode);
     if(resultPersonality)
     {
       this.setState({selectedPersonality:resultPersonality})
     }

   // this.setState({selectedTopicCategory: 'LEADERSHIP'});
    //this.retriveCoachingTopics();
  }

  increase = (key, value) => {
    debugger;
    this.setState({
      [key]: this.state[key] + value,
    });
  };

  renderTabButton(buttonName, tabNumber) {
    var backgroundcolor =
      buttonName == this.state.selectedTopicCategory ? 'white' : 'transparent';
    var tabWidth = width * 0.95;
    return (
      <TouchableOpacity
        onPress={() => this.onPressTabButton(buttonName, tabNumber)}
        style={{
          borderRadius: height * 0.066 * 0.49,
          width: tabWidth * 0.33,
          height: height * 0.06,
          borderWidth: 0,
          borderColor: '#39393a',
          backgroundColor: backgroundcolor,
          shadowOpacity: 0.7,
          shadowRadius: 3,
          shadowOffset: {
            height: 1,
            width: 1,
          },
          //android
          // elevation: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text
          numberOfLines={2}
          style={{
            color: this.state.tabColor,
            fontFamily: 'Ariel',
            fontSize: 13,
            fontWeight: 'bold',
          }}>
          {buttonName}
        </Text>
      </TouchableOpacity>
    );
  }

  renderDoneButton(buttonText)
  {
    return this.state.isUpdatingUserData ? <Spinner style={{alignSelf:'center',margin:20}}/> :
      <TouchableOpacity onPress={()=> Actions.reset('onBoarding')}
    style={{
      width: width * 0.95,
      height: height * 0.08,
      borderRadius:3,
      backgroundColor: '#71a86d',
      alignItems: 'center',
      justifyContent: 'center',
    }}>
    <Text style={{  color:'#fff',
      fontSize:18}}>{buttonText}</Text>
  </TouchableOpacity>
  }

  renderReportButton(buttonText)
  {
    return this.state.isUpdatingUserData ? <Spinner style={{alignSelf:'center',margin:20}}/> :
      <TouchableOpacity onPress={()=> Actions.resultReport()}
    style={{
      width: width * 0.95,
      height: height * 0.06,
      borderRadius:3,
      backgroundColor: '#fc9999',
      alignItems: 'center',
      justifyContent: 'center',
    }}>
    <Text style={{  color:'#fff',
      fontSize:16}}>{buttonText}</Text>
  </TouchableOpacity>
  }

  renderCard()
  {
    const colorss = ['red', 'orange', 'yellow', 'purple', '#4ab865'];
    return  <View
    style={{
      backgroundColor: '#fff',
      width: width * 0.96,
      alignItems: 'center',
      padding: 10,
      borderRadius: 10,
      marginTop:height*0.12
    }}>
    {/* <Text style={{margin:10}}>In the past two months</Text> */}
    <Text style={{fontSize: 18, fontWeight: 'bold',textAlign:'center',marginTop:20}}>
      I have found it difficult to cope with stress
    </Text>

    <View style={{flexDirection: 'row', marginTop: 20}}>

        <View
          style={{
            flex: 4,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
     
     <View style={{ height: height * 0.18,width: height * 0.18,borderRadius:10,
            borderWidth:1,alignItems:'center',justifyContent:"center"}}>
            <TouchableOpacity
          onPress={()=>Alert.alert('sds')}
            style={{
              backgroundColor: 'orange',
              height: height * 0.16,
              width: height * 0.16,
              borderWidth: 0,
              borderRadius:10,
              margin:10,
              alignItems:'center',
              justifyContent:'center'
            }}>
               <Image source={require('../../Assets/mobile.png')} style={{resizeMode:'contain',height: height * 0.15,width: height * 0.15,alignSelf:'center'}}/>
            </TouchableOpacity>
            </View>
             <Text style={{fontSize:16,marginTop:5}}>The Pay</Text>
            </View>

            <View
          style={{
            flex: 4,
            alignItems: 'center',
            justifyContent: 'center',
          }}>

          
          <View style={{ height: height * 0.18,width: height * 0.18,borderRadius:10,
            borderWidth:1,alignItems:'center',justifyContent:"center"}}>
            <TouchableOpacity
          onPress={()=>Alert.alert('sds')}
            style={{
              backgroundColor:'#4db79b',
              height: height * 0.16,
              width: height * 0.16,
              borderWidth: 0,
              borderRadius:10,
              margin:10,
              alignItems:'center',
              justifyContent:'center'
            }}>
               <Image source={require('../../Assets/mobile.png')} style={{resizeMode:'contain',height: height * 0.15,width: height * 0.15,alignSelf:'center'}}/>
            </TouchableOpacity>
            </View>
             <Text style={{fontSize:16,marginTop:5}}>Creating/Building</Text>
            </View>

    </View>

   
  </View>
  }

  handleOptionsClick(el)
  {
    let {optionsData} = this.state;
    var newlist = optionsData;
    var id = el.id;
    newlist.forEach(function(items){
      items.isSelected = items.id == id
    });
     this.setState({optionsData:newlist});
  }

renderTextRow(header, title)
{
  return <View style={{backgroundColor:'#fff',height:height*0.12,width:width*1,borderBottomWidth:0.4, borderBottomColor:'gray', padding:10,justifyContent:'center'}}>
  <Text style={{fontSize:14,color:'gray'}}>
 {header}
  </Text>
  
  <Text style={{fontSize:14,fontWeight:'500',marginTop:5}}>
 {title}
  </Text>
  </View>
  
}


returnImage(personalitytype)
{
  debugger;
if(personalitytype.toLowerCase() == "The Friend".toLowerCase())
 {
  return require(`../../Assets/Personality/TheFriend.png`)
 }
 else if(personalitytype.toLowerCase() == "The Manager".toLowerCase())
 {
  return require(`../../Assets/Personality/TheManager.png`)
 }
 else if(personalitytype.toLowerCase() == "The Orderly".toLowerCase())
 {
  return require(`../../Assets/Personality/TheOrderly.png`)
 }
 else if(personalitytype.toLowerCase() == "The Entertainer".toLowerCase())
 {
  return require(`../../Assets/Personality/TheEntertainer.png`)
 }
 else if(personalitytype.toLowerCase() == "The Composer".toLowerCase())
 {
  return require(`../../Assets/Personality/TheComposer.png`)
 }
 else if(personalitytype.toLowerCase() == "The Conductor".toLowerCase())
 {
  return require(`../../Assets/Personality/TheConductor.png`)
 }

 else if(personalitytype.toLowerCase() == "The Troubleshooter".toLowerCase())
 {
  return require(`../../Assets/Personality/TheTroubleshooter.png`)
 }

 else if(personalitytype.toLowerCase() == "The Idealist".toLowerCase())
 {
  return require(`../../Assets/Personality/TheIdealist.png`)
 }

 else if(personalitytype.toLowerCase() == "The Magnet".toLowerCase())
 {
  return require(`../../Assets/Personality/TheMagnet.png`)
 }

 else if(personalitytype.toLowerCase() == "The Judge".toLowerCase())
 {
  return require(`../../Assets/Personality/TheJudge.png`)
 }

 else if(personalitytype.toLowerCase() == "The Energiser".toLowerCase())
 {
  return require(`../../Assets/Personality/TheEnergiser.png`)
 }
 
 else if(personalitytype.toLowerCase() == "The Mastermind".toLowerCase())
 {
  return require(`../../Assets/Personality/TheMastermind.png`)
 }
 
 else if(personalitytype.toLowerCase() == "The Captain".toLowerCase())
 {
  return require(`../../Assets/Personality/TheCaptain.png`)
 }
 
 else if(personalitytype.toLowerCase() == "The Creator".toLowerCase())
 {
  return require(`../../Assets/Personality/TheCreator.png`)
 }
  
 else if(personalitytype.toLowerCase() == "The Intellectual".toLowerCase())
 {
  return require(`../../Assets/Personality/TheIntellectual.png`)
 }
}

  render() {
    const barWidth = width * 0.95;
    const progressCustomStyles = {
      backgroundColor: 'red',
      borderRadius: 0,
      borderColor: 'orange',
    };
    const {selectedPersonality} = this.state;
    //var imageName = `../../Assets/Personality/${selectedPersonality.type}.png`;
   var imagename = this.returnImage(selectedPersonality.type);


   debugger;
    return (
      <View style={{flex: 1, backgroundColor: '#f7f7f7', alignItems: 'center'}}>
        <StatusBar translucent={true} barStyle="light-content" />
        {/* <CustomHeaderWithText leftIcon={true} text="COMPLETE LATER" /> */}
        
        <View
         style={{
               // backgroundColor: '#4db79b',
                alignItems: 'center',
              }}>
              <Image
              style={{width:width* 1,height:height*0.4}} source={imagename} />
              
              <View style={{position:'absolute',width:width*1}}>
              {/* <CustomHeaderTransparent headerStyle={{backgroundColor:'transparent'}} leftIcon={true} text={"The MasterMind".toUpperCase()} />  */}
              <View style={{flexDirection:'row'}}>
                <View style={{flex:2,paddingTop:20}}>
                {/* <Icon
            name="arrow-back"
            style={{color:'#fff', fontSize: 35}}
          /> */}
                </View>

                <View style={{flex:6,paddingTop:20,alignItems:'center',justifyContent:'center'}}>
                    <Text style={{fontSize:20, color:'#fff',fontWeight:'900'}}>
                      {selectedPersonality.type.toUpperCase()}
                       </Text>
                </View>
                <View style={{flex:2,paddingTop:20}}>

                </View>
              </View>
              </View>

            </View>


        <ScrollView scrollEnabled>
          <View
            style={{
              flex: 1,
              borderWidth: 0,
              margin:5,
              width: width * 1,
              alignItems: 'center',
              backgroundColor: 'transparent',
              justifyContent:'center'
            }}>


<View style={{height:height*0.13,width:width*1,borderWidth:0,padding:10,alignItems:'center',justifyContent:'center',marginTop:10,marginBottom:10,backgroundColor:'#fff'}}>
<Text style={{fontSize:16,fontWeight:'900',textAlign:'center'}}>
 {selectedPersonality.keywords}
</Text>
</View>


{this.renderTextRow('Key Strengths'.toUpperCase(),selectedPersonality.keyStrengths)}
{this.renderTextRow('Main Motivators'.toUpperCase(),selectedPersonality.mainMotivators)}
{this.renderTextRow('Improvement Areas'.toUpperCase(),selectedPersonality.improvementAreas)}


<View style={{height:height*0.13,width:width*1,borderWidth:0,padding:10,alignItems:'center',
justifyContent:'space-between',marginTop:0,marginBottom:10,backgroundColor:'#e85f5f'}}>
<Text style={{fontSize:14,color:'#fff',textAlign:'center'}}>
 See your Full Report here
</Text>
{this.renderReportButton('Report')}
</View>


          </View>
        </ScrollView>
        <View
    style={{
      height: height * 0.11,
      width: width * 0.95,
      backgroundColor: 'transparent',
      justifyContent: 'center',
      alignItems: 'center',
      borderTopColor:'gray',
      borderTopWidth:0.4
    }}>
   {this.renderDoneButton('DONE')}
  </View>
        
      </View>
    );
  }
}

//export default CoachingTopics;
const mapStateToProps = ({auth}) => {
  const {user, selectedCoach, upComingSessions} = auth;

  return {user, selectedCoach, upComingSessions};
};
export default connect(mapStateToProps, null)(FirstResult);

const styles = {
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    marginTop: 50,
    padding: 15,
  },
  buttonContainer: {
    marginTop: 15,
  },
  separator: {
    marginVertical: 30,
    borderWidth: 0.5,
    borderColor: '#DCDCDC',
  },
  label: {
    color: '#999',
    fontSize: 14,
    fontWeight: '500',
    marginBottom: 10,
  },
};

// onPress={this.increase.bind(this, 'progress', 20)}
