import React, { Component } from 'react';
import {
  Dimensions,Text
} from 'react-native';
import { WebView } from 'react-native-webview';
import { Actions } from 'react-native-router-flux';
import DeviceInfo from 'react-native-device-info';
import { View } from 'native-base';
import { CustomHeaderWithText } from '../CommonViews';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;


class index extends Component {
    constructor(props) {
        super(props);
        // ...
      this.webview = null;
this.state = {isloading:false}
    }

 

  render() {
    const htmlTags = `<!DOCTYPE html>
    <html> <head><meta name="viewport" content="width=1080"><title></title></head>
    <style> html, body {  height: 100%;  margin: 0;  }
       .full-height { height: 100%;background: white; }
      </style>
    <body>
    <div class="full-height"><div class='truefinder-inline-widget' data-url='https://api.talentinsights.com/v1/TruityGetAssessment/JtYYy6INLrKHJR64y4T2Mw=='></div><script type='text/javascript' src='https://d3gs66gcyc7ay8.cloudfront.net/typeFinderwidget.js'></script>
    </body>
    </html>
    `;
    return (
      <View style={{flex:1}}>
        <CustomHeaderWithText leftIcon={true} text="Explore Yourself"/>
        {this.state.isloading ? <Text style={{alignSelf:'center',margin:20}}>Loading Page please wait</Text>: <View/>}
        <WebView
      originWhitelist={['*']}
      userAgent={DeviceInfo.getUserAgent() + " - Taizen - android "} 
      ref={ref => (this.webview = ref)}
        source={{ html: htmlTags }}
        style ={{marginTop:30}}
        focusable={true}
        onLoadStart={()=>this.setState({isloading:true})}
        onLoad={()=>this.setState({isloading:false})}
      />
      </View>
      
    );
  }
}
export default index;