import React, {Component} from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  Alert,
  Button,
} from 'react-native';
import {UpdateUserFormData} from '../../actions';
import {questions} from '../../Assets/Source/questions.json';
import {result} from '../../Assets/Source/personality.json';
import ProgressBarAnimated from 'react-native-progress-bar-animated';
import {Actions} from 'react-native-router-flux';
import {
  Container,
  Header,
  Tab,
  Tabs,
  TabHeading,
  Icon,
  Toast,
  Thumbnail,
  Spinner,DeckSwiper
} from 'native-base';
import firebase from 'firebase';
import {connect} from 'react-redux';
import {CustomHeaderWithText} from '../CommonViews';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

class Questionier extends Component {
  constructor(props) {
    super(props);
    // ...
    this.state = {
      questionArray:questions,
      personalityArray:result,
      isUpdatingUserData:false,
      resultArray:[],
      currentQueation:1,
      progreeIncrement:0,
      progress: 0,
      progressWithOnComplete: 20,
      progressCustomized: 20,
      optionsData:[{id:0, isSelected:false,value:'Stronlgy Disagree'},{id:1, isSelected:false,value:'DisAgree'},{id:2, isSelected:false,value:'Neutral'}
    ,{id:3, isSelected:false,value:'Agree'},{id:4, isSelected:false,value:'Stronlgy Agree'}],
    cardArray:[{id:1, value:'card no 1',isSelected:false},{id:2, value:'card no 2',isSelected:false},{id:3, value:'card no 3',isSelected:false},{id:4, value:'card no 4',isSelected:false}]

    };
    this._deckSwiper = null;
  }

  retriveCoachingTopics() {
    debugger;
    var firebaseConfig = {
      apiKey: 'AIzaSyDlfb35etfsO24wlrghJ_dzpgIRSPhff94',
      authDomain: 'taizen-cf195.firebaseapp.com',
      databaseURL: 'https://taizen-cf195.firebaseio.com',
      projectId: 'taizen-cf195',
      storageBucket: 'taizen-cf195.appspot.com',
      messagingSenderId: '814929083198',
      appId: '1:814929083198:web:9465639048338ee3654a6f',
      measurementId: 'G-3S2TR3Z0W1',
    };
    var me = this;
    // Initialize Firebase
    !firebase.apps.length
      ? firebase.initializeApp(firebaseConfig)
      : firebase.app();
    debugger;
    var db2 = firebase.firestore();
    firebase.firestore().settings({experimentalForceLongPolling: true});
    var result2 = [];
    db2
      .collection('coachingTopics') //"1a392I0XGAWkS6TjOBvLArg9psB2")
      .get()
      .then(function (querySnapshot) {
        debugger;
        querySnapshot.forEach(function (doc) {
          debugger;
          var rec = {coachingTopicsId: doc.id, ...doc.data()};
          result2.push(rec);
          // doc.data() is never undefined for query doc snapshots
          ///console.log(doc.id, " => ", doc.data());
        });
        debugger;
        me.setState({isloading: false, coachingTopicsArr: result2});
      })
      .catch(function (error) {
        debugger;
        me.setState({isloading: false});
        console.log('Error getting documents: ', error);
      });
  }

  componentDidMount() {
    debugger;
    const {questionArray} = this.state;
    var countInc = 100/questionArray.length;
    questionArray.forEach(function(el){
      el.isSelected = false;
    })

    this.setState({selectedTopicCategory: 'LEADERSHIP',progreeIncrement:countInc});
    //this.retriveCoachingTopics();
  }

  increase = (key, value) => {
    debugger;
    this.setState({
      [key]: this.state[key] + value,
    });
  };

  renderTabButton(buttonName, tabNumber) {
    var backgroundcolor =
      buttonName == this.state.selectedTopicCategory ? 'white' : 'transparent';
    var tabWidth = width * 0.95;
    return (
      <TouchableOpacity
        onPress={() => this.onPressTabButton(buttonName, tabNumber)}
        style={{
          borderRadius: height * 0.066 * 0.49,
          width: tabWidth * 0.33,
          height: height * 0.06,
          borderWidth: 0,
          borderColor: '#39393a',
          backgroundColor: backgroundcolor,
          shadowOpacity: 0.7,
          shadowRadius: 3,
          shadowOffset: {
            height: 1,
            width: 1,
          },
          //android
          // elevation: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text
          numberOfLines={2}
          style={{
            color: this.state.tabColor,
            fontFamily: 'Ariel',
            fontSize: 13,
            fontWeight: 'bold',
          }}>
          {buttonName}
        </Text>
      </TouchableOpacity>
    );
  }

  onPressNextQuestion(){
    debugger;
    const {currentQueation,questionArray,progreeIncrement,progress,optionsData} = this.state;
let newlist = optionsData;
    if(currentQueation < questionArray.length)
    {
      newlist.forEach(function(items){
        items.isSelected = items.id == id
      });
       this.setState({optionsData:newlist,currentQueation:currentQueation+1,progress:progress+progreeIncrement})
    }
    
    else
    {
     // Alert.alert('test completed');
    this.updateUserDataOnFirebase();
    }
  }
   randomIntFromInterval(min, max) { // min and max included 
    return Math.floor(Math.random() * (max - min + 1) + min);
  }
  // green =  hexString: "#4ab865"
  // yellow = hexString: "#ffdb7c"
  //  peach =  hexString: "#ffb390"
  //   red =   hexString: "#b80f0a"

  updateFormData(fieldname, value) {
    this.props.UpdateUserFormData({prop: fieldname, value: value});
  }


  updateUserDataOnFirebase() {
    var me=this;
    debugger;
    const {personalityArray} = this.state;
    var randomNumber = this.randomIntFromInterval(0,personalityArray.length-1);
    var code = personalityArray[randomNumber].code;
    me.setState({isUpdatingUserData:true});
        const {user} = this.props;
        //const {selectedCategoryTopics} = this.state;
        //const {coachingTopicsArr} = this.state;
      //  var selectedCat = [];
      //  selectedCat = coachingTopicsArr.filter((aa) => selectedCategoryTopics.includes(aa.title));
        const {id} = user;
        const topicChosen = user.profileSteps ? (user.profileSteps.topicChosen?true:false):false;
       // const assessment = user.profileSteps ? (user.profileSteps.assessment?true:false):false;
        const coachAssigned = user.profileSteps ? (user.profileSteps.coachAssigned?true:false):false;
        console.log('id is', id);
        var db = firebase.firestore();
        firebase.firestore().settings({experimentalForceLongPolling: true});
        var washingtonRef = db
          .collection('users')
          .doc(id);
    
        // Set the "capital" field of the city 'DC'
        return washingtonRef
          .update({
            profileSteps:
            {
              coachAssigned:coachAssigned,
              topicChosen:topicChosen,
              assessment:true
            },
           // selectedTopics:selectedCat,
            personalityCode:code
          })
          .then(function () {
            debugger;
            me.setState({isUpdatingUserData:false});
            var updatedUser = user;
            updatedUser.profileSteps = {
              coachAssigned:coachAssigned,
              topicChosen:topicChosen,
              assessment:true
            };
           // updatedUser.selectedTopics = selectedCat;
            updatedUser.personalityCode = code;
            //Alert.alert('Success','Session Booked Successfully');
            me.updateFormData('user',updatedUser);
            Actions.firstResult();
          })
          .catch(function (error) {
            // The document probably doesn't exist.
            debugger;
            me.setState({isUpdatingUserData: false});
            console.error('Error updating document: ', error);
          });
      }


  renderDoneButton(buttonText)
  {
    const {currentQueation,questionArray} = this.state;
    var selectedQuestion = questionArray[currentQueation-1];
    return this.state.isUpdatingUserData ? <Spinner style={{alignSelf:'center',margin:20}}/> : 
     <TouchableOpacity onPress={()=> selectedQuestion.isSelected ? this.onPressNextQuestion():
    console.log('')}
    // onPress={()=> Actions.questionier2()}
    style={{
      width: width * 0.95,
      height: height * 0.08,
      borderRadius:3,
      backgroundColor: selectedQuestion.isSelected ? '#4ab865':'#E9E9E9',
      alignItems: 'center',
      justifyContent: 'center',
    
    }}>
    <Text style={{  color:selectedQuestion.isSelected ? '#fff':'gray',
      fontSize:18}}>{buttonText}</Text>
  </TouchableOpacity>
  }

renderCardSwiper()
{
  let {cardArray} = this.state;
 return <DeckSwiper
  ref={(c) => this._deckSwiper = c}
  dataSource={cardArray}
  renderEmpty={() =>
    <View style={{ alignSelf: "center" ,height:400,width:300}}>
      <Text>Over</Text>
    </View>
  }
  renderItem={item =>
 this.renderCardSw(item)
  }
/>
}

renderCardSw(item)
{
  debugger;
  const colorss = ['red', 'orange', 'yellow', 'purple', '#4ab865'];
  return  <View
  style={{
    backgroundColor: '#fff',
    width: width * 0.96,
    alignItems: 'center',
    alignSelf:'center',
    padding: 10,
    borderRadius: 10,
  }}>
  <Text style={{margin:10}}>In the past two months {item.id}</Text>
  <Text style={{fontSize: 18, fontWeight: 'bold',textAlign:'center'}}>
    I have found it difficult to cope with stress
  </Text>
<Button title={'sds'} onPress={()=> Alert.alert('dsd')}>
  
</Button>
  <View style={{flexDirection: 'row', marginTop: 20}}>
    {this.state.optionsData.map((el, index) => (
      <View
        key={index}
        style={{
          flex: 2,
          height: height * 0.1,
          alignItems: 'center',
          justifyContent: 'center',
        }}>

          <View style={{height: height * 0.09,alignItems:'center',justifyContent:'center',
            width: height * 0.09,
            borderRadius: height * 0.09 * 0.5,borderWidth:el.isSelected ? 0.6 : 0,borderColor:colorss[index]}}>
       
          <TouchableOpacity
          onPress={()=>Alert.alert('jhjkhjkhjkh')}
          style={{
            backgroundColor: colorss[index],
            height: height * 0.08,
            width: height * 0.08,
            borderRadius: height * 0.08 * 0.5,
            borderWidth: 0,
            alignItems:'center',
            justifyContent:'center'
          }}>
         {el.isSelected ? <Icon type="Entypo" name="check" style={{fontSize:height*0.058,color:'#fff'}} /> : <View/>}   
          </TouchableOpacity>

          </View>

      </View>
    ))}


  </View>

  <View style={{flexDirection: 'row', marginTop: 20}}>
 
      <View
        
        style={{
          flex: 2,
          height: height * 0.1,
          width:width*0.2,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
       <Text style={{fontSize:16,textAlign:'center'}}>Strongly Diasgree</Text>
      </View>
      <View style={{flex:2}}></View>
      <View
        
        style={{
          flex: 2,
          height: height * 0.1,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
       <Text style={{fontSize:16,textAlign:'center'}}>Neutral</Text>
      </View>
      <View style={{flex:2}}></View>
      <View
        
        style={{
          flex: 2,
          height: height * 0.1,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
       <Text style={{fontSize:16,textAlign:'center'}}>Strongly Agree</Text>
      </View>
   
  </View>

</View>
}



  renderCard()
  {
    const colorss = ['#b80f0a', 'orange', '#ffdb7c', '#ffb390', '#4ab865'];
    const {currentQueation,questionArray} = this.state;
    var selectedQuestion = questionArray[currentQueation-1];
    return  <View
    style={{
      backgroundColor: '#fff',
      width: width * 0.96,
      alignItems: 'center',
       paddingTop: 20,
      borderRadius: 10,
      marginTop:height*0.12
    }}>
    {/* <Text style={{margin:10}}>In the past two months</Text> */}
  
  {selectedQuestion.type == 'single' ? 
  <Text style={{fontSize: 17, fontWeight: 'bold',textAlign:'center'}}>
  {selectedQuestion.phraseOne}
 </Text>
  : 
  <View style={{flexDirection:'row',backgroundColor:'transparent'}}>
    <View style={{flex:4.3,backgroundColor:'transparent',justifyContent:'flex-start'}}>
    <Text style={{fontSize: 17, fontWeight: 'bold',textAlign:'center'}}>
      {selectedQuestion.phraseOne}
      </Text>
    </View>
<View style={{flex:1.4}}></View>
    <View style={{flex:4.3}}>
    <Text style={{fontSize: 17, fontWeight: 'bold',textAlign:'center'}}>
      {selectedQuestion.phraseTwo}
      </Text>
    </View>
 
    </View>
  }

    

    

    <View style={{flexDirection: 'row', marginTop: 20}}>
      {this.state.optionsData.map((el, index) => (
        <View
          key={index}
          style={{
            flex: 2,
            height: height * 0.1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>

            <View style={{height: height * 0.09,alignItems:'center',justifyContent:'center',
              width: height * 0.09,
              borderRadius: height * 0.09 * 0.5,borderWidth:el.isSelected ? 0.6 : 0,borderColor:colorss[index]}}>
         
            <TouchableOpacity
          onPress={()=>this.handleOptionsClick(el)}
            style={{
              backgroundColor: colorss[index],
              height: height * 0.08,
              width: height * 0.08,
              borderRadius: height * 0.08 * 0.5,
              borderWidth: 0,
              alignItems:'center',
              justifyContent:'center'
            }}>
           {el.isSelected ? <Icon type="Entypo" name="check" style={{fontSize:height*0.058,color:'#fff'}} /> : <View/>}   
            </TouchableOpacity>

            </View>

        </View>
      ))}


    </View>

    <View style={{flexDirection: 'row', marginTop: 20}}>
   
        <View
          
          style={{
            flex: 2,
            height: height * 0.1,
            width:width*0.2,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
         <Text style={{fontSize:16,textAlign:'center'}}>Strongly Diasgree</Text>
        </View>
        <View style={{flex:2}}></View>
        <View
          
          style={{
            flex: 2,
            height: height * 0.1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
         <Text style={{fontSize:16,textAlign:'center'}}>Neutral</Text>
        </View>
        <View style={{flex:2}}></View>
        <View
          
          style={{
            flex: 2,
            height: height * 0.1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
         <Text style={{fontSize:16,textAlign:'center'}}>Strongly Agree</Text>
        </View>
     
    </View>

  </View>
  }

  handleOptionsClick(el)
  {
    let {optionsData} = this.state;
    const {currentQueation,questionArray} = this.state;
    var selectedQuestion = questionArray[currentQueation-1];
    questionArray[currentQueation-1].isSelected = true;
    var newlist = optionsData;
    var id = el.id;
    newlist.forEach(function(items){
      items.isSelected = items.id == id
    });
     this.setState({optionsData:newlist});
  }

  handleOptionsClickForSwiper(el)
  {
    debugger;
    let {cardArray} = this.state;
    var newlist = cardArray;
    var id = el.id;
    newlist.forEach(function(items){
      items.isSelected = items.id == id
    });
     this.setState({cardArray:newlist});
  }

  render() {
    const barWidth = width * 0.95;
    const progressCustomStyles = {
      backgroundColor: 'red',
      borderRadius: 0,
      borderColor: 'orange',
    };
  
    return (
      <View style={{flex: 1, backgroundColor: '#f7f7f7', alignItems: 'center'}}>
        <StatusBar translucent backgroundColor={'transparent'} barStyle="light-content" />
        <CustomHeaderWithText leftIcon={true} text="COMPLETE LATER" />
        
        <View
              style={{
                backgroundColor: '#4db79b',
                alignItems: 'center',
                width: width * 1,
              }}>
         <Image
          source={require('../../Assets/Rectangle.png')}
          style={{
            height: Dimensions.get('window').height / 9,
            width: width * 1,
            resizeMode: 'cover',
          }}/>

              <View
                style={{
                  //backgroundColor: '#4db79b',
                  alignItems: 'center',
                  width: width * 0.95,
                  borderBottomWidth: 0,
                  position:'absolute'
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    backgroundColor: 'transparent',
                    alignSelf: 'stretch',
                  }}>
                  <View style={{flex: 5.4, alignItems: 'flex-end'}}>
                    <Text
                      style={{
                        backgroundColor: 'transparent',
                        fontSize: 16,
                        fontWeight: 'bold',
                        color: '#fff',
                      }}>
                      {`${parseFloat(this.state.progress).toFixed(2)}%`}
                    </Text>
                  </View>
                  <View style={{flex: 4.6}}>
                    <Text
                      style={{
                        textAlign: 'right',
                        backgroundColor: 'transparent',
                        fontSize: 15,
                        color: '#fff',
                      }}>
                      {this.state.currentQueation}
                    </Text>
                  </View>
                </View>
                <View style={{marginTop: 5, marginBottom: 10}}>
                  <ProgressBarAnimated
                    style={{borderRadius: 20}}
                    width={barWidth}
                    height={20}
                    value={this.state.progress}
                    backgroundColorOnComplete="#6CC644"
                    maxValue={100}
                    backgroundColor={'orange'}
                    onComplete={() => {
                      Alert.alert('Hey!', 'onComplete event fired!');
                    }}
                  />
                </View>
              </View>
        </View>


        <ScrollView scrollEnabled>
          <View
            style={{
              flex: 1,
              borderWidth: 0,
              marginTop: -1,
              width: width * 1,
              alignItems: 'center',
              backgroundColor: 'transparent',
              justifyContent:'center'
            }}>


         

     {this.renderCard()}
{/* 
     <View style={{backgroundColor:'transparent',height:width * 0.96,width: width * 0.96,
      alignItems: 'center',
      borderRadius: 10,
      marginTop:height*0.07}}
     >
{this.renderCardSwiper()}

     </View> */}

          </View>
        </ScrollView>
        <View
    style={{
      height: height * 0.11,
      width: width * 0.95,
      backgroundColor: 'transparent',
      justifyContent: 'center',
      alignItems: 'center',
      borderTopColor:'gray',
      borderTopWidth:0.4
    }}>
   {this.renderDoneButton('NEXT QUESTION')}
  </View>
        
      </View>
    );
  }
}

//export default CoachingTopics;
const mapStateToProps = ({auth}) => {
  const {user, selectedCoach, upComingSessions} = auth;

  return {user, selectedCoach, upComingSessions};
};
export default connect(mapStateToProps, {UpdateUserFormData})(Questionier);

const styles = {
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    marginTop: 50,
    padding: 15,
  },
  buttonContainer: {
    marginTop: 15,
  },
  separator: {
    marginVertical: 30,
    borderWidth: 0.5,
    borderColor: '#DCDCDC',
  },
  label: {
    color: '#999',
    fontSize: 14,
    fontWeight: '500',
    marginBottom: 10,
  },
};

// onPress={this.increase.bind(this, 'progress', 20)}
