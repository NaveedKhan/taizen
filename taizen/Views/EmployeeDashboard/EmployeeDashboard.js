import React, {Component} from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  Modal,
  PanResponder,
  Alert,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import firebase from 'firebase';
import {
  Container,
  Header,
  Tab,
  Tabs,
  TabHeading,
  Icon,
  Thumbnail,
  Spinner,
} from 'native-base';
import { UpdateUserFormData} from '../../actions';
import {CustomHeaderWithText} from '../CommonViews';
import moment from 'moment';
import Animated from 'react-native-reanimated';
import CustomFooter from '../CommonViews/CustomFooter';
import requestCameraAndAudioPermission from '../VideoCall/permission';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

class EmployeeDashboard extends Component {
  constructor(props) {
    super(props);
    // this.state = {sessionArray:[],isloading:false}
    this.state = {
      AppID: '19e5b00e21dc4d1b8fcffc52e7159fa7', //Set your APPID here
      isloading:false, sessionArray:[],selectedCoach:null,
      isloadingCoach:false
      //ChannelName: '', //Set a default channel or leave blank
    };
    
    if (Platform.OS === 'android') {
      //Request required permissions from Android
      requestCameraAndAudioPermission().then(_ => {
        console.log('requested!');
      });
    }

  }


  getUserData = ()=> {
    debugger;
    var me = this;
    this.setState({isloadingCoach:true})
    const {user} = this.props;
    var coachId = user.assignedCoachId;
    var db = firebase.firestore();
    firebase.firestore().settings({experimentalForceLongPolling: true});
    var docRef = db.collection('users').doc(coachId);
    docRef
      .get()
      .then(function (doc) {
        debugger;
        
        if (doc.exists) {
          console.log('coach is fetched:');
               me.setState({isloadingCoach:false,selectedCoach:doc.data()})
               me.props.UpdateUserFormData({prop: 'selectedCoach', value: doc.data()});
        } else {
          me.setState({isloadingCoach:false})
        }
      })
      .catch(function (error) {
        debugger;
        me.setState({isloadingCoach:false})

        console.log('Error getting document:', error);
      });
  }
  
  componentDidMount() {
    // Initialize Firebase
    // setTimeout(chnagepage, 2000);
    //chnagepage();
    const {selectedCoach} = this.props
    if(selectedCoach == null)
    {
      this.getUserData();
    }
    this.retiriveUpComingSessions();
  }

  retiriveUpComingSessions()
  {
    debugger;
    const {user} = this.props;
    const {id} = user;
    var firebaseConfig = {
      apiKey: "AIzaSyDlfb35etfsO24wlrghJ_dzpgIRSPhff94",
      authDomain: "taizen-cf195.firebaseapp.com",
      databaseURL: "https://taizen-cf195.firebaseio.com",
      projectId: "taizen-cf195",
      storageBucket: "taizen-cf195.appspot.com",
      messagingSenderId: "814929083198",
      appId: "1:814929083198:web:9465639048338ee3654a6f",
      measurementId: "G-3S2TR3Z0W1"
    };
    var me = this;
    this.setState({isloading:true});
    // Initialize Firebase
    !firebase.apps.length ? firebase.initializeApp(firebaseConfig): firebase.app();
    debugger;
    var db2 = firebase.firestore();
    firebase.firestore().settings({experimentalForceLongPolling: true});
    var result = []
    db2.collection("sessions").where("employeeId", "==", id)//"1a392I0XGAWkS6TjOBvLArg9psB2")
    .get()
    .then(function(querySnapshot) {
      debugger;  
      querySnapshot.forEach(function(doc) {
          debugger;
          var rec= {sessionid:doc.id,...doc.data()};
          var sessiondate = rec.date.toDate();
          console.log('sessiondate',sessiondate);
          var sesssiondatemom = moment(sessiondate);
          var currentdate = moment(new Date());
          if(sesssiondatemom.isAfter(currentdate))
             result.push(rec)
            // doc.data() is never undefined for query doc snapshots
            ///console.log(doc.id, " => ", doc.data());
        });
        debugger;
        result.sort(function(a, b) {
          var c = new Date(a.date);
          var d = new Date(b.date);
          return c-d;
      });
         me.setState({isloading:false,sessionArray:result});
         me.props.UpdateUserFormData({prop: 'upComingSessions', value: result});
    })
    .catch(function(error) {
      debugger;
      me.setState({isloading:false});
        console.log("Error getting documents: ", error);
    });
  }

  _renderCards() {
    debugger;
    const {user} = this.props;
    const {selectedCoach} = this.props;
    var image = require('../../Assets/add-session.png');


    return (
      <TouchableOpacity
      onPress={()=> selectedCoach ? Actions.scheduleSession({selectedCoach:selectedCoach,isCoachAssigned:true}): console.log('null coach')}
        style={{
          height: height * 0.16,
          backgroundColor: '#4ab865',
          borderRadius: 10,
          marginBottom: 10,
          justifyContent: 'flex-end',
          overflow: 'hidden',
        }}>
        <Image
          source={image}
          style={{
            height: height * 0.16,
            width: width * 0.95,
            resizeMode: 'cover',
          }}
        />

        <View
          style={{
            position: 'absolute',
            height: height * 0.16,
            backgroundColor: 'transparent',
            flexDirection: 'column',
            justifyContent: 'flex-end',
            alignItems: 'center',
            width: width * 0.95,
            bottom: height * 0.16 * 0.2,
          }}>
          <Text style={{color: 'white', fontSize: 18, fontWeight: 'bold'}}>
            {'Add new session'.toUpperCase()}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  renderActionsCards()
  {
    return    <View
    style={{
      flexDirection: 'row',
      height: height * 0.35,
      borderWidth: 0,
      margin:10,
       borderRadius: 10,
    }}>
    <View
      style={{
        backgroundColor: 'white',
        borderRadius: 10,
        padding: 15,
        width: width * 0.5,
        overflow: 'hidden',
      }}>
      <View
        style={{
          flex: 7,
          borderWidth: 0,
          backgroundColor: '',
          backgroundColor: '#4db79b',
          borderRadius:10
        }}>
          <View style={{flex:5}}/>
          <View style={{flex:5,padding:5,paddingLeft:15}}>
          <Text style={{fontSize:16,color:'white',fontWeight:'900'}}>Practice daily medicaton</Text>
          </View>
        
        </View>
      <View style={{flex: 3.1, borderWidth: 0, padding: 10,marginTop:10}}>
        <Text style={{fontSize: 16, fontWeight: 'bold'}}>
          7:30 am
        </Text>
        <Text style={{fontSize: 15, color: 'gray'}}>
          Repeat daily
        </Text>
      </View>
    </View>
  </View>


  }

  renderUpComingSessionGridRow() {
    debugger;
    const {upComingSessions} = this.props;
    return   this.state.isloading ? <Spinner/> :
    upComingSessions.length < 1 ? <Text style={{margin:10, textAlign:'center'}}>No upcoming session</Text>:
    upComingSessions.slice(0,1).map(el=>{
        debugger;
      return  <View
        key={el.sessionid}
        style={{
          width: width * 0.95,
          borderColor: '#e0e0e0',
          borderWidth: 1,
          backgroundColor: 'white',
          borderRadius: 8,
          marginBottom: 15,
          alignSelf: 'center',
          justifyContent: 'center',
          flexDirection: 'row',
          marginBottom: 10,
        }}>
        <View
          style={{
            flex: 6.5,
            backgroundColor: 'transparent',
            padding: 20,
          }}>
          <Text style={{fontSize: 14, marginTop: 1}}>{moment(el.date.toDate()).format('dddd DD MMM yyyy')}</Text>
          <Text style={{fontSize: 14, marginTop: 1}}>
            {`${el.timeWindow} w/${el.employeeName}`}
          </Text>
          <Text style={{fontSize: 14, marginBottom: 0, marginTop: 1}}>
            30 min
          </Text>
        </View>
        <View
          style={{
            flex: 3.5,
            backgroundColor: 'transparent',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            onPress={() =>  Actions.video({AppID:this.state.AppID, ChannelName:el.channelName})}
            style={{
              width: width * 0.23,
              height: height * 0.05,
              borderRadius: 5,
              borderWidth: 1,
              borderColor: '#c4c0c0',
              marginTop: 5,
              backgroundColor: 'transparent',
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
            }}>
            <Text style={{color: 'gray', fontSize: 14,}}>
              {'JOIN'}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
  }
        )
   
  
  }

  render() {
   const {sessionArray} = this.state;
   const {upComingSessions} = this.props;
    return (
      <View style={{flex: 1, backgroundColor: '#f9f9f9', alignItems: 'center'}}>
        <StatusBar backgroundColor={'transparent'} barStyle="dark-content" />

        <ScrollView scrollEnabled showsVerticalScrollIndicator={false}>
          {/* <CustomHeaderWithText
            text="YOUR COACHING"
            headerStyle={{backgroundColor: 'transparent'}}
            headerTextStyle={{color: 'black'}}
            iconStyle={{color: '#000'}}
          /> */}

          <View style={{flex: 1}}>
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                marginTop: height*0.07,
              }}>
              {this._renderCards()}
            </View>

            <View
              style={{
                marginTop: 20,
                borderWidth: 0,
                flexDirection: 'row',
                justifyContent: 'center',
              }}>
              <View style={{flex: 3.3, backgroundColor: 'transparent'}} />
              <View
                style={{
                  flex: 3.3,
                  margin: 10,
                  backgroundColor: 'transparent',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{fontSize: 18, fontWeight: '900'}}>{'Upcoming'.toUpperCase()}</Text>
              </View>
              <View style={{flex: 3.3}}>
                <TouchableOpacity
                  onPress={() => Actions.allSessions({sessionArray:upComingSessions})}
                  style={{
                    width: width * 0.23,
                    height: height * 0.05,
                    borderRadius: 5,
                    borderWidth: 1,
                    borderColor: '#c4c0c0',
                    marginTop: 5,
                    backgroundColor: 'transparent',
                    alignItems: 'center',
                    justifyContent: 'center',
                    alignSelf: 'center',
                  }}>
                  <Text
                    style={{color: 'gray', fontSize: 14}}>
                    {'SEE ALL'}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            {this.renderUpComingSessionGridRow()}
            {/* Actions to repeat area */}

            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                borderWidth: 0,
                marginTop: 10,
                width:width*1
              }}>
              <Text style={{fontSize: 18, fontWeight: '900', margin: 10}}>
                {'Actions to Repeat'.toUpperCase()}
              </Text>

              <ScrollView scrollEnabled horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={{
                  flexDirection: 'row',
                  borderWidth: 0,
                  margin:10
                  
                }}>
        
          {this.renderActionsCards()}
          {this.renderActionsCards()}
          {this.renderActionsCards()}
            </View>
            </ScrollView>
            </View>
            {/* Activites area */}
            <View
              style={{
                marginTop: 20,
                borderWidth: 0,
                flexDirection: 'row',
                justifyContent: 'center',
              }}>
              <View style={{flex: 3.3, backgroundColor: 'transparent'}} />
              <View
                style={{
                  flex: 3.3,
                  margin: 10,
                  backgroundColor: 'transparent',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{fontSize: 18, fontWeight: '900'}}>
                  {'Activities'.toUpperCase()}
                </Text>
              </View>
              <View style={{flex: 3.3}}>
                <TouchableOpacity
                  onPress={() => this.setState({modalVisible: true})}
                  style={{
                    width: width * 0.23,
                    height: height * 0.05,
                    borderRadius: 5,
                    borderWidth: 1,
                    borderColor: '#c4c0c0',
                    marginTop: 5,
                    backgroundColor: 'transparent',
                    alignItems: 'center',
                    justifyContent: 'center',
                    alignSelf: 'center',
                  }}>
                  <Text
                    style={{color: 'gray', fontSize: 14}}>
                    {'SEE ALL'}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>

            <View
              style={{
                width: width * 0.95,
                borderBottomColor: '#c4c0c0',
                borderBottomWidth: 1,
                backgroundColor: 'white',
                borderRadius: 6,
                marginTop: 10,
                alignSelf: 'center',
                justifyContent: 'center',
                flexDirection: 'row',
              }}>
              <View
                style={{
                  flex: 4.7,
                  backgroundColor: 'transparent',
                  padding: 20,
                }}>
                <Text style={{fontSize: 14, marginTop: 1}}>
                  Complete the Enlighten Excercise before next session
                </Text>
              </View>
              <View style={{flex: 1.8, backgroundColor: 'transparent'}} />
              <View
                style={{
                  flex: 3.5,
                  backgroundColor: 'transparent',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  onPress={() => this.setState({modalVisible: true})}
                  style={{
                    width: width * 0.23,
                    height: height * 0.05,
                    borderRadius: 5,
                    borderWidth: 1,
                    borderColor: '#c4c0c0',
                    marginTop: 5,
                    backgroundColor: 'transparent',
                    alignItems: 'center',
                    justifyContent: 'center',
                    alignSelf: 'center',
                  }}>
                  <Text
                    style={{color: 'gray', fontSize: 14}}>
                    {'JOIN'}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>

            <View></View>
          </View>
        </ScrollView>
        <CustomFooter />
      </View>
    );
  }
}

// export default EmployeeDashboard;


const mapStateToProps = ({auth}) => {
  const {user,selectedCoach,upComingSessions} = auth;

  return {user,selectedCoach,upComingSessions};
};
export default connect(mapStateToProps, {UpdateUserFormData})(EmployeeDashboard);
