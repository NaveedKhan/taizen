import React, {Component} from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  Modal,
  PanResponder,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {
  Container,
  Header,
  Tab,
  Tabs,
  TabHeading,
  Icon,
  Thumbnail,
} from 'native-base';
import {CustomHeaderWithText} from '../CommonViews';
import Animated from 'react-native-reanimated';
import CustomFooter from '../CommonViews/CustomFooter';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

class Journey extends Component {
  constructor(props) {
    super(props);
    // ...
  }
  componentDidMount() {
    // Initialize Firebase
    // setTimeout(chnagepage, 2000);
    //chnagepage();
  }

  _renderCards(text, imagename, onpressButton) {
    debugger;
    var image = require('../../Assets/onBoardimage1.png');
    image =
      imagename == 'onBoardimage1.png'
        ? require('../../Assets/onBoardimage1.png')
        : imagename == 'onBoardimage2.png'
        ? require('../../Assets/onBoardimage2.png')
        : require('../../Assets/onBoardimage3.png');

    return (
      <View
        style={{
          height: height * 0.16,
          backgroundColor: '#4ab865',
          borderRadius: 10,
          marginBottom: 10,
          justifyContent: 'flex-end',
          overflow: 'hidden',
        }}>
        <Image
          source={image}
          style={{
            height: height * 0.16,
            width: width * 0.95,
            resizeMode: 'cover',
          }}
        />

        <View
          style={{
            position: 'absolute',
            height: height * 0.16,
            backgroundColor: 'transparent',
            flexDirection: 'column',
            justifyContent: 'flex-end',
            alignItems: 'center',
            width: width * 0.95,
            bottom: height * 0.16 * 0.2,
          }}>
          <Text style={{color: 'white', fontSize: 23, fontWeight: 'bold'}}>
            Add new session
          </Text>
        </View>
      </View>
    );
  }

  renderBadge(iconaName, backgroundcolor, badgeTitle, badgevalue) {
    debugger;
    return (
      <View style={{flex: 1, overflow: 'hidden'}}>
        <View
          style={{
            flex: 6.6,
            backgroundColor: 'white',
            padding: 5,
            paddingLeft: 10,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <View
            style={{
              width: width * 0.09,
              height: width * 0.09,
              marginTop: 5,
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: width * 0.09 * 0.5,
              backgroundColor: backgroundcolor,
            }}>
            <Icon
              name={iconaName}
              style={{
                color: '#fff',

                alignSelf: 'center',
                fontSize: height * 0.034,
              }}
            />
          </View>

          <Text
            style={{
              fontSize: 25,
              backgroundColor: 'transparent',
              margin: 5,
              marginTop: 1,
              fontWeight: 'bold',
              textAlign: 'center',
            }}>
            {badgevalue}
          </Text>
        </View>
        <View
          style={{
            flex: 3.4,
            backgroundColor: 'transparent',
            padding: 5,
            paddingLeft: 10,
            paddingTop: 2,
            justifyContent: 'flex-start',
          }}>
          <Text
            style={{
              fontSize: 20,
              color: 'gray',
              fontFamily: 'Ariel',
              fontWeight: 'bold',
            }}>
            {badgeTitle}
          </Text>
        </View>
      </View>
    );
  }

  renderLargeBadge() {
    return (
      <View style={{flex: 1, overflow: 'hidden'}}>
        <View
          style={{
            flex: 6.6,
            backgroundColor: 'white',
            padding: 5,
            paddingLeft: 10,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <View
            style={{
              width: width * 0.09,
              height: width * 0.09,
              marginTop: 5,
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: width * 0.09 * 0.5,
              backgroundColor: '#4db79b',
            }}>
            <Icon
              name="person"
              style={{
                color: '#fff',

                alignSelf: 'center',
                fontSize: height * 0.034,
              }}
            />
          </View>

          <Text
            style={{
              fontSize: 25,
              backgroundColor: 'transparent',
              margin: 5,
              marginTop: 1,
              fontWeight: 'bold',
              textAlign: 'center',
            }}>
            2
          </Text>
        </View>
        <View
          style={{
            flex: 3.4,
            backgroundColor: 'transparent',
            padding: 5,
            paddingLeft: 10,
            paddingTop: 2,
            justifyContent: 'flex-start',
          }}>
          <Text
            style={{
              fontSize: 20,
              color: 'gray',
              fontFamily: 'Ariel',
              fontWeight: 'bold',
            }}>
            Today
          </Text>
        </View>
      </View>
    );
  }

  renderTopicBadge(badgerheadertitle, badgetitle, badgevalue,backgroundcolor) {
    return (
      <View
        style={{
          backgroundColor: '#f9f9f9',
          marginTop: height * 0.05,
          borderRadius: 5,
          flex: 2,
          borderWidth: 0,
          width: width * 0.9,
        }}>
        <Text style={{fontSize: 25, fontWeight: 'bold'}}>
          {badgerheadertitle}
        </Text>
        <View
          style={{
            borderWidth: 0,
            borderColor: 'red',
            flexDirection: 'row',
            backgroundColor: '#fff',
            borderRadius: 5,
            marginTop: height * 0.015,
            height: height * 0.11,
          }}>
          <View
            style={{
              flex: 1.7,
              backgroundColor: 'transparent',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View
              style={{
                width: width * 0.11,
                height: width * 0.11,

                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: width * 0.11 * 0.5,
                backgroundColor: backgroundcolor,
              }}>
              <Icon
                name={'bars'}
                type={'AntDesign'}
                style={{
                  color: '#fff',

                  alignSelf: 'center',
                  fontSize: height * 0.05,
                }}
              />
            </View>
          </View>
          <View
            style={{
              flex: 6.6,
              backgroundColor: 'transparent',
              justifyContent: 'center',
            }}>
            <Text style={{fontSize: 20}}>{badgetitle}</Text>
          </View>
          <View
            style={{
              flex: 1.7,
              backgroundColor: 'transparent',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text style={{fontSize: 22}}>{badgevalue}</Text>
            <Icon name="arrow-forward" style={{fontSize: height * 0.035}} />
          </View>
        </View>
      </View>
    );
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#f9f9f9', alignItems: 'center'}}>
        <StatusBar translucent backgroundColor={'transparent'} barStyle="light-content" />

        <ScrollView scrollEnabled showsVerticalScrollIndicator={false}>
          <CustomHeaderWithText
            text="JOURNEY"
            headerStyle={{backgroundColor: 'transparent'}}
            headerTextStyle={{color: 'black'}}
            iconStyle={{color: '#000'}}
          />

          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'space-between',
                backgroundColor: 'transparent',
                marginTop: 20,
                flex: 2,
                borderWidth: 0,
                width: width * 0.9,
                flexDirection: 'row',
              }}>
              <View
                style={{
                  flex: 4.5,
                  borderWidth: 0,
                  borderRadius: 5,
                  overflow: 'hidden',
                  backgroundColor: 'white',
                  height: height * 0.13,
                }}>
                {this.renderBadge('calendar', '#4db79b', 'Today', 2)}
              </View>
              <View
                style={{
                  flex: 1,
                  backgroundColor: 'transparent',
                  height: height * 0.13,
                }}></View>
              <View
                style={{
                  flex: 4.5,
                  backgroundColor: 'white',
                  borderWidth: 0,
                  borderRadius: 5,
                  overflow: 'hidden',
                  height: height * 0.13,
                }}>
                {this.renderBadge('mail', '#4ab865', 'Scheduled', 1)}
              </View>
            </View>

            <View
              style={{
                alignItems: 'center',
                justifyContent: 'space-between',
                backgroundColor: 'white',
                marginTop: height * 0.05,
                borderRadius: 5,
                flex: 2,
                borderWidth: 0,
                width: width * 0.9,
                flexDirection: 'row',
              }}>
              {this.renderBadge('mail', 'gray', 'All', 4)}
            </View>

            {this.renderTopicBadge('Leadership', 'Team-Building', 1,'#4ab865')}
            {this.renderTopicBadge('Performance', 'Productivity', 1,'pink')}
            {this.renderTopicBadge('Wellbeing', 'Mindfullness', 1,'orange')}

            <View style={{alignItems:'flex-end',justifyContent:'flex-end',width:width*0.9,backgroundColor:'transparent',flexDirection:'row',marginTop:height * 0.05}}>
              <TouchableOpacity
                onPress={() => Actions.scheduleSession()}
                style={{
                  padding: 5,
                  borderRadius: 3,
                  width:width*0.37,
                  borderWidth:1,
                  backgroundColor: 'transparent',
                  alignItems: 'center',
                  justifyContent: 'center',
                  alignSelf: 'center',
                }}>
                <Text style={{color: '#000', fontSize: 16}}>
                  {'FOCUS AREAS'}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
        <CustomFooter />
      </View>
    );
  }
}

export default Journey;

function chnagepage() {
  Actions.login();
}
