import React, {Component} from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  Modal,
  PanResponder,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import firebase from 'firebase';
import {
  Container,
  Header,
  Tab,
  Tabs,
  TabHeading,
  Icon,
  Content,
  List,
  ListItem,
  Thumbnail,
  Spinner,
} from 'native-base';
import {CustomHeaderWithText} from '../CommonViews';
import moment from 'moment';
import Animated from 'react-native-reanimated';
import CustomFooter from '../CommonViews/CustomFooter';
import requestCameraAndAudioPermission from '../VideoCall/permission';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
 class AllSessions extends Component {
  constructor(props) {
    super(props);
    // this.state = {sessionArray:[],isloading:false}
    this.state = {
      AppID: '19e5b00e21dc4d1b8fcffc52e7159fa7', //Set your APPID here
      isloading:false, sessionArray:[]
      //ChannelName: '', //Set a default channel or leave blank
    };
    
    if (Platform.OS === 'android') {
      //Request required permissions from Android
      requestCameraAndAudioPermission().then(_ => {
        console.log('requested!');
      });
    }

  }

  renderUpComingSessionGridRow() {
    debugger;
    return  this.props.sessionArray.length < 1 ? <Text style={{margin:10, textAlign:'center'}}>No upcoming session</Text>:
      this.props.sessionArray.map(el=>
        <View
        key={el.sessionid}
        style={{
          width: width * 0.99,
          borderColor: '#e0e0e0',
          borderWidth: 1,
          backgroundColor: 'white',
          borderRadius: 8,
          marginTop:1,
          marginBottom: 15,
          alignSelf: 'center',
          justifyContent: 'center',
          flexDirection: 'row',
          marginBottom: 10,
        }}>
        <View
          style={{
            flex: 6.5,
            backgroundColor: 'transparent',
            padding: 20,
          }}>
          <Text style={{fontSize: 14, marginTop: 1}}>{moment(el.date.toDate()).format('dddd DD MMM yyyy')}</Text>
          <Text style={{fontSize: 14, marginTop: 1}}>
            {`${el.timeWindow} w/${el.employeeName}`}
          </Text>
          <Text style={{fontSize: 14, marginBottom: 0, marginTop: 1}}>
            30 min
          </Text>
        </View>
        <View
          style={{
            flex: 3.5,
            backgroundColor: 'transparent',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            onPress={() =>  Actions.video({AppID:this.state.AppID, ChannelName:el.channelName})}
            style={{
              width: width * 0.23,
              height: height * 0.05,
              borderRadius: 5,
              borderWidth: 1,
              borderColor: '#c4c0c0',
              marginTop: 5,
              backgroundColor: 'transparent',
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
            }}>
            <Text style={{color: 'gray', fontSize: 14}}>
              {'JOIN'}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
        )
   
  
  }

    render() {
   
    /// source={{ uri: 'Image URL' }}
    return (
      <Container>
         <StatusBar translucent backgroundColor={'transparent'} barStyle="light-content" />
        <CustomHeaderWithText leftIcon={true} text={"Upcoming Sessions".toUpperCase()} />
        <Content>
          <List>
       {this.renderUpComingSessionGridRow()}
          </List>
        </Content>
      </Container>
    );
  }
}

export default AllSessions;