import React, {Component} from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  Modal,
  Alert,
  PanResponder,
  Linking
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {logOutUserSucess,logOut} from '../../actions'
import AsyncStorage from '@react-native-community/async-storage';
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Left,
  Body,
  Right,
  Thumbnail,
  Icon,
  Spinner,
} from 'native-base';
import firebase from 'firebase';
import {connect} from 'react-redux';
import {CustomHeaderWithText} from '../CommonViews';
import Animated from 'react-native-reanimated';
import CustomFooter from '../CommonViews/CustomFooter';
import Messsages from './Messsages';
// import CustomFooter from '../CommonViews/CustomFooter';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
class ClientProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {isloading: false};
    // ...
  }
  componentDidMount() {
    // Initialize Firebase
    // setTimeout(chnagepage, 2000);
    //chnagepage();
  }

   sendWhatsAppMessage = link => {
    if (link) {
     Linking.canOpenURL(link)
      .then(supported => {
        if (!supported) {
         Alert.alert(
           'Please install whats app to send direct message to Support team!'
         );
       } else {
         return Linking.openURL(link);
       }
     })
     .catch(err => console.error('An error occurred', err));
   } else {
     console.log('sendWhatsAppMessage -----> ', 'message link is undefined');
    }
   };


  removeItemValue = async (key) => {
    try {
      await AsyncStorage.removeItem(key);
      return true;
    } catch (exception) {
      return false;
    }
  };

  onpressListItem(el) {
    if (el == 'Sync Working Calender') {
      Actions.coachOnBoarding({fromProfile:true});
    }
    if (el == 'Change Password') {
      Actions.changePassword();
    }
    if(el== "Contact Support")
    {
      this.sendWhatsAppMessage('whatsapp://send?text=hello&phone=+447415336681');
    }
  }
  ReturnROw(el) {
    return (
      <ListItem key={el} onPress={() => this.onpressListItem(el)}>
        <Left>
          <Text>{el}</Text>
        </Left>
        <Right>
          <Icon name="arrow-forward" />
        </Right>
      </ListItem>
    );
  }

  onPressLoginButton() {
    // var me = this;
    // this.setState({isloading: true});
    // firebase
    //   .auth()
    //   .signOut()
    //   .then(function () {
    //     // Sign-out successful.
    //     me.props.logOutUserSucess()
    //     me.setState({isloading: false});
    //     Actions.reset('splashScreen');
    //   })
    //   .catch(function (error) {
    //     me.setState({isloading: true});
    //     // An error happened
    //     console.log('error', error);
    //   });
    this.props.logOut();
  }
  swapButtonAndSpinner() {
    return this.props.loading ? (
      <Spinner />
    ) : (
      <TouchableOpacity
        style={{
         // width: width * 0.5,
         // height: height * 0.08,
          borderBottomWidth: 1,
          borderBottomColor:'#b80f0a',
          marginTop: 30,
          //borderRadius: 5,
          justifyContent: 'center',
          alignItems: 'center',
          alignSelf: 'center',
          //backgroundColor: '#4ab865',
        }}
        onPress={() => this.onPressLoginButton()}>
        <Text style={{color: '#b80f0a',fontSize:14}}>LOGOUT</Text>
      </TouchableOpacity>
    );
  }

  render() {
    const {user} = this.props;
    let fullname = user ? `${user.firstName} ${user.lastName}`:'';
    return (
      <View style={{flex: 1, backgroundColor: '#f9f9f9', alignItems: 'center'}}>
        <StatusBar backgroundColor={'transparent'} translucent barStyle="light-content" />
        <CustomHeaderWithText  text="YOU" />
        <ScrollView scrollEnabled>
          <View
            style={{
              backgroundColor: '#f9f9f9',
              width: width * 0.95,
              justifyContent: 'center',
              alignItems: 'stretch',
              backgroundColor: 'transparent',
              flex: 1,
            }}>
            <View
              style={{
                borderBottomWidth: 2,
                margin: 10,
                height: height * 0.16,
                flexDirection: 'row',
              }}>
              <View
                style={{
                  flex: 7,
                  backgroundColor: 'transparent',
                  justifyContent: 'center',
                }}>
                <Text style={{fontSize: 18, fontWeight: 'bold'}}>
                 {fullname}
                </Text>
              <Text style={{color: 'gray'}}>{user ?user.email:''}</Text>
              </View>

              <View
                style={{
                  flex: 3,
                  backgroundColor: 'transparent',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <TouchableOpacity
                  onPress={() => Actions.editProfile()}
                  style={{
                    borderRadius: 1,
                    borderBottomWidth: 1,
                    borderBottomColor: '#4ab865',
                    marginTop: 5,
                    backgroundColor: 'transparent',
                    alignItems: 'center',
                    justifyContent: 'center',
                    alignSelf: 'center',
                  }}>
                  <Text
                    style={{color: '#4ab865', fontSize: 13, fontWeight: 'normal'}}>
                    {'Edit Profile'.toUpperCase()}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            {[
              'Change Password',
              'Contact Support',
            ].map((el) => this.ReturnROw(el))}
            {this.swapButtonAndSpinner()}
          </View>
        </ScrollView>
        <CustomFooter />
      </View>
    );
  }
}

//export default Profile;
const mapStateToProps = ({auth}) => {
  const {user,loading} = auth;

  return {user,loading};
};
export default connect(mapStateToProps, {logOutUserSucess,logOut})(ClientProfile);
function chnagepage() {
  Actions.login();
}
