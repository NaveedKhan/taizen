import React, { Component } from 'react';
import {Dimensions} from 'react-native';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail, Text, Spinner } from 'native-base';
import firebase from 'firebase';
import {connect} from 'react-redux';
import { Actions } from 'react-native-router-flux';
const image22 = require('../../Assets/coach.png');


const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
 class Messsages extends Component {
 
  constructor(props) {
    super(props);
    // this.state = {sessionArray:[],isloading:false}
    this.state = {
      AppID: '19e5b00e21dc4d1b8fcffc52e7159fa7', //Set your APPID here
      isloading:false, chatArray:[]
      //ChannelName: '', //Set a default channel or leave blank
    };
  }
  
  componentDidMount()
  {
    this.retriveChatData();
  }
  retriveChatData()
  {
    debugger;
    const {user} = this.props;
    const {id} = user;
    var firebaseConfig = {
      apiKey: "AIzaSyDlfb35etfsO24wlrghJ_dzpgIRSPhff94",
      authDomain: "taizen-cf195.firebaseapp.com",
      databaseURL: "https://taizen-cf195.firebaseio.com",
      projectId: "taizen-cf195",
      storageBucket: "taizen-cf195.appspot.com",
      messagingSenderId: "814929083198",
      appId: "1:814929083198:web:9465639048338ee3654a6f",
      measurementId: "G-3S2TR3Z0W1"
    };
    var me = this;
    this.setState({isloading:true});
    // Initialize Firebase
    !firebase.apps.length ? firebase.initializeApp(firebaseConfig): firebase.app();
    debugger;
    var db2 = firebase.firestore();
    firebase.firestore().settings({experimentalForceLongPolling: true});
    var result = []
    db2.collection("chats").where("users", "array-contains", id)//"1a392I0XGAWkS6TjOBvLArg9psB2")
    .onSnapshot(function(querySnapshot) {
      var result = []
         querySnapshot.forEach(function(doc) {
          debugger;
          var rec= {chatModelid:doc.id,...doc.data()};
             result.push(rec)
        });
        debugger;
      //   result.sort(function(a, b) {
      //     var c = new Date(a.date);
      //     var d = new Date(b.date);
      //     return c-d;
      // });
         me.setState({isloading:false,chatArray:result});
         //me.props.UpdateUserFormData({prop: 'upComingSessions', value: result});
}, function(error) {
    me.setState({isloading:false});
    console.log("Error getting documents: ", error);
  });  



    // .then(function(querySnapshot) {
    //   debugger;  
    //   querySnapshot.forEach(function(doc) {
    //       debugger;
    //       var rec= {chatModelid:doc.id,...doc.data()};
    //          result.push(rec)
    //     });
    //     debugger;
    //   //   result.sort(function(a, b) {
    //   //     var c = new Date(a.date);
    //   //     var d = new Date(b.date);
    //   //     return c-d;
    //   // });
    //      me.setState({isloading:false,chatArray:result});
    //      //me.props.UpdateUserFormData({prop: 'upComingSessions', value: result});
    // })
    // .catch(function(error) {
    //   debugger;
    //   me.setState({isloading:false});
    //     console.log("Error getting documents: ", error);
    // });
  }

  ReturnROw(el)
  {
    const {user} = this.props;
    if(user)
    {
      var reciverDetail = el.usersInfo.find(aa=>aa.id != user.id);
      var image = {uri:reciverDetail.picture};
      console.log('user pic url',image);
      console.log('user pic url2',image22);
       const {firstName,lastName} = reciverDetail;
      let fullname = `${firstName} ${lastName}`;
         return   <ListItem
         onPress={()=> Actions.chat({chatDetail:el,reciverDetail:reciverDetail})}
         style={{height:height*0.13}} key={reciverDetail.id} avatar>
         <Left>
          {reciverDetail.picture ? <Thumbnail source={image} />:<Thumbnail source={image22} /> } 
         </Left>
         <Body>
     <Text>{fullname}</Text>
           <Text note>{el.lastMessage} </Text>
         </Body>
         <Right>
           {/* <Text note>3:43 pm</Text> */}
         </Right>
       </ListItem>
    }
 
  }
    render() {
   
    /// source={{ uri: 'Image URL' }}
    return (
      <Container style={{flex:1}}>
        <Content>
          <List>
         { this.state.isloading ? <Spinner style={{alignSelf:'center',margin:20}}/> :
          this.state.chatArray.length <1 ? <Text style={{margin:20,alignSelf:'center'}}>Your inbox is empty</Text> :this.state.chatArray.map(el=>
            this.ReturnROw(el)
            )}
          </List>
        </Content>
      </Container>
    );
  }
}

//export default Messsages;
const mapStateToProps = ({auth}) => {
  const {user,selectedCoach} = auth;

  return {user,selectedCoach};
};
export default connect(mapStateToProps, null)(Messsages);