import React, { Component } from 'react';
import {Alert, Dimensions,StatusBar} from 'react-native';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail, Text, Spinner, View, Input, Button } from 'native-base';
import firebase from 'firebase';
import {connect} from 'react-redux';
import { CustomHeaderWithText } from '../CommonViews';
import { ScrollView, TouchableHighlight, TouchableOpacity } from 'react-native-gesture-handler';
const image22 = require('../../Assets/coach.png');

const fcmChannelID = 'fcm_default_channel';
 
// This is the Firebase Server Key (for test purposes only - DO NOT SHARE IT!)
// cfr: https://www.ryadel.com/en/react-native-push-notifications-setup-firebase-4/
const firebase_server_key = 'AAAAvb2O-z4:APA91bGpw6vAShfjrmdDMy2RY8DSsGAThxhivpQh5NgpxRklT9xl82aA4qSK1j-lCj3NXgGywdg2ZpdeKxFgVVAaaOwPLuhjAluyk6DLKLJK1BuLHZewsIQndTnb8B8DfCeU-W-EcKvS';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
 class Chat extends Component {
 
  constructor(props) {
    super(props);
    // this.state = {sessionArray:[],isloading:false}
    this.arr = [];
    this.state = {
      AppID: '19e5b00e21dc4d1b8fcffc52e7159fa7', //Set your APPID here
      isloading:false, chatArray:[],chatThreadsArray:[],
      senderDetail:this.props.senderDetail,
      chatDetail:this.props.chatDetail,
      reciverDetail:this.props.reciverDetail,
      isSendingMessage:false,
      message:''
      //ChannelName: '', //Set a default channel or leave blank
    };
    this.scrollview_ref=null
  }

  sendToServer = async(title,str,deviceToken) => {
    console.log("sendToServer");
    console.log(str);
    console.log('coach devicetocken',deviceToken);
    // SEND NOTIFICATION THROUGH FIREBASE
    // Workflow: React -> Firebase -> Target Devices
 if(deviceToken){
    fetch('https://fcm.googleapis.com/fcm/send', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'key=' + firebase_server_key,
      },
      body: JSON.stringify({
        "registration_ids":[
          deviceToken
        ],
        "notification": {
            "title":title,
            "body":str
        },
        "data": {
          "key1" : "value1",
          "key2" : "value2",
          "key3" : 23.56565,
          "key4" : true
        }
      }),
    })
    .then((response) => {
      console.log("Request sent!");
      console.log(response);
     // console.log("FCM Token: " + this.state.firebase_messaging_token  );
      console.log("Message: " + str);
      //this.setState({ firebase_send: '' });
    })
    .catch((error) => {
      console.log("Request Not sent!");
      console.error(error);
    });
 
 }else
 {
   console.log('coach device id is null')
 }
 
  }

  updateChatDataOnFirebase(me,lastmessage) {
    let {chatDetail,user,reciverDetail} = me.props;
    
    let {chatModelid} = chatDetail;
    var db = firebase.firestore();
    firebase.firestore().settings({experimentalForceLongPolling: true});
    var washingtonRef = db
      .collection('chats')
      .doc(chatModelid);

    // Set the "capital" field of the city 'DC'
    return washingtonRef
      .update({
        lastMessage:lastmessage
      })
      .then(function () {
        debugger;
        me.setState({isSendingMessage:false})
        me.sendToServer('Message From Coach',lastmessage,reciverDetail.deviceToken)
      })
      .catch(function (error) {
        // The document probably doesn't exist.
        debugger;
        me.setState({isSendingMessage:false})
        console.error('Error updating document: ', error);
      });
  }

  addchatMessageintoDb()
  {
    var me  =this;
    debugger;
   let {chatDetail,user} = this.props;
   let {chatModelid} = chatDetail;

   this.setState({isSendingMessage:true})
   const {firstName,lastName} = user;
   let fullname = `${firstName} ${lastName}`;
    var db = firebase.firestore();
    firebase.firestore().settings({experimentalForceLongPolling: true})
    db.collection("chats").doc(chatModelid).collection('thread').add({
      content:me.state.message,
      senderID:user.id,
      senderName:fullname,
      timeStamp: new Date()
  })
  .then(function(docRef) {
    debugger;
      console.log("Document written with ID: ", docRef);
      // me.setState({isSendingMessage:false});
      me.updateChatDataOnFirebase(me,me.state.message);
  })
  .catch(function(error) {
      console.error("Error adding document: ", error);
      me.setState({isSendingMessage:false,modalVisible:false});
  });
  }

  componentDidMount()
  {
    this.retriveChatData();
  }
  retriveChatData()
  {
    debugger;
    const {user,chatDetail} = this.props;
    let {chatModelid} = chatDetail;

    const {id} = user;
    var firebaseConfig = {
      apiKey: "AIzaSyDlfb35etfsO24wlrghJ_dzpgIRSPhff94",
      authDomain: "taizen-cf195.firebaseapp.com",
      databaseURL: "https://taizen-cf195.firebaseio.com",
      projectId: "taizen-cf195",
      storageBucket: "taizen-cf195.appspot.com",
      messagingSenderId: "814929083198",
      appId: "1:814929083198:web:9465639048338ee3654a6f",
      measurementId: "G-3S2TR3Z0W1"
    };
    var me = this;
    this.setState({isloading:true});
    // Initialize Firebase
    !firebase.apps.length ? firebase.initializeApp(firebaseConfig): firebase.app();
    debugger;
    var db2 = firebase.firestore();
    firebase.firestore().settings({experimentalForceLongPolling: true});
    
    db2.collection("chats").doc(chatModelid).collection('thread')//where("users", "array-contains", id)//"1a392I0XGAWkS6TjOBvLArg9psB2")
    .onSnapshot(function(querySnapshot) {
      var result = []
      querySnapshot.forEach(function(doc) {
        debugger;
        var rec= {threadId:doc.id,...doc.data()};
           result.push(rec)
      });
      result.sort(function(a,b){
        // Turn your strings into dates, and then subtract them
        // to get a value that is either negative, positive, or zero.
        return new Date(a.timeStamp.toDate()) - new Date(b.timeStamp.toDate());
      });
      me.setState({isloading:false,chatThreadsArray:result});
     var aa= result.length-2
      setTimeout(
        () =>
          me.scrollview_ref.scrollTo({
            x: 0,
            y: result.length < 2?0: me.arr[result.length-2],
            animated: true,
          }),
        800,
      );
  }, function(error) {
    me.setState({isloading:false});
    console.log("Error getting documents: ", error);
  });
  }



  ReturnROw(el)
  {
    const {user} = this.props;
    var reciverDetail = el.usersInfo.find(aa=>aa.id != user.id);
   var image = {uri:reciverDetail.picture};
   console.log('user pic url',image);
   console.log('user pic url2',image22);
    const {firstName,lastName} = reciverDetail;
   let fullname = `${firstName} ${lastName}`;
      return   <ListItem style={{height:height*0.13}} key={reciverDetail.id} avatar>
      <Left>
       {reciverDetail.picture ? <Thumbnail source={image} />:<Thumbnail source={image22} /> } 
      </Left>
      <Body>
  <Text>{fullname}</Text>
        <Text note>{el.lastMessage} </Text>
      </Body>
      <Right>
        {/* <Text note>3:43 pm</Text> */}
      </Right>
    </ListItem>
  }
    render() {
   const {reciverDetail,user} = this.props;
   const {firstName,lastName} = reciverDetail;

    /// source={{ uri: 'Image URL' }}
    return (
      <Container>
        <StatusBar backgroundColor={'transparent'} translucent barStyle="light-content" />
        <CustomHeaderWithText leftIcon={true} text={`${firstName} ${lastName}`}/>
      <ScrollView showsVerticalScrollIndicator={false} ref={(ref) => {
              this.scrollview_ref = ref;
            }} scrollEnabled>
        <View style={{flex:1,backgroundColor:'transparent',marginBottom:30}}>
{this.state.isloading? <Spinner/> : 
<View>
  {this.state.chatThreadsArray.map((thr,index)=>
   <View 
   onLayout={(event) => {
    const layout = event.nativeEvent.layout;
    this.arr[index] = layout.y;
    console.log('height:', layout.height);
    console.log('width:', layout.width);
    console.log('x:', layout.x);
    console.log('y:', layout.y);
  }}
   key={index} style={{padding:5,
    margin:5,marginTop:10,borderRadius:10,paddingBottom:10,paddingTop:10,
    borderBottomRightRadius:thr.senderID == user.id ?2:10,
    borderBottomLeftRadius:thr.senderID == user.id ?10:2,
   alignSelf:thr.senderID == user.id ?'flex-end':'flex-start',
    backgroundColor: thr.senderID == user.id ? '#4ab865':'#e9e9e9',borderWidth:0}}>
 <Text style={{margin:5,alignSelf:thr.senderID == user.id ?'flex-end':'flex-start'}}>{thr.content}</Text>
   </View>
    )}
</View>

}
        </View>
        </ScrollView>
      
        <View style={{flexDirection:'row',borderTopWidth:1,borderTopColor:'gray',
        height:height*0.09}}>
<View style={{flex:1,backgroundColor:'transparent',justifyContent:'center',alignItems:'center'}}>
  <Text style={{alignSelf:'center'}}>Aa</Text>
</View>
<View style={{flex:7,backgroundColor:'transparent'}}>
  <Input  value={this.state.message} 
  onChangeText={(text)=> this.setState({message:text})}/>
</View>
<View style={{flex:2,backgroundColor:'transparent',alignItems:'center',justifyContent:'center'}}>
   {this.state.isSendingMessage ? <Spinner/> : 
        <Button
        onPress={() =>  this.addchatMessageintoDb()}
        style={{
          //width: width * 0.23,
          //height: height * 0.05,
          //borderRadius: 5,
          borderWidth: 0,
          borderColor: 'red',
         // marginTop: 5,
          backgroundColor: 'white',
          alignItems: 'center',
          justifyContent: 'center',
          alignSelf: 'center',
        }}>
        <Text style={{color: 'gray', fontSize: 14,}}>
          {'SEND'}
        </Text>
      </Button>
    }
</View>
        </View>
      </Container>
    );
  }
}

//export default Messsages;
const mapStateToProps = ({auth}) => {
  const {user,selectedCoach} = auth;

  return {user,selectedCoach};
};
export default connect(mapStateToProps, null)(Chat);