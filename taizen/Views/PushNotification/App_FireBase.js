import React, {Fragment} from 'react';
import {
  Alert,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TextInput,
  Button,
  StatusBar,
  Clipboard,
  Platform
} from 'react-native';
 
import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
 
import firebase, { RNFirebase } from 'react-native-firebase';



// This is the default FCM Channel Name (required by Android)
const fcmChannelID = 'fcm_default_channel';
 
// This is the Firebase Server Key (for test purposes only - DO NOT SHARE IT!)
// cfr: https://www.ryadel.com/en/react-native-push-notifications-setup-firebase-4/
const firebase_server_key = 'AAAAvb2O-z4:APA91bGpw6vAShfjrmdDMy2RY8DSsGAThxhivpQh5NgpxRklT9xl82aA4qSK1j-lCj3NXgGywdg2ZpdeKxFgVVAaaOwPLuhjAluyk6DLKLJK1BuLHZewsIQndTnb8B8DfCeU-W-EcKvS';


export default class App_FireBase extends React.Component {
    constructor() {
      super();
      this.state = { 
        firebase_messaging_token: '',
        firebase_messaging_message: '',
        firebase_notification: '',
        firebase_send: ''
      };
    }
   
    async componentDidMount() {
      this.createNotificationChannel();
      this.checkNotificationPermissions();
      this.addNotificationListeners();
    }
   
    componentWillUnmount() {
      //this.removeNotificationListeners();
    }


    createNotificationChannel = () => {
        console.log("createNotificationChannel");
        // Build a android notification channel
        const channel = new firebase.notifications.Android.Channel(
          fcmChannelID, // channelId
          "FCM Default Channel", // channel name
          firebase.notifications.Android.Importance.High // channel importance
        ).setDescription("Test Channel"); // channel description
        // Create the android notification channel
        firebase.notifications().android.createChannel(channel);
      };

      checkNotificationPermissions() {
        console.log("checkNotificationPermissions");
        // show token
        firebase.messaging().hasPermission()
          .then(enabled => {
            if (enabled) {
              console.log('user has notification permission')
              this.setToken();
            } else {
              console.log('user does not have notification permission')
              firebase.messaging().requestPermission()
                .then((result) => {
                  if (result) {
                    this.setToken();
                  }
                  else {
                  }
                });
            }
          });
      }
     
      setToken(token) {
        console.log("setToken");
        firebase.messaging().getToken().then((token) => {
          this.setState({ firebase_messaging_token: token });
          console.log(token);
        });
      }


      addNotificationListeners() {
        console.log("receiveNotifications");
        this.messageListener = firebase.messaging().onMessage((message) => {
          // "Headless" Notification
          console.log("onMessage");
        });
     
        this.notificationInitialListener = firebase.notifications().getInitialNotification().then((notification) => {
              if (notification) {
                // App was opened by a notification
                // Get the action triggered by the notification being opened
                // Get information about the notification that was opened
                console.log("onInitialNotification");
              }
          });
     
        this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification) => {
          console.log("onNotificationDisplayed");
        });
     
        this.notificationListener = firebase.notifications().onNotification((notification) => {
          console.log("onNotification");
          notification.android.setChannelId(fcmChannelID);
          firebase.notifications().displayNotification(notification).catch((err) => {
            console.log(err);
          });
     
          // Process your notification as required
     
          // #1: draw in View
          var updatedText = this.state.firebase_notification + "\n" +
            "[" + new Date().toLocaleString() + "]" + "\n" + 
            notification.title + ":" + notification.body + "\n";
     
          this.setState({ firebase_notification: updatedText });
        });
     
        this.tokenRefreshListener = firebase.messaging().onTokenRefresh(fcmToken => {
          // Process your token as required
          console.log("onTokenRefresh");
        });  
      }
     
      removeNotificationListeners() {
        this.messageListener();
        this.notificationInitialListener();
        this.notificationDisplayedListener();
        this.notificationListener();
        this.tokenRefreshListener();    
      }


      sendToServer = async(str) => {
        console.log("sendToServer");
        console.log(str);
     
        // SEND NOTIFICATION THROUGH FIREBASE
        // Workflow: React -> Firebase -> Target Devices
     
        fetch('https://fcm.googleapis.com/fcm/send', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'key=' + firebase_server_key,
          },
          body: JSON.stringify({
            "registration_ids":[
              this.state.firebase_messaging_token
            ],
            "notification": {
                "title":"Title",
                "body":str
            },
            "data": {
              "key1" : "value1",
              "key2" : "value2",
              "key3" : 23.56565,
              "key4" : true
            }
          }),
        })
        .then((response) => {
          console.log("Request sent!");
          console.log(response);
          console.log("FCM Token: " + this.state.firebase_messaging_token  );
          console.log("Message: " + str);
          this.setState({ firebase_send: '' });
        })
        .catch((error) => {
          console.error(error);
        });
      }

      writeToClipboard = async(str) => {
        console.log("writeToClipboard");
        await Clipboard.setString(str);
        // raise a popup message to allow "copy" from Android & IOS devices
        alert(str); 
      }


      render() {
        return (
          <Fragment>
          <StatusBar barStyle="dark-content" />
          <SafeAreaView>
            <ScrollView
              contentInsetAdjustmentBehavior="automatic"
              style={styles.scrollView}>
              {global.HermesInternal == null ? null : (
                <View style={styles.engine}>
                  <Text style={styles.footer}>Engine: Hermes</Text>
                </View>
              )}
              <View style={styles.body}>
              <View style={styles.sectionContainer}>
                  <Text style={styles.sectionTitle}>Notification Test View</Text>
     
                  <Text style={styles.modulesHeader}>Firebase modules currently installed:</Text>
                  {firebase.admob.nativeModuleExists && <Text style={styles.module}>admob()</Text>}
                  {firebase.analytics.nativeModuleExists && <Text style={styles.module}>analytics()</Text>}
                  {firebase.auth.nativeModuleExists && <Text style={styles.module}>auth()</Text>}
                  {firebase.config.nativeModuleExists && <Text style={styles.module}>config()</Text>}
                  {firebase.crashlytics.nativeModuleExists && <Text style={styles.module}>crashlytics()</Text>}
                  {firebase.database.nativeModuleExists && <Text style={styles.module}>database()</Text>}
                  {firebase.firestore.nativeModuleExists && <Text style={styles.module}>firestore()</Text>}
                  {firebase.functions.nativeModuleExists && <Text style={styles.module}>functions()</Text>}
                  {firebase.iid.nativeModuleExists && <Text style={styles.module}>iid()</Text>}
                  {firebase.links.nativeModuleExists && <Text style={styles.module}>links()</Text>}
                  {firebase.messaging.nativeModuleExists && <Text style={styles.module}>messaging()</Text>}
                  {firebase.notifications.nativeModuleExists && <Text style={styles.module}>notifications()</Text>}
                  {firebase.perf.nativeModuleExists && <Text style={styles.module}>perf()</Text>}
                  {firebase.storage.nativeModuleExists && <Text style={styles.module}>storage()</Text>}
                  <Text value={'\n\n'} />
                  <Text style={styles.highlight}>FCM Token:</Text>
                  <Text>{this.state.firebase_messaging_token}</Text>
                  <Button onPress={() => this.writeToClipboard(this.state.firebase_messaging_token)} title="Copy to Cliboard" />
                  <Text value={'\n\n'} />
                  <Text style={styles.highlight}>Send Notification:</Text>
                  <TextInput style={styles.textInput} value={this.state.firebase_send} onChangeText={(txt) => this.setState({firebase_send: txt})} />
                  <Button onPress={() => this.sendToServer(this.state.firebase_send)} title="Send and Receive" />
                  <Text value={'\n\n'} />
                  <Text style={styles.highlight}>Received Notification:</Text>
                  <Text>{this.state.firebase_notification}</Text>
     
                </View>
              </View>
            </ScrollView>
          </SafeAreaView>
        </Fragment>
          );
      }


}


const styles = StyleSheet.create({
    scrollView: {
      backgroundColor: Colors.lighter,
    },
    engine: {
      position: 'absolute',
      right: 0,
    },
    body: {
      backgroundColor: Colors.white,
    },
    sectionContainer: {
      marginTop: 32,
      paddingHorizontal: 24,
    },
    sectionTitle: {
      fontSize: 24,
      fontWeight: '600',
      color: Colors.black,
    },
    sectionDescription: {
      marginTop: 8,
      fontSize: 18,
      fontWeight: '400',
      color: Colors.dark,
    },
    highlight: {
      fontWeight: '700',
    },
    footer: {
      color: Colors.dark,
      fontSize: 12,
      fontWeight: '600',
      padding: 4,
      paddingRight: 12,
      textAlign: 'right',
    },
    modules: {
      margin: 20,
    },
    modulesHeader: {
      fontSize: 16,
      marginBottom: 8,
    },
    module: {
      fontSize: 14,
      marginTop: 4,
      textAlign: 'center',
    },
    lineStyle:{
      borderWidth: 0.5,
      borderColor:'black',
      marginTop:10,
      marginBottom:10,
    },
    textInput:{
      borderWidth: 0.5,
      borderColor:'black',
      marginTop: 0,
      marginBottom: 0,
    }
  });