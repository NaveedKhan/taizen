import React, {Component} from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  StatusBar,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {
  Container,
  Header,
  Tab,
  Tabs,
  TabHeading,
  Icon,
  Thumbnail,
  Spinner,
} from 'native-base';
import firebase from 'firebase';
import {CustomHeaderWithText} from '../CommonViews';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

class PickCoach extends Component {
  constructor(props) {
    super(props);
    // ...
    this.state = {isloading: false, coachArr: [], selectedCoach: null};
  }

  retrieveCoachUsers() {
    debugger;
    // const {user} = this.props;
    // const {id} = user;
    var firebaseConfig = {
      apiKey: 'AIzaSyDlfb35etfsO24wlrghJ_dzpgIRSPhff94',
      authDomain: 'taizen-cf195.firebaseapp.com',
      databaseURL: 'https://taizen-cf195.firebaseio.com',
      projectId: 'taizen-cf195',
      storageBucket: 'taizen-cf195.appspot.com',
      messagingSenderId: '814929083198',
      appId: '1:814929083198:web:9465639048338ee3654a6f',
      measurementId: 'G-3S2TR3Z0W1',
    };
    var me = this;
    this.setState({isloading: true});
    // Initialize Firebase
    !firebase.apps.length
      ? firebase.initializeApp(firebaseConfig)
      : firebase.app();
    debugger;
    var db2 = firebase.firestore();
    firebase.firestore().settings({experimentalForceLongPolling: true});
    var result = [];
    db2
      .collection('users')
      .where('userType', '==', 'Coach') //"1a392I0XGAWkS6TjOBvLArg9psB2")
      .get()
      .then(function (querySnapshot) {
        debugger;
        querySnapshot.forEach(function (doc) {
          //debugger;
          var rec = doc.data();
          if (rec.forDemo) {
            debugger;
            result.push(rec);
          }
          // doc.data() is never undefined for query doc snapshots
          console.log(doc.id, ' => ', doc.data());
        });
        debugger;
        console.log(' result', result);
        me.setState({
          isloading: false,
          selectedCoach: result[0],
          coachArr: result,
        });
      })
      .catch(function (error) {
        debugger;
        me.setState({isloading: false});
        console.log('Error getting documents: ', error);
      });
  }
  componentDidMount() {
    //    /* <TouchableOpacity  style={{backgroundColor:'black',overflow:'hidden',height:width*0.17,borderRadius:width*0.17*0.5,width:width*0.17,margin:5}}>
    //    <Image source={ require('../../Assets/onBoardimage3.png')} style={{height:width*0.17,borderRadius:width*0.17*0.5,width:width*0.17,resizeMode:'stretch'}}/>
    //  </TouchableOpacity> */
    this.retrieveCoachUsers();
  }

  renderButton(buttonText, el) {
    return (
      <TouchableOpacity
        onPress={() => Actions.scheduleSession({selectedCoach: el})}
        style={{
          width: width * 0.95,
          height: height * 0.08,
          borderRadius: 3,
          backgroundColor: '#4ab865',
          alignItems: 'center',
          justifyContent: 'center',
          alignSelf: 'center',
        }}>
        <Text style={{color: '#fff', fontSize: 18}}>{buttonText}</Text>
      </TouchableOpacity>
    );
  }

  returnImageNameforExperties(el) {
    if (el.replace(" ","").toUpperCase() === 'Anxiety'.toUpperCase()) {
      return require('../../Assets/expertise/Anxiety.png');
    } else if (el.replace(" ","").toUpperCase() === 'ChangeManagement'.toUpperCase()) {
      return require('../../Assets/expertise/ChangeManagement.png');
    }else if (el.replace(" ","").toUpperCase() === 'Collaboration'.toUpperCase()) {
      return require('../../Assets/expertise/Collaboration.png');
    }else if (el.replace(" ","").toUpperCase() === 'Communication'.toUpperCase()) {
      return require('../../Assets/expertise/Communication.png');
    }else if (el.replace(" ","").toUpperCase() === 'Confidence'.toUpperCase()) {
      return require('../../Assets/expertise/Confidence.png');
    }else if (el.replace(" ","").toUpperCase() === 'Conflict'.toUpperCase()) {
      return require('../../Assets/expertise/Conflict.png');
    }else if (el.replace(" ","").toUpperCase() === 'ConflictManagement'.toUpperCase()) {
      return require('../../Assets/expertise/ConflictManagement.png');
    }else if (el.replace(" ","").toUpperCase() === 'Creativity'.toUpperCase()) {
      return require('../../Assets/expertise/Creativity.png');
    }else if (el.replace(" ","").toUpperCase() === 'CrisisManagement'.toUpperCase()) {
      return require('../../Assets/expertise/CrisisManagement.png');
    }else if (el.replace(" ","").toUpperCase() === 'Delegation'.toUpperCase()) {
      return require('../../Assets/expertise/Delegation.png');
    }else if (el.replace(" ","").toUpperCase() === 'GoalSetting'.toUpperCase()) {
      return require('../../Assets/expertise/GoalSetting.png');
    }else if (el.replace(" ","").toUpperCase() === 'Gratitude'.toUpperCase()) {
      return require('../../Assets/expertise/Gratitude.png');
    }else if (el.replace(" ","").toUpperCase() === 'Group'.toUpperCase()) {
      return require('../../Assets/expertise/Group.png');
    }else if (el.replace(" ","").toUpperCase() === 'InspiringOthers'.toUpperCase()) {
      return require('../../Assets/expertise/InspiringOthers.png');
    }else if (el.replace(" ","").toUpperCase() === 'IntelligenceGathering'.toUpperCase()) {
      return require('../../Assets/expertise/IntelligenceGathering.png');
    }else if (el.replace(" ","").toUpperCase() === 'Leadership'.toUpperCase()) {
      return require('../../Assets/expertise/Leadership.png');
    }else if (el.replace(" ","").toUpperCase() === 'LearnedOptimism'.toUpperCase()) {
      return require('../../Assets/expertise/LearnedOptimism.png');
    }else if (el.replace(" ","").toUpperCase() === 'Mindfulness'.toUpperCase()) {
      return require('../../Assets/expertise/Mindfulness.png');
    }else if (el.replace(" ","").toUpperCase() === 'Motivation'.toUpperCase()) {
      return require('../../Assets/expertise/Motivation.png');
    }else if (el.replace(" ","").toUpperCase() === 'Networking'.toUpperCase()) {
      return require('../../Assets/expertise/Networking.png');
    }else if (el.replace(" ","").toUpperCase() === 'Organisation'.toUpperCase()) {
      return require('../../Assets/expertise/Organisation.png');
    }else if (el.replace(" ","").toUpperCase() === 'PassionRediscovery'.toUpperCase()) {
      return require('../../Assets/expertise/PassionRediscovery.png');
    }else if (el.replace(" ","").toUpperCase() === 'Perseverance'.toUpperCase()) {
      return require('../../Assets/expertise/Perseverance.png');
    }else if (el.replace(" ","").toUpperCase() === 'ProcessImprovement'.toUpperCase()) {
      return require('../../Assets/expertise/ProcessImprovement.png');
    }else if (el.replace(" ","").toUpperCase() === 'Productivity'.toUpperCase()) {
      return require('../../Assets/expertise/Productivity.png');
    }else if (el.replace(" ","").toUpperCase() === 'ProjectManagement'.toUpperCase()) {
      return require('../../Assets/expertise/ProjectManagement.png');
    }else if (el.replace(" ","").toUpperCase() === 'RelationshipManagement'.toUpperCase()) {
      return require('../../Assets/expertise/RelationshipManagement.png');
    }else if (el.replace(" ","").toUpperCase() === 'RiskTaking'.toUpperCase()) {
      return require('../../Assets/expertise/RiskTaking.png');
    }else if (el.replace(" ","").toUpperCase() === 'SelfAwareness'.toUpperCase()) {
      return require('../../Assets/expertise/SelfAwareness.png');
    }else if (el.replace(" ","").toUpperCase() === 'Self-InducedObstacles'.toUpperCase()) {
      return require('../../Assets/expertise/Self-InducedObstacles.png');
    }else if (el.replace(" ","").toUpperCase() === 'Sleep'.toUpperCase()) {
      return require('../../Assets/expertise/Sleep.png');
    }else if (el.replace(" ","").toUpperCase() === 'StrategicDecisionMaking'.toUpperCase()) {
      return require('../../Assets/expertise/StrategicDecisionMaking.png');
    }else if (el.replace(" ","").toUpperCase() === 'SuccessionPlanning'.toUpperCase()) {
      return require('../../Assets/expertise/SuccessionPlanning.png');
    }else if (el.replace(" ","").toUpperCase() === 'TeamBuilding'.toUpperCase()) {
      return require('../../Assets/expertise/TeamBuilding.png');
    }else if (el.replace(" ","").toUpperCase() === 'TeamBuy-In'.toUpperCase()) {
      return require('../../Assets/expertise/TeamBuy-In.png');
    }else if (el.replace(" ","").toUpperCase() === 'TimeManagement'.toUpperCase()) {
      return require('../../Assets/expertise/TimeManagement.png');
    }else if (el.replace(" ","").toUpperCase() === 'Transitions'.toUpperCase()) {
      return require('../../Assets/expertise/Transitions.png');
    }else if (el.replace(" ","").toUpperCase() === 'ValueAlignment'.toUpperCase()) {
      return require('../../Assets/expertise/ValueAlignment.png');
    }else if (el.replace(" ","").toUpperCase() === 'Work-LifeBalance'.toUpperCase()) {
      return require('../../Assets/expertise/Work-LifeBalance.png');
    } 
    else{
      return require('../../Assets/expertise/Anxiety.png');
    }
  }

  renderExpertieImage(el) {
    var image = this.returnImageNameforExperties(el);
    return (
      <Image
        style={{
          height: height * 0.05,
          width: height * 0.05,
          alignSelf: 'center',
          resizeMode: 'contain',
        }}
        source={image}
      />
    );
  }


  returnImageNameforRoleExperience(el)
  {
    if (el.replace(" ","").toUpperCase() === 'Administration'.toUpperCase()) {
      return require('../../Assets/role-experience/Administration.png');
    }else if (el.replace(" ","").toUpperCase() === 'BiotechPharmaMedicineClinical'.toUpperCase()) {
      return require('../../Assets/role-experience/BiotechPharmaMedicineClinical.png');
    }else if (el.replace(" ","").toUpperCase() === 'BuildingStartups'.toUpperCase()) {
      return require('../../Assets/role-experience/BuildingStartups.png');
    }else if (el.replace(" ","").toUpperCase() === 'Compliance'.toUpperCase()) {
      return require('../../Assets/role-experience/Compliance.png');
    }else if (el.replace(" ","").toUpperCase() === 'Consultancy'.toUpperCase()) {
      return require('../../Assets/role-experience/Consultancy.png');
    }else if (el.replace(" ","").toUpperCase() === 'Consulting'.toUpperCase()) {
      return require('../../Assets/role-experience/Consulting.png');
    }else if (el.replace(" ","").toUpperCase() === 'Creative&Design'.toUpperCase()) {
      return require('../../Assets/role-experience/Creative&Design.png');
    }else if (el.replace(" ","").toUpperCase() === 'CustomerRelations'.toUpperCase()) {
      return require('../../Assets/role-experience/CustomerRelations.png');
    }else if (el.replace(" ","").toUpperCase() === 'DataScience'.toUpperCase()) {
      return require('../../Assets/role-experience/DataScience.png');
    }else if (el.replace(" ","").toUpperCase() === 'Digitalisation'.toUpperCase()) {
      return require('../../Assets/role-experience/Digitalisation.png');
    }else if (el.replace(" ","").toUpperCase() === 'Engineering'.toUpperCase()) {
      return require('../../Assets/role-experience/Engineering.png');
    }else if (el.replace(" ","").toUpperCase() === 'Finance&Accounting'.toUpperCase()) {
      return require('../../Assets/role-experience/Finance&Accounting.png');
    }else if (el.replace(" ","").toUpperCase() === 'Health&Safety'.toUpperCase()) {
      return require('../../Assets/role-experience/Health&Safety.png');
    }else if (el.replace(" ","").toUpperCase() === 'HumanResources'.toUpperCase()) {
      return require('../../Assets/role-experience/HumanResources.png');
    }else if (el.replace(" ","").toUpperCase() === 'Legal'.toUpperCase()) {
      return require('../../Assets/role-experience/Legal.png');
    }else if (el.replace(" ","").toUpperCase() === 'Logistics'.toUpperCase()) {
      return require('../../Assets/role-experience/Logistics.png');
    }else if (el.replace(" ","").toUpperCase() === 'Management&BusinessLeadership'.toUpperCase()) {
      return require('../../Assets/role-experience/Management&BusinessLeadership.png');
    }else if (el.replace(" ","").toUpperCase() === 'Marketing&Advertising'.toUpperCase()) {
      return require('../../Assets/role-experience/Marketing&Advertising.png');
    }else if (el.replace(" ","").toUpperCase() === 'ProductManagement&Strategy'.toUpperCase()) {
      return require('../../Assets/role-experience/ProductManagement&Strategy.png');
    }else if (el.replace(" ","").toUpperCase() === 'Programming'.toUpperCase()) {
      return require('../../Assets/role-experience/Programming.png');
    }else if (el.replace(" ","").toUpperCase() === 'ProjectManagement&Operations'.toUpperCase()) {
      return require('../../Assets/role-experience/ProjectManagement&Operations.png');
    }else if (el.replace(" ","").toUpperCase() === 'ProjectManagementandOperations'.toUpperCase()) {
      return require('../../Assets/role-experience/ProjectManagementandOperations.png');
    }else if (el.replace(" ","").toUpperCase() === 'PublicRelations'.toUpperCase()) {
      return require('../../Assets/role-experience/PublicRelations.png');
    }else if (el.replace(" ","").toUpperCase() === 'Purchasing'.toUpperCase()) {
      return require('../../Assets/role-experience/Purchasing.png');
    }else if (el.replace(" ","").toUpperCase() === 'QualityAssurance'.toUpperCase()) {
      return require('../../Assets/role-experience/QualityAssurance.png');
    }else if (el.replace(" ","").toUpperCase() === 'Sales&BusinessDevelopment'.toUpperCase()) {
      return require('../../Assets/role-experience/Sales&BusinessDevelopment.png');
    }else if (el.replace(" ","").toUpperCase() === 'Science&Research'.toUpperCase()) {
      return require('../../Assets/role-experience/Science&Research.png');
    }else if (el.replace(" ","").toUpperCase() === 'SocialSciences'.toUpperCase()) {
      return require('../../Assets/role-experience/SocialSciences.png');
    }else if (el.replace(" ","").toUpperCase() === 'SocialWork'.toUpperCase()) {
      return require('../../Assets/role-experience/SocialWork.png');
    }else if (el.replace(" ","").toUpperCase() === 'Surveying'.toUpperCase()) {
      return require('../../Assets/role-experience/Surveying.png');
    }else if (el.replace(" ","").toUpperCase() === 'Teaching'.toUpperCase()) {
      return require('../../Assets/role-experience/Teaching.png');
    }else if (el.replace(" ","").toUpperCase() === 'Writing&Editing'.toUpperCase()) {
      return require('../../Assets/role-experience/Writing&Editing.png');
    }else{
      return require('../../Assets/role-experience/Writing&Editing.png');
    }
  }
  renderRoleExperienceImage(el) {
    var image = this.returnImageNameforRoleExperience(el);
    return (
      <Image
        style={{
          height: height * 0.05,
          width: height * 0.05,
          alignSelf: 'center',
          resizeMode: 'contain',
        }}
        source={image}
      />
    );
  }


  renderTabExpertise(el, num) {
    debugger;
    return el.expertise.length > num ? (
      <View
        key={num}
        style={{
          flex: 3.3,
          borderColor: 'black',
          // borderWidth: 1,
          backgroundColor: 'transparent',
          margin: 2,
        }}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <View style={{padding: 5, backgroundColor: 'transparent'}}>
            {this.renderExpertieImage(el.expertise[num])}
          </View>
          <Text
            style={{
              alignSelf: 'center',
              fontSize: 11,
              textAlign:'center',
              color: '#39393a',
              marginTop: 5,
            }}>
            {el.expertise[num]}
          </Text>
        </View>
      </View>
    ) : (
      <View />
    );
  }

  renderRoleExperience(el, num) {
    return el.extraQualifications.length > num ? (
      <View
        key={num}
        style={{
          flex: 3.3,
          borderColor: 'black',
          // borderWidth: 1,
          backgroundColor: 'transparent',
          margin: 2,
        }}>
        <View style={{flex: 1}}>
        <View style={{padding: 5, backgroundColor: 'transparent'}}>
            {this.renderRoleExperienceImage(el.expertise[num])}
          </View>
          <Text
            style={{
              alignSelf: 'center',
              fontSize: 11,
              textAlign:'center',
              color: '#39393a',
              marginTop: 5,
            }}>
            {el.extraQualifications[num]}
          </Text>
        </View>
      </View>
    ) : (
      <View />
    );
  }

  renderTabData(el) {
    let Image_Http_URL = {uri: el.picture};
    var numRowsQaul = el.extraQualifications
      ? Math.ceil(el.extraQualifications.length / 3)
      : 0;
    var numRows = el.expertise ? Math.ceil(el.expertise.length / 3) : 0;
    var rowArray = [];
    var rowQuaArray = [];

    for (var j = 0; j < numRowsQaul; j++) {
      rowQuaArray.push(j);
    }

    for (var j = 0; j < numRows; j++) {
      rowArray.push(j);
    }
    return (
      <View style={{flex: 1, backgroundColor: 'transparent'}}>
        <ScrollView scrollEnabled>
          <View
            style={{
              flex: 1,
              backgroundColor: 'transparent',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View
              style={{
                margin: 10,
                marginTop: 15,
                backgroundColor: 'transparent',
              }}>
              <Thumbnail
                large
                source={Image_Http_URL}
                style={{marginBottom: 5}}
              />
            </View>

            <View style={{backgroundColor: 'transparent'}}>
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: 'Ariel',
                  fontWeight: '900',
                }}>{`${el.firstName} ${el.lastName}`}</Text>
            </View>

            <View style={{alignItems: 'center', marginTop: 5}}>
              <Text
                style={{
                  width: width * 0.7,
                  textAlign: 'center',
                  fontFamily: 'Ariel',
                  fontSize: 13,
                  //fontWeight:'bold'
                }}
                >
                {el.attributes}
              </Text>
            </View>

            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                marginTop: 20,
                width: width * 0.9,
                height: height * 0.1,
                backgroundColor: 'transparent', //'#f9f7f7',
                flexDirection: 'row',
              }}>
              <View
                style={{
                  flex: 3,
                  justifyContent: 'center',
                  backgroundColor: 'transparent',
                }}>
                <Icon
                  type="MaterialCommunityIcons"
                  name="fire"
                  style={{
                    color: 'orange',
                    alignSelf: 'center',
                    fontSize: 40,
                    backgroundColor: 'transparent',
                  }}
                />
                <Text
                  style={{
                    alignSelf: 'center',
                    fontSize: 12,
                    color: '#39393a',
                  }}>
                  {el.approach}
                </Text>
              </View>

              <View style={{flex: 3}} />

              <View style={{flex: 3, justifyContent: 'center'}}>
                <Icon
                  type="MaterialCommunityIcons"
                  name="fire"
                  style={{
                    color: 'orange',
                    alignSelf: 'center',
                    fontSize: 40,
                    backgroundColor: 'transparent',
                  }}
                />
                <Text
                  style={{
                    alignSelf: 'center',
                    fontSize: 12,
                    color: '#39393a',
                  }}>
                  {`${el.energyLevel} out of 5 Energy Level`}
                </Text>
              </View>
            </View>

            {this.renderHeaderAndTextSection(
              'LANGUAGES',
              el.languages
                ? el.languages.length < 1
                  ? ''
                  : el.languages.toString()
                : '',
            )}

            <View
              style={{
                justifyContent: 'center',
                marginTop: 10,
                width: width * 0.95,
                backgroundColor: 'transparent',
                alignItems: 'center',
              }}>
              <View>
                <View
                  style={{
                    width: width * 0.95,
                    alignItems: 'baseline',
                    justifyContent: 'flex-start',
                  }}>
                  <Text
                    style={{
                      margin: 5,
                      alignSelf: 'baseline',
                      backgroundColor: 'transparent',
                      fontSize: 16,
                      fontWeight: '900',
                      fontFamily: 'Ariel',
                      color: '#39393a',
                    }}>
                    EXPERTIES
                  </Text>
                </View>
                <View
                  style={{
                    backgroundColor: 'transparent',
                    width: width * 0.95,
                    // flexDirection: 'row',

                    marginTop: 5,
                  }}>
                  {el.expertise ? (
                    rowArray.map((num,index) => {
                      debugger;
                      return (
                        <View
                          key={num}
                          style={{flex: 1, flexDirection: 'row', margin: 5}}>
                          {this.renderTabExpertise(el, 3 * index + 0)}
                          {this.renderTabExpertise(el, 3 * index + 1)}
                          {this.renderTabExpertise(el, 3 * index + 2)}
                        </View>
                      );
                    })
                  ) : (
                    <View />
                  )}
                </View>
              </View>
            </View>

            <View
              style={{
                width: width * 0.95,
                backgroundColor: 'transparent',
                marginTop: 15,
              }}>
              <Text
                style={{
                  margin: 5,
                  marginLeft: 10,
                  marginBottom: 5,
                  alignSelf: 'baseline',
                  backgroundColor: 'transparent',
                  fontSize: 16,
                  color: '#39393a',
                  fontWeight: '900',
                  fontFamily: 'Ariel',
                }}>
                ABOUT
              </Text>
              <Text
                style={{
                  fontSize: 13,
                  padding: 10,
                  fontFamily: 'Ariel',
                  color: 'gray',
                }}>
                Close to 30 years in financial services, including senior
                leadership roles,
              </Text>
            </View>
            {this.renderHeaderAndTextSection(
              'EXTRA QUALIFICATION',
              el.extraQualifications
                ? el.extraQualifications.length < 1
                  ? ''
                  : el.extraQualifications.toString()
                : '',
            )}

            <View
              style={{
                justifyContent: 'center',
                marginTop: 10,
                width: width * 0.95,
                backgroundColor: 'transparent',
              }}>
              <View>
                <View
                  style={{
                    width: width * 0.95,
                    alignItems: 'baseline',
                    justifyContent: 'flex-start',
                  }}>
                  <Text
                    style={{
                      margin: 5,
                      alignSelf: 'baseline',
                      backgroundColor: 'transparent',
                      fontSize: 16,
                      fontWeight: '900',
                      fontFamily: 'Ariel',
                      color: '#39393a',
                    }}>
                    ROLE EXPERIENCE
                  </Text>
                </View>
                <View
                  style={{
                    backgroundColor: 'transparent',
                    width: width * 0.95,
                    //flexDirection: 'row',
                    padding: 5,
                    marginTop: 5,
                  }}>
                  {el.extraQualifications ? (
                    rowQuaArray.map((num2,index2) => {
                      return (
                        <View
                          key={num2}
                          style={{flex: 1, flexDirection: 'row', margin: 5}}>
                          {this.renderRoleExperience(el, 3 * index2 + 0)}
                          {this.renderRoleExperience(el, 3 * index2 + 1)}
                          {this.renderRoleExperience(el, 3 * index2 + 2)}
                        </View>
                      );
                    })
                  ) : (
                    <View />
                  )}
                </View>
              </View>
            </View>

            {this.renderHeaderAndTextSection(
              'WORKED IN',
              el.workedIn
                ? el.workedIn.length < 1
                  ? ''
                  : el.workedIn.toString()
                : '',
            )}
            {this.renderHeaderAndTextSection(
              'PERSONALITY',
              el.personality
                ? el.personality.length < 1
                  ? ''
                  : el.personality.toString()
                : '',
            )}
          </View>
        </ScrollView>

        <View
          style={{
            height: height * 0.11,
            width: width * 0.95,
            backgroundColor: 'transparent',
            justifyContent: 'center',
            alignItems: 'center',
            borderTopColor: 'gray',
            borderTopWidth: 1,
            alignSelf: 'center',
          }}>
          {this.renderButton('SCHEDULE SESSION', el)}
        </View>
      </View>
    );
  }

  renderHeaderAndTextSection(header, desc) {
    return (
      <View
        style={{
          justifyContent: 'center',
          //marginTop: 10,
          width: width * 0.95,
          backgroundColor: 'transparent',
          marginTop: 10,
          marginBottom: 10,
        }}>
        <View>
          <View
            style={{
              width: width * 0.95,
              alignItems: 'baseline',
              justifyContent: 'flex-start',
            }}>
            <Text
              style={{
                margin: 5,
                alignSelf: 'baseline',
                backgroundColor: 'transparent',
                fontSize: 16,
                fontWeight: '900',
                fontFamily: 'Ariel',
                color: '#39393a',
              }}>
              {header}
            </Text>
          </View>
          <View
            style={{
              backgroundColor: 'transparent',
              width: width * 0.95,
              flexDirection: 'row',
            }}>
            <Text
              style={{color: 'gray', fontSize: 12, margin: 5, marginTop: 0}}>
              {desc}
            </Text>
          </View>
        </View>
      </View>
    );
  }

  renderTab(el) {
    let Image_Http_URL = {uri: el.picture};
    return (
      <Tab
        tabStyle={{borderWidth: 0, borderColor: '#4db79b'}}
        heading={
          <View
            style={{
              flex: 1,
              borderWidth: 0,
              borderColor: '#4db79b',
              backgroundColor: 'transparent',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Thumbnail source={Image_Http_URL} style={{marginBottom: 0}} />
          </View>
        }>
        {this.renderTabData(el)}
      </Tab>
    );
  }

  renderCoachHeaderImage(el) {
    return el ? (
      <TouchableOpacity
        onPress={() => this.setState({selectedCoach: el})}
        style={{
          flex: 3.3,
          padding: 5,
          borderBottomWidth:el.id == this.state.selectedCoach.id ?3:0,
          borderBottomColor:'orange',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Thumbnail
          source={{uri: el.picture}}
          style={{
            marginBottom: 0,
            height: height * 0.1,
            width: height * 0.1,
            borderRadius: height * 0.1 * 0.5,
          }}
        />
      </TouchableOpacity>
    ) : (
      <View style={{flex: 3.3}} />
    );
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: 'transparent'}}>
        <StatusBar
          translucent
          backgroundColor={'transparent'}
          barStyle="light-content"
        />

        <View style={{marginTop: 0, borderWidth: 0}}>
          <Image
            source={require('../../Assets/Rectangle.png')}
            style={{
              height: height * 0.24,
              width: width * 1,
              resizeMode: 'cover',
            }}
          />
          <View
            style={{
              position: 'absolute',
              width: width * 1,
              borderWidth: 0,
              justifyContent: 'center',
              alignItems: 'center',
              borderWidth: 0,
              borderColor: 'red',
            }}>
            <CustomHeaderWithText leftIcon={true} text="PICK YOUR COACH" />
            {this.state.isloading || this.state.coachArr.length == 0 ? (
              <Spinner color="red" style={{marginBottom: 5}} />
            ) : (
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                {this.renderCoachHeaderImage(this.state.coachArr[0])}
                {this.renderCoachHeaderImage(this.state.coachArr[1])}
                {this.renderCoachHeaderImage(this.state.coachArr[2])}
              </View>
            )}
          </View>
        </View>

        {this.state.isloading || this.state.coachArr.length == 0 ? (
          <Spinner />
        ) : (
          <View style={{flex: 1}}>
            {this.renderTabData(this.state.selectedCoach)}
          </View>
        )}
      </View>
    );
  }
}

export default PickCoach;

function chnagepage() {
  Actions.login();
}
