import React, {Component} from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  Modal,
  PanResponder,
  Alert,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import axios from 'axios';
import {
  Container,
  Header,
  Tab,
  Tabs,
  TabHeading,
  Icon,
  Thumbnail,
  Spinner,
} from 'native-base';
import {CustomHeaderWithText} from '../CommonViews';
import {UpdateUserFormData} from '../../actions';
import Animated from 'react-native-reanimated';
import firebase from 'firebase';
import firebase2, { RNFirebase } from 'react-native-firebase';
import moment from 'moment';
import {connect} from 'react-redux';
const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
//const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

const fcmChannelID = 'fcm_default_channel';
 
// This is the Firebase Server Key (for test purposes only - DO NOT SHARE IT!)
// cfr: https://www.ryadel.com/en/react-native-push-notifications-setup-firebase-4/
const firebase_server_key = 'AAAAvb2O-z4:APA91bGpw6vAShfjrmdDMy2RY8DSsGAThxhivpQh5NgpxRklT9xl82aA4qSK1j-lCj3NXgGywdg2ZpdeKxFgVVAaaOwPLuhjAluyk6DLKLJK1BuLHZewsIQndTnb8B8DfCeU-W-EcKvS';

class ScheduleSession extends Component {
  constructor(props) {
    super(props);
    this.arr = [];
    this.items = [
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      10,
      11,
      12,
      13,
      14,
      15,
      16,
      17,
      18,
      19,
      20,
      21,
      22,
      23,
      24,
      25,
      26,
      27,
      28,
      29,
      30,
      31,
    ];
    this.state = {dynamicIndex: 0, visible: false, modalVisible: false,selectedDate:7,isBookingSession:false,selectedDateString:'',
    upcomingSessionsArr:[],isfethingSessions:false,selectedCoach:this.props.selectedCoach,selectedTimeInterval:'',selectedDateIndex:1,selectedDateModel:null
    };
    debugger;
  }

   uuidv4=() =>
   {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }
  
  sendToServer = async(title,str,deviceToken) => {
    console.log("sendToServer");
    console.log(str);
    console.log('coach devicetocken',deviceToken);
    // SEND NOTIFICATION THROUGH FIREBASE
    // Workflow: React -> Firebase -> Target Devices
 if(deviceToken){
    fetch('https://fcm.googleapis.com/fcm/send', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'key=' + firebase_server_key,
      },
      body: JSON.stringify({
        "registration_ids":[
          deviceToken
        ],
        "notification": {
            "title":title,
            "body":str
        },
        "data": {
          "key1" : "value1",
          "key2" : "value2",
          "key3" : 23.56565,
          "key4" : true
        }
      }),
    })
    .then((response) => {
      console.log("Request sent!");
      console.log(response);
     // console.log("FCM Token: " + this.state.firebase_messaging_token  );
      console.log("Message: " + str);
      //this.setState({ firebase_send: '' });
    })
    .catch((error) => {
      console.log("Request Not sent!");
      console.error(error);
    });
 
 }else
 {
   console.log('coach device id is null')
 }
 
  }

addchatModelitoDb(me,isCoachAssigned,user,selectedCoach)
{
  //var me  =this;
  var clientId = user.id;
  const {firstName,lastName} = user;
  let fullname = `${firstName} ${lastName}`;
  var coachId = selectedCoach.id;
   
  var db = firebase.firestore();
  firebase.firestore().settings({experimentalForceLongPolling: true})
  db.collection("chats").add({
    users: [clientId,coachId],
    usersInfo: [user,selectedCoach],
    lastMessage:''
})
.then(function(docRef) {
  debugger;
    console.log("Document written with ID: ", docRef);
    if(isCoachAssigned)
    {
      me.setState({isBookingSession:false,modalVisible:false});
      me.sendToServer('New Session Booked',`${fullname} booked a session with you.`,selectedCoach.deviceToken);
      Actions.reset('employeeDashboard');
    }else
    {
      me.sendToServer('Congratulations',`${fullname} signed up with you as client and booked chemistry session.`,selectedCoach.deviceToken);
      me.updateUserDataOnFirebase(me,selectedCoach.id);
    }
})
.catch(function(error) {
    console.error("Error adding document: ", error);
    me.setState({isBookingSession:false,modalVisible:false});
});
}

  addSeesionToDb()
  {
    debugger;
    const {selectedCoach,user,isCoachAssigned} = this.props;
    const {firstName,lastName} = user;
    let fullname = `${firstName} ${lastName}`;
    const {selectedTimeInterval,selectedDateModel}=  this.state;
    var channelName = this.uuidv4()
    this.setState({isBookingSession:true});
    var me  =this;
    var db = firebase.firestore();
    firebase.firestore().settings({experimentalForceLongPolling: true})
    db.collection("sessions").add({
      channelName: channelName,
      coachId: selectedCoach.id,
      coachName: selectedCoach.firstName,
      date: selectedDateModel.date.toDate(), 
      employeeId: user.id,
      employeeName: user.firstName,
      timeWindow: selectedTimeInterval
  })
  .then(function(docRef) {
    debugger;
      console.log("Document written with ID: ", docRef);
      // if(isCoachAssigned)
      // {
      //   me.setState({isBookingSession:false,modalVisible:false});
      //   me.sendToServer('New Session Booked',`${fullname} booked a session with you.`,selectedCoach.deviceToken);
      //   Actions.reset('employeeDashboard');
      // }else
      // {
      //   me.sendToServer('Congratulations',`${fullname} signed up with you as client and booked chemistry session.`,selectedCoach.deviceToken);
      //   me.updateUserDataOnFirebase(me,selectedCoach.id);
      // }
      me.addchatModelitoDb(me,isCoachAssigned,user,selectedCoach);
  })
  .catch(function(error) {
      console.error("Error adding document: ", error);
      me.setState({isBookingSession:false,modalVisible:false});
  });
  }

  updateFormData(fieldname, value) {
    this.props.UpdateUserFormData({prop: fieldname, value: value});
  }


  updateUserDataOnFirebase(me,coachId) {

    const {user} = this.props;
    const {id} = user;
    const topicChosen = user.profileSteps ? (user.profileSteps.topicChosen?true:false):false;
    const assessment = user.profileSteps ? (user.profileSteps.assessment?true:false):false;
    console.log('id is', id);
    var db = firebase.firestore();
    firebase.firestore().settings({experimentalForceLongPolling: true});
    var washingtonRef = db
      .collection('users')
      .doc(id);

    // Set the "capital" field of the city 'DC'
    return washingtonRef
      .update({
        assignedCoachId:coachId,
        profileSteps:
        {
          coachAssigned:true,
          topicChosen:topicChosen,
          assessment:assessment
        }
      })
      .then(function () {
        debugger;
        me.setState({isBookingSession:false,modalVisible:false});
       // Alert.alert('Success','Session Booked Successfully');
        var updateduser = user;
        updateduser.profileSteps  = {
          coachAssigned:true,
          topicChosen:topicChosen,
          assessment:assessment
        };
        updateduser.assignedCoachId=coachId;
        me.updateFormData('user',updateduser);
        Actions.reset('employeeDashboard');
      })
      .catch(function (error) {
        // The document probably doesn't exist.
        debugger;
        me.setState({isBookingSession: false,modalVisible:false});
        console.error('Error updating document: ', error);
      });
  }



  pressBookSession()
  {
    this.addSeesionToDb();
  }

  renderModal() {
    const {selectedCoach} = this.props;
    debugger;
    const {selectedTimeInterval,isBookingSession,selectedDateString,selectedDateModel} = this.state;
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          debugger;
          this.setState({modalVisible: false});
        }}>
        <View style={styles.datecontainer}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              overflow: 'hidden',
            }}>
            <View
              style={{
                backgroundColor: 'transparent',
                width: width * 1,
                overflow: 'hidden',
              }}>
              <TouchableOpacity
                style={{
                  width: 30,
                  height: 30,
                  borderRadius: 15,
                  justifyContent: 'flex-end',
                  alignItems: 'center',
                  alignSelf: 'flex-end',
                  margin: 10,
                  borderColor: '#fff',
                  borderWidth: 1,
                }}
                onPress={() => {
                  this.setState({modalVisible: false});

                }}>
                <Icon name="close" style={{color: '#000'}} />
              </TouchableOpacity>
             
            </View>

          </View>

<View style={{width:width*0.9, borderBottomColor: '#c4c0c0',
          borderBottomWidth: 1,
    alignSelf:'center',justifyContent:'center'}}>
<Text style={{fontSize:18,fontWeight:'900',width:width*0.6,marginBottom:10}}>Chemistry Coaching Session</Text>
              <Text style={{fontSize:15}}>{selectedDateModel ?selectedDateModel.date.format('dddd'):""}, {selectedDateString}</Text>
              <Text style={{fontSize:15}}>{selectedTimeInterval} w/{selectedCoach.firstName}</Text>
<Text style={{fontSize:15,marginBottom:10}}>30 min</Text>
</View>

<View style={{height:height*0.11,width:width*0.9, borderBottomColor: 'black',
          borderBottomWidth: 1,marginBottom:15,
    alignSelf:'center', justifyContent:'center',alignItems:'center'}}>
<Text style={{fontSize:15,textAlign:'center',color:'gray'}} numberOfLines={2}>Please note that you are required to give atleast
     48 hours notice,if you have to cancel you session</Text>

</View>

        {isBookingSession ? <Spinner/> :
         <TouchableOpacity
            onPress={() =>this.pressBookSession()}
            style={{
              width: width * 0.95,
              height: height * 0.07,
              borderRadius: 5,
              backgroundColor: '#4ab865',
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
              marginBottom:10
            }}>
            <Text style={{color: '#fff', fontSize: 16}}>{'BOOK'}</Text>
          </TouchableOpacity>
          } 
        </View>
      </Modal>
    );
  }

  sendApicall(item) {
    //     var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    // var d = new Date("09/08/2020");
    // var dayName = days[d.getDay()];
    debugger;
    this.setState({isfethingSessions:true});
        var me = this;
        var tocken = "FW3eR0nNQIjnIkum84ayD64M6Aum9c";
        const {selectedCoach} = this.props;
        debugger;
      var  accountid = selectedCoach.workCalendar ? selectedCoach.workCalendar.accountId:'';
      if(accountid == null || accountid == '')
      {
        Alert.alert('Coach does not have sync the calender');
        this.setState({isfethingSessions:false});
        return;
      }
         //accountid = 380161292;
        //code = 'xK96A40wHm5x5KxSkKOYcXip22GJQg';
        var url = `https://api.kloudless.com/v1/accounts/${accountid}/cal/availability`;
        debugger;
        console.log('sending api call:');
        //this.setState({fetchingSession: true});
        let apiconfig = {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${tocken}`
          },
        };
var datestring = item.date.toString();
var datestring2 = item.date.toString();
var m = moment(datestring, 'ddd MMM D YYYY HH:mm:ss ZZ');
// Use moment(Date) if your input is a JS Date
//var m = moment(date);
m.set({'h': 8, 'm': 0});


var m2 = moment(datestring2, 'ddd MMM D YYYY HH:mm:ss ZZ');
// Use moment(Date) if your input is a JS Date
//var m = moment(date);
m2.set({'h': 17, 'm': 0});

var datestring2 = item.date.format('ddd MMM D YYYY HH:mm:ss ZZ');
        var stat = m.format()
        var stat2 =m2.format()
        const bodyParameters = {
          meeting_duration: "PT30M",
          time_slot_increment: 30,
          time_windows:[{
            "start": stat,//"2020-09-09T08:00:00",
            "end": stat2
        }]
       };
    
        axios
          .post(url, bodyParameters, apiconfig)
          .then((response) => {
            debugger;
            //const baseModel = response.data;
            //me.setState({fetchingSession: false});
            if (response.status == 200) 
            {
var baseModel = response.data.time_windows;
me.setState({isfethingSessions:false,upcomingSessionsArr:baseModel})
            }else
            {
              Alert.alert('Error', `somthing went wrong`);
              me.setState({isfethingSessions:false})
            }
            //   //var workCalendar = {accessToken:'rXFhs35L8trlviIoTWV3BT945090A0',accountId:'408081468'};
            //   var workCalendar = {
            //     accessToken: baseModel.access_token,
            //     accountId: String(baseModel.account_id),
            //   };
            //   me.updateUserDataOnFirebase(workCalendar,me);
            // }
            console.log('response success:', response);
          })
          .catch((error) => {
            debugger;
           me.setState({isfethingSessions: false});
            var err = String(error) == '' ? 'no error ' : String(error);
            console.log('error', error);
          });
      }


  componentDidMount() {
    // Initialize Firebase
    // setTimeout(chnagepage, 2000);
    //chnagepage();
    debugger;
      var nextdate = moment().add(1,'days'); 
      model = {day:nextdate.date(),dayName:nextdate.format('ddd'),date:nextdate,index:1};
    this.setState({selectedDateString:moment().format('DD MMMM yyyy'),selectedDateModel:model});
  
    var model = {day:nextdate.date(),dayName:nextdate.format('ddd'),date:nextdate} 
   this.sendApicall(model);
    debugger;
    //setTimeout(() => this.refs._scrollView.scrollTo({ x: 100, y: 0 }) , 0);
    var selectedDate = this.state.selectedDate;
    var scrollToview = parseInt(this.state.selectedDate - 1);
    // setTimeout(
    //   () =>
    //     this.scrollview_ref.scrollTo({
    //       x: width * 0.1 * scrollToview,
    //       y: 0,
    //       animated: true,
    //     }),
    //   1000,
    // );
    
  }

  renderDateDay() {
    var dayNmames = ['M', 'T', 'W', 'T', 'F', 'S', 'S'];
    var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    // var d = new Date("09/08/2020");
    // var dayName = days[d.getDay()];
    const {selectedDate,selectedDateIndex} = this.state;
    var dates = [
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      10,
      11,
      12,
      13,
      14,
      15,
      16,
      17,
      18,
      19,
      20,
      21,
      22,
      23,
      24,
      25,
      26,
      27,
      28,
      29,
      30,
      31,
    ];
var datesModelArr = []
var currentdata = moment(); 
var model = {day:currentdata.date(),dayName:currentdata.format('ddd'),date:currentdata,isSelected:true,index:0} 
///
datesModelArr.push(model)
for(var i=1;i <=60;i++)
    {
      var nextdate = moment().add(i,'days'); 
       model = {day:nextdate.date(),dayName:nextdate.format('ddd'),date:nextdate,index:i};
       datesModelArr.push(model) 
    }
    return datesModelArr.map((item, key) => 
      <TouchableOpacity
        onPress={() => this.downButtonHandler(item)}
        onLayout={(event) => {
          const layout = event.nativeEvent.layout;
          this.arr[key] = layout.x;
          console.log('height:', layout.height);
          console.log('width:', layout.width);
          console.log('x:', layout.x);
          console.log('y:', layout.y);
        }}
        key={item.index}
        style={{
          width: width * 0.1,
          height: height * 0.1,
          borderWidth: 0,
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
          margin: 5,
          marginLeft: 0,
        }}>

      <Text style={{fontFamily: 'Ariel', fontSize: 14,fontWeight:'900'}}>{item.dayName}</Text>
        <View style={{ width: width * 0.08,height:width * 0.08,borderRadius:width * 0.08*0.5,
          backgroundColor: selectedDateIndex == item.index ? '#4db79b':'transparent',alignItems:'center'}}>
          <Text style={{fontFamily: 'Ariel', fontSize: 14, marginTop: 5,
        color:selectedDateIndex == item.index ?'#fff':'#000'}}>
            {item.day}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  downButtonHandler = (item) => {
    debugger;
    var indexTpScroll = item.index > 2 ? item.index - 2 : 0;
    //this.setState({selectedDateString:currentdata.format('dd MMMM yyyy')})
    this.setState({selectedDate:item.index,selectedDateModel:item,
      selectedDateIndex:item.index,selectedDateString:item.date.format('DD MMMM yyyy')})
      this.sendApicall(item);
    if (this.arr.length >= item.index) {
      // To Scroll to the index 5 element
      this.scrollview_ref.scrollTo({
        x: this.arr[indexTpScroll],
        y: 0,
        animated: true,
      });
    } else {
      alert('Out of Max Index');
    }
  };

  renderBookingTimeGridRow(el) {
    var startime =moment(el.start).format('HH:mm A') //new Date(el.start).toLocaleTimeString()

    var endtime = moment(el.end).format('HH:mm A')//new Date(el.end).toLocaleTimeString()
    return (
      <View
      key={el.start}
        style={{
          width: width * 0.95,
          borderBottomColor: '#c4c0c0',
          borderBottomWidth: 1,
          height: height * 0.13,
          alignItems: 'center',
          justifyContent: 'space-between',
          flexDirection: 'row',
        }}>
        <Text style={{marginLeft: 20, fontSize: 14, fontWeight: '900'}}>
          {`${startime} - ${endtime}`}
        </Text>
        <TouchableOpacity
          onPress={() => this.setState({modalVisible: true,selectedTimeInterval: `${startime} - ${endtime}`})}
          style={{
            width: width * 0.23,
            height: height * 0.05,
            borderRadius: 5,
            borderWidth: 1,
            borderColor: '#c4c0c0',
            marginRight: 20,
            backgroundColor: 'transparent',
            alignItems: 'center',
            justifyContent: 'center',
            alignSelf: 'center',
          }}>
          <Text style={{color: '#000', fontSize: 14}}>{'BOOK'}</Text>
        </TouchableOpacity>
      </View>
    );
  }

  render() {
    const {selectedDateString} = this.state;
    return (
      <View
        style={{flex: 1, backgroundColor: 'transparent', alignItems: 'center'}}>
        <StatusBar translucent backgroundColor={'transparent'} barStyle="light-content" />
        <CustomHeaderWithText text="SCHEDULE SESSION" />
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: 20,
          }}>
          <Text style={{fontFamily: 'Ariel', fontSize: 17, fontWeight: 'bold'}}>
            {selectedDateString}
          </Text>
        </View>

        <View
          style={{
            height: height * 0.13,
            borderWidth: 0,
            alignItems: 'center',
            marginTop: 15,
            backgroundColor: '#fff',
            justifyContent: 'center',
            marginBottom: 10,
          }}>
          <ScrollView
            ref={(ref) => {
              this.scrollview_ref = ref;
            }}
            style={{backgroundColor: '#fff'}}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            scrollEnabled>
            <View
              style={{
                height: height * 0.1,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: '#fff',
                borderBottomWidth:1,
                borderBottomColor:'gray'
              }}>
              {this.renderDateDay()}
            </View>
          </ScrollView>
        </View>
        <ScrollView showsVerticalScrollIndicator={false} scrollEnabled>
          <View
            style={{
              flex: 1,
              backgroundColor: '#fff',
              alignItems: 'center',
            }}>
             
            {this.state.isfethingSessions ? <Spinner style={{alignSelf:'center'}}/>
          :
            this.state.upcomingSessionsArr.length <1 ?
             <Text style={{alignSelf:'center',margin:20}}>No slot availble</Text>
             :this.state.upcomingSessionsArr.map(el=> {
             
              return this.renderBookingTimeGridRow(el)
             }) 
          }
          </View>
        </ScrollView>
        {this.renderModal()}
      </View>
    );
  }
}

//export default ScheduleSession;
const mapStateToProps = ({auth}) => {
  const {user} = auth;

  return {user};
};
export default connect(mapStateToProps, {UpdateUserFormData})(ScheduleSession);
function chnagepage() {
  Actions.login();
}
const styles = {
  container: {
    // height: Dimensions.get('window').height / 3.3,
    backgroundColor: '#e6e7e9',
    margin: 20,
    marginBottom: 5,
    borderRadius: 9,
    overflow: 'hidden',
    borderColor: '#f48004',
    borderWidth: 2,
  },
  DiscriptionBorder: {
    borderWidth: 3,
    borderColor: '#f48004',
    backgroundColor: 'white',
    width: width * 0.9,
    height: height * 0.25,
    margin: 10,
    borderRadius: 10,
    alignSelf: 'center',
  },
  datecontainer: {
    backgroundColor: '#f9f7f7', //'#E9EBEE',
    borderRadius: 5,

    marginTop: height * 0.48,
  },
  rowList: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    margin: 10,
    borderBottomColor: 'white',
    width: width * 0.95,
    borderBottomWidth: 1,
    alignSelf: 'center',
    padding: 10,
  },
  rowIcon: {
    color: '#f48004',
    margin: 20,
    marginTop: 10,
    marginBottom: 5,
  },
  rowHeadingList: {
    fontWeight: 'bold',
    marginRight: 10,
    marginLeft: 10,
    fontSize: 16,
    color: 'black',
    fontFamily: 'sens-serif',
  },
  rowSelectType: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 10,
    marginTop: 10,
  },
  rowText: {
    color: 'grey',
    alignSelf: 'center',
    marginTop: 5,
    marginLeft: 20,
  },
  card: {
    flex: 1,
    borderRadius: 4,
    borderWidth: 2,
    borderColor: '#E8E8E8',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  text: {
    textAlign: 'center',
    fontSize: 50,
    backgroundColor: 'transparent',
  },
  done: {
    textAlign: 'center',
    fontSize: 30,
    color: 'white',
    backgroundColor: 'transparent',
  },
};
