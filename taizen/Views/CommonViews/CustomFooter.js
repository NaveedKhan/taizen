import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Image,
  ScrollView
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Badge } from 'native-base';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import { UpdateFooterData} from '../../actions';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const greyColor = '#A0A1A5';
//const CustomFooter = () => {
class CustomFooter extends Component {
  constructor(props) {
    super(props);
    debugger;
this.state ={homepressed:true,coachingpressed:false,journeypressed:false,profilepressed:false}
  }
  onPressCartRequests() {
    const { isHomePage, selectedApp, cartRequestsCount } = this.props;
    if (isHomePage) {
      Actions.onBoarding()
    }
    if (selectedApp == "Grocerry") {
      Actions.onBoarding()
    }
    Actions.onBoarding();
  }

  onHomepressed(value)
  {
    //this.setState({homepressed:true, coachingpressed:false,journeypressed:false,profilepressed:false})
    this.props.UpdateFooterData({prop: 'employeeSelectedTab', value: value});
    Actions.employeeDashboard();
  }

  onCoachingpressed(value)
  {
    this.props.UpdateFooterData({prop: 'employeeSelectedTab', value: value});
    Actions.coaching();
  }

  onJourneypressed(value)
  {
    this.props.UpdateFooterData({prop: 'employeeSelectedTab', value: value});
    Actions.journey();
  }
 
  onProfilepressed(value)
  {
    this.props.UpdateFooterData({prop: 'employeeSelectedTab', value: value});
    Actions.clientProfile()
  }
  //const images = require('../Grocessry/Asset/Kaam_Tamam_Logo.png')
  render() {
    var landingTo = this.props.landingTo == "G" ? "landingPage" : "selectServices";
    const {homepressed,coachingpressed,journeypressed,profilepressed} = this.state;
    const {employeeSelectedTab} = this.props;
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          height: height * 0.1,
          margin: 5,
          marginLeft: 0,
          paddingLeft: 5,
          paddingRight: 5,
          marginRight: 0,
          height: height*0.09,
          width:width*1,
          borderTopColor: 'gray',
          borderTopWidth: 0,
          backgroundColor: 'white'
        }}>


        <TouchableOpacity 
        style={{ alignItems: 'center',justifyContent:'center',
        //backgroundColor:'red',
        borderTopWidth:  employeeSelectedTab == 'HOME' ?  2: 0,
        borderTopColor:'#4ab865',
        height:height*0.08 ,marginLeft:10}}
        onPress={()=> 
          {
            console.log('footer clicked')
            this.onHomepressed('HOME')}}
        >
          <Image tintColor={employeeSelectedTab == 'HOME' ?  '#4ab865': 'gray'} source={require('../../Assets/home.png')}
           style={{resizeMode:'contain',
         height:height*0.04,width:height*0.04}}/>
          {/* <Icon name="home" type="AntDesign" style={{ fontSize: height*0.036, color: greyColor }} /> */}
          <Text style={{ fontSize: 13, color: greyColor,fontWeight:'900' }}>HOME</Text>
        </TouchableOpacity>



  
        <TouchableOpacity onPress={()=> this.onCoachingpressed('COACHING')}
         style={{alignItems: 'center',
         borderTopWidth:  employeeSelectedTab == 'COACHING' ?  2: 0,
         borderTopColor:'#4ab865',
         justifyContent:'center',height:height*0.08 }}>     
        <Image tintColor={employeeSelectedTab == 'COACHING' ?  '#4ab865': 'gray'} source={require('../../Assets/coaching.png')} style={{resizeMode:'contain',
         height:height*0.04,width:height*0.04}}/>
          <Text style={{ fontSize: 13, color: greyColor,fontWeight:'900' }}>COACHING</Text>
        </TouchableOpacity>


        <TouchableOpacity onPress={()=> this.onJourneypressed('JOURNEY')} 
        style={{alignItems: 'center',justifyContent:'center',
        borderTopWidth:  employeeSelectedTab == 'JOURNEY' ?  2: 0,
        borderTopColor:'#4ab865',
        height:height*0.08 }}>     
        <Image tintColor={employeeSelectedTab == 'JOURNEY' ?  '#4ab865': 'gray'} source={require('../../Assets/journey.png')} style={{resizeMode:'contain',
         height:height*0.04,width:height*0.04}}/>
          <Text style={{ fontSize: 13, color: greyColor,fontWeight:'900' }}>JOURNEY</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={()=> this.onProfilepressed('PROFILE')} 
        style={{alignItems: 'center',justifyContent:'center',
        borderTopWidth:  employeeSelectedTab == 'PROFILE' ?  2: 0,
        borderTopColor:'#4ab865',
        height:height*0.08,marginRight:10 }}>     
        <Image tintColor={employeeSelectedTab == 'PROFILE' ?  '#4ab865': 'gray'} source={require('../../Assets/person.png')} style={{resizeMode:'contain',
         height:height*0.04,width:height*0.04}}/>
          <Text style={{ fontSize: 13, color: greyColor,fontWeight:'900' }}>PROFILE</Text>
        </TouchableOpacity>

      </View>
    );
  }
};

//export default CustomFooter;
// const mapStateToProps = ({ notificationsReducer }) => {
//   const { unReadNotificationCount, totalpendingOrdercount } = notificationsReducer;
//   return { unReadNotificationCount, totalpendingOrdercount };
// };
// export default connect(mapStateToProps, null)(CustomFooter);

const mapStateToProps = ({footer}) => {
  const {employeeSelectedTab} = footer;

  return {employeeSelectedTab};
};
export default connect(mapStateToProps, { UpdateFooterData})(CustomFooter);
