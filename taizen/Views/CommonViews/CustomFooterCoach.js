import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Image,
  ScrollView
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Badge } from 'native-base';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import { UpdateFooterData} from '../../actions';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const greyColor = '#A0A1A5';
//const CustomFooter = () => {
class CustomFooterCoach extends Component {
  constructor(props) {
    super(props);
    debugger;

  }
 
  onHomepressed(value)
  {
    //this.setState({homepressed:true, coachingpressed:false,journeypressed:false,profilepressed:false})
    this.props.UpdateFooterData({prop: 'coachSelectedTab', value: value});
    Actions.coachDashboard();
  }

  onClientpressed(value)
  {
    this.props.UpdateFooterData({prop: 'coachSelectedTab', value: value});
    Actions.clients();
  }

  onProfilepressed(value)
  {
    this.props.UpdateFooterData({prop: 'coachSelectedTab', value: value});
    Actions.profile();
  }

  render() {
    var landingTo = this.props.landingTo == "G" ? "landingPage" : "selectServices";
    const {coachSelectedTab} = this.props;
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-around',
          alignItems: 'center',
          height: height * 0.1,
          margin: 5,
          marginLeft: 0,
          paddingLeft: 5,
          paddingRight: 5,
          marginBottom:0,
          marginRight: 0,
          //zIndex:5,
          height: height*0.09,
          width:width*1,
          borderTopColor: greyColor,
          borderTopWidth: 1,
          backgroundColor: 'white'
        }}>

        <TouchableOpacity onPress={()=>  this.onHomepressed('CoachDashboard') } 
        style={{ alignItems: 'center',justifyContent:'center',
        //backgroundColor:'red',
        borderTopWidth:   coachSelectedTab == 'CoachDashboard' ?  2: 0,
        borderTopColor:'#4ab865',
        height:height*0.09,width:height*0.08 }}
         >
          <Image tintColor={coachSelectedTab == 'CoachDashboard' ?  '#4ab865': 'gray'} source={require('../../Assets/home.png')} style={{resizeMode:'contain',
         height:height*0.04,width:height*0.04}}/>
         
        </TouchableOpacity>

  
        
        <TouchableOpacity onPress={()=>this.onClientpressed('Clients')} 
        style={{ width:height*0.08,alignItems: 'center',
        borderTopWidth:  coachSelectedTab == 'Clients' ?  2: 0,
        borderTopColor:'#4ab865',
        justifyContent:'center',height:height*0.09 }}>

        <Image tintColor={coachSelectedTab == 'Clients' ?  '#4ab865': 'gray'} source={require('../../Assets/clients.png')} style={{resizeMode:'contain',
         height:height*0.04,width:height*0.04}}/>
        </TouchableOpacity>

        <TouchableOpacity onPress={()=>this.onProfilepressed('Profile')} 
        style={{ width:height*0.08,alignItems: 'center',
        borderTopWidth:  coachSelectedTab == 'Profile' ?  2: 0,
        borderTopColor:'#4ab865',
        justifyContent:'center',height:height*0.09,marginRight:10 }}>     
        <Image tintColor={coachSelectedTab == 'Profile' ?  '#4ab865': 'gray'} source={require('../../Assets/person.png')} style={{resizeMode:'contain',
         height:height*0.04,width:height*0.04}}/>
        </TouchableOpacity>
      </View>
    );
  }
};


const mapStateToProps = ({footer}) => {
  const {coachSelectedTab} = footer;

  return {coachSelectedTab};
};
export default connect(mapStateToProps, { UpdateFooterData})(CustomFooterCoach);