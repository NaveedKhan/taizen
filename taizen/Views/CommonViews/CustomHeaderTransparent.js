import React, {Component} from 'react';
import {View, Text, Dimensions,Image, TouchableHighlight} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {
  Container,
  Header,
  Content,
  Tab,
  Tabs,
  Icon,
  Input,
  ScrollableTab,
  Button,
} from 'native-base';
import {IconInput} from '.';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class CustomHeaderTransparent extends Component {
  clickc() {
    debugger;
    Actions.pop();
  }

  backpressed() {
    debugger;

    Actions.pop();
  }

  render() {
    const {
      leftIcon,
      searchBar,
      rightIcon,
      refreshButton,
      refreshButtonClicked,
      headerStyle,
      headerTextStyle,
      iconStyle
    } = this.props;
    return (

<View style={[styles.headerMainDiv,headerStyle]}>
        {leftIcon ? 
          <TouchableHighlight
          style={styles.leftIconDiv}
          onPress={() => this.backpressed()}>
          <Icon
            name="arrow-back"
            style={[{color: iconStyle ? '#000':'#fff', fontSize: 35, marginLeft: 10},iconStyle]}
          />
        </TouchableHighlight>
        :<View  style={styles.leftIconDiv}/>}
        <View style={styles.centerDiv}>
          <Text
            style={[{
              fontSize: 18,
              color: '#fff',
              alignSelf: 'center',
              fontWeight: 'bold',
              fontFamily:'Ariel'},headerTextStyle]
              }>
            {this.props.text}
          </Text>
        </View>
        {refreshButton ? (
          <TouchableHighlight
          style={styles.righIconDiv}
            onPress={refreshButtonClicked}>
            <View > 
              <Icon
                name="refresh"
                type={'FontAwesome'}
                style={{color: '#f48004', fontSize: 35,alignSelf:'center'}}
              />
              <Text>Refresh</Text>
            </View>
          </TouchableHighlight>
        ) : (
          <View />
        )}
        {this.props.showMenu ? (
          <TouchableHighlight
            style={styles.righIconDiv}
            onPress={() => this.clickc()}>
            <Icon name="menu" style={{color: '#f48004', fontSize: 27}} />
          </TouchableHighlight>
        ) : (
          <View style={{flex: 1}} />
        )}
      </View>
      
    );
  }
}

export {CustomHeaderTransparent};

const styles = {
  headerMainDiv: {
    height: Dimensions.get('window').height / 8,
   // backgroundColor: '#4db79b', //'#f48004',
    flexDirection: 'row',
    borderWidth:0,
   // position:'absolute'
    //zIndex:5
  },
  leftIconDiv: {
    backgroundColor: 'transparent',
    width: Dimensions.get('window').width /5,
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  centerDiv: {
    backgroundColor: 'transparent',
    width: Dimensions.get('window').width /1.7,
    justifyContent: 'center',
    alignItems: 'center',
  },
  centerTextFieldDiv: {
    flexDirection: 'row',
    alignItems: 'center',
   // borderColor: 'gray',
    backgroundColor: '#fff',
    //borderWidth: 1,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 26,
  },
  righIconDiv: {
    width: Dimensions.get('window').width /5,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
};
