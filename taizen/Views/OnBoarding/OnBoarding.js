import React, {Component} from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import {connect} from 'react-redux';
import styles from '../../Assets/Css/style';
import {Actions} from 'react-native-router-flux';
import {Button, CheckBox} from 'native-base';
import {ScrollView} from 'react-native-gesture-handler';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
class OnBoarding extends Component {
  constructor(props) {
    super(props);
    // ...
  }
  componentDidMount() {
    // Initialize Firebase
    // setTimeout(chnagepage, 2000);
    //chnagepage();
  }

  renderButton(onpressButton) {
    return (
      <TouchableOpacity
        style={{
          width: width * 0.23,
          height: height * 0.045,
          borderWidth: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#FFF',
        }}
        onPress={onpressButton}>
        <Text>LEST'S GO</Text>
      </TouchableOpacity>
    );
  }

  _renderCards(
    headingpart1,
    headingpart2,
    questions,
    time,
    imagename,
    onpressButton,
    isCompleted
  ) {
    debugger;
    var image = require('../../Assets/onBoardimage1.png');
    image =
      imagename == 'onBoardimage1.png'
        ? require('../../Assets/onBoardimage1.png')
        : imagename == 'onBoardimage2.png'
        ? require('../../Assets/onBoardimage2.png')
        : require('../../Assets/onBoardimage3.png');

    return (
      <View
        style={{
          height: height * 0.2,
          backgroundColor: '#4ab865',
          borderRadius: 10,
          marginBottom: 10,
          justifyContent: 'flex-end',
          overflow: 'hidden',
          opacity:isCompleted ? 0.6: 1
        }}>
       
        <Image
          source={image}
          style={{
            height: height * 0.2,
            width: width * 0.95,
            resizeMode: 'cover',
          }}
        />
        <View
          style={{
            position: 'absolute',
            height: height * 0.2,
            backgroundColor: 'transparent',
            width: width * 0.95,
            flexDirection: 'row',
          }}>
          <View style={{flex: 1.5, flexDirection: 'column'}}>
            <View
              style={{
                flex: 5,
                justifyContent: 'flex-end',
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: width * 0.08,
                  height: width * 0.08,
                  backgroundColor: 'white',
                  borderRadius: width * 0.08 * 0.5,
                }}></View>
            </View>
            <View style={{flex: 5}} />
          </View>
          <View style={{flex: 8.5, flexDirection: 'column'}}>
            <View
              style={{
                flex: 4,
                backgroundColor: 'transparent',
                justifyContent: 'flex-end',
                alignItems: 'flex-start',
              }}>
              <Text
                style={{
                  fontSize: 22,
                  fontWeight: 'bold',
                  fontFamily: 'Ariel',
                  padding: 4,
                  color: 'white',
                }}>
                {headingpart1}
              </Text>
            </View>
            <View
              style={{
                flex: 6,
                backgroundColor: 'transparent',
                flexDirection: 'row',
              }}>
              <View
                style={{
                  flex: 6.7,
                  flexDirection: 'column',
                  backgroundColor: 'transparent',
                }}>
                <Text
                  style={{
                    fontSize: 22,
                    fontWeight: 'bold',
                    fontFamily: 'Ariel',
                    padding: 4,
                    paddingTop: 0,
                    paddingBottom: 0,
                    color: 'white',
                  }}>
                  {headingpart2}
                </Text>
                <View style={{flexDirection: 'row', marginTop: 5}}>
                  {questions != '' ? (
                    <View style={{flex: 5}}>
                      <Text style={{fontSize: 15, color: 'white'}}>
                        {' '}
                        {questions}
                      </Text>
                    </View>
                  ) : (
                    <View style={{backgroundColor: 'red'}} />
                  )}
                  <View style={{flex: questions != '' ? 5 : 7, fontSize: 15}}>
                    <Text style={{fontSize: 15, color: 'white'}}>{time}</Text>
                  </View>
                </View>
              </View>
              <View style={{flex: 3.3, backgroundColor: 'transparent'}}>
                <View style={{flex: 4}}></View>
                <View style={{flex: 6, backgroundColor: 'pink'}}>
                  {
                  this.renderButton(onpressButton)
                  }
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }

  render() {
    const {user} = this.props;
    return (
      <View style={{flex: 1, alignItems: 'center'}}>
         <StatusBar backgroundColor={'transparent'} barStyle="dark-content" />
        <ScrollView scrollEnabled>
          <View
            style={{
              height: height * 0.3,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{fontSize: 23, fontFamily: 'Ariel', fontWeight: 'bold'}}>
              Welcome To Taizen,
            </Text>
            <Text
              style={{fontSize: 23, fontFamily: 'Ariel', fontWeight: 'bold'}}>
              {user.firstName} {user.lastName}!
            </Text>
            <Text style={{fontSize: 14, marginTop: 5, fontFamily: 'Ariel'}}>
              Getting started is super easy!{' '}
            </Text>
          </View>
          <View
            style={{
              height: height * 0.7,
              backgroundColor: 'transparent',
              width: width * 0.95,
            }}>
            <View>
              {this._renderCards(
                'Explore your strengths',
                '& weaknesses',
                '56 questions,',
                '5-7 minutes',
                'onBoardimage1.png',
                () => user.profileSteps ? (user.profileSteps.assessment ? console.log('topic chosen'):Actions.questionier()):Actions.questionier(),
                user.profileSteps ? (user.profileSteps.assessment ? true:false):false
                )}
              {this._renderCards(
                'Choose your coaching',
                'topics',
                '',
                '30 seconds',
                'onBoardimage2.png',
                () => user.profileSteps ? (user.profileSteps.topicChosen ? console.log('topic chosen'):Actions.coachingTopics()):Actions.coachingTopics(), 
                user.profileSteps ? (user.profileSteps.topicChosen ? true:false):false
                 )}

              {this._renderCards(
                'Pick your coach and first',
                'session',
                '',
                'Less than aminute',
                'onBoardimage3.png',
                () => user.profileSteps ? (user.profileSteps.coachAssigned ? console.log('coach assigned'):Actions.pickCoach()):Actions.pickCoach() ,
                user.profileSteps ? (user.profileSteps.coachAssigned ? true:false):false
              )}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

//export default OnBoarding;
const mapStateToProps = ({auth}) => {
  const {user} = auth;

  return {user};
};
export default connect(mapStateToProps, null)(OnBoarding);
function chnagepage() {
  Actions.login();
}
