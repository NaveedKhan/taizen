import React, { Component } from 'react';
import { Text,Dimensions, View,Image ,TouchableOpacity,StatusBar} from 'react-native';
import { Provider } from 'react-redux';
import { Root, Spinner } from "native-base";
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import DeviceInfo from 'react-native-device-info';
import AsyncStorage from '@react-native-community/async-storage';
import firebase, { RNFirebase } from 'react-native-firebase';
// import firebase from 'firebase';
import styles from '../../Assets/Css/style';
import { Actions } from 'react-native-router-flux';
import {connect} from 'react-redux';
import {loginUser,UpdateUserFormData} from '../../actions';

// This is the default FCM Channel Name (required by Android)
const fcmChannelID = 'fcm_default_channel';
 
// This is the Firebase Server Key (for test purposes only - DO NOT SHARE IT!)
// cfr: https://www.ryadel.com/en/react-native-push-notifications-setup-firebase-4/
const firebase_server_key = 'AAAAvb2O-z4:APA91bGpw6vAShfjrmdDMy2RY8DSsGAThxhivpQh5NgpxRklT9xl82aA4qSK1j-lCj3NXgGywdg2ZpdeKxFgVVAaaOwPLuhjAluyk6DLKLJK1BuLHZewsIQndTnb8B8DfCeU-W-EcKvS';


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
class SplashScreen extends Component {
  constructor(props) {
    super(props);
    // ...
    //this.state = {isloading:false};
    this.state = {
      isloading: false,
      firebase_messaging_token: '',
      firebase_messaging_message: '',
      firebase_notification: '',
      firebase_send: '',
    };
    this.messageListener = null;
    this.notificationInitialListener = null;
    this.notificationListener = null;
    this.tokenRefreshListener = null;
    // this.notificationInitialListener = null;
  
  }

  componentWillUnmount() {
    //this.removeNotificationListeners();
  }

  createNotificationChannel = () => {
    console.log('createNotificationChannel');
    // Build a android notification channel
    const channel = new firebase.notifications.Android.Channel(
      fcmChannelID, // channelId
      'FCM Default Channel', // channel name
      firebase.notifications.Android.Importance.High, // channel importance
    ).setDescription('Test Channel'); // channel description
    // Create the android notification channel
    firebase.notifications().android.createChannel(channel);
  };

  checkNotificationPermissions() {
    console.log('checkNotificationPermissions');
    // show token
    firebase
      .messaging()
      .hasPermission()
      .then(enabled => {
        if (enabled) {
          console.log('user has notification permission');
          this.setToken();
        } else {
          console.log('user does not have notification permission');
          firebase
            .messaging()
            .requestPermission()
            .then(result => {
              if (result) {
                this.setToken();
              } else {
              }
            });
        }
      });
  }

  setToken() {
    console.log('setToken');
    firebase
      .messaging()
      .getToken()
      .then(token => {
        this.setState({firebase_messaging_token: token});
        this.props.UpdateUserFormData({prop: 'DeviceTocken', value: token});
        console.log(token);
        this.isAlreadyLoggedIn(token);
      });
  }

  addNotificationListeners() {
    console.log('receiveNotifications');
    this.messageListener = firebase.messaging().onMessage(message => {
      // "Headless" Notification
      console.log('onMessage');
    });

    this.notificationInitialListener = firebase
      .notifications()
      .getInitialNotification()
      .then(notification => {
        if (notification) {
          // App was opened by a notification
          // Get the action triggered by the notification being opened
          // Get information about the notification that was opened
          debugger;
          console.log('onInitialNotification');
        }
      });

    this.notificationDisplayedListener = firebase
      .notifications()
      .onNotificationDisplayed(notification => {
        console.log('onNotificationDisplayed');
      });

    this.notificationListener = firebase
      .notifications()
      .onNotification(notification => {
        console.log('onNotification');
        notification.android.setChannelId(fcmChannelID);
        firebase
          .notifications()
          .displayNotification(notification)
          .catch(err => {
            console.log(err);
          });
        debugger;
        // Process your notification as required

        // #1: draw in View
        var updatedText =
          this.state.firebase_notification +
          '\n' +
          '[' +
          new Date().toLocaleString() +
          ']' +
          '\n' +
          notification.title +
          ':' +
          notification.body +
          '\n';
console.log('notification data is', updatedText);
        this.setState({firebase_notification: updatedText});
      });

    this.tokenRefreshListener = firebase
      .messaging()
      .onTokenRefresh(fcmToken => {
        // Process your token as required
        console.log('onTokenRefresh');
      });
  }

  removeNotificationListeners() {
    this.messageListener();
    this.notificationInitialListener();
    this.notificationDisplayedListener();
    this.notificationListener();
    this.tokenRefreshListener();
  }



async componentDidMount() {
    // Initialize Firebase
   // configureFirebase();
   this.setState({ isloading: true });
   this.createNotificationChannel();
    this.checkNotificationPermissions();
    this.addNotificationListeners();
  }
  
  isAlreadyLoggedIn(toekn) {
  //  debugger;
    var me = this;
    console.log('isAlreadyLoggedIn');
   
    try {
      const token = AsyncStorage.getItem('userData')
        .then(token => {
          debugger;
          if (token != null && token != "{}") {
            let userObj = JSON.parse(token);
            const {UserName, Password} = userObj;
            me.props.loginUser({email: UserName, password: Password,DeviceTocken:toekn});
          }else
          {
            me.setState({isloading:false});
           // Actions.login();
          }
        })
        .done();
    } catch (error) {
      var err = String(error) == '' ? 'no error ' : String(error);
      Alert.alert('error', err);
        me.setState({ isloading: false });
        console.log('error',error);
      //return null;
    }
  }
  
  renderSignupdButton(onpressButton,buttontext)
  {
  
    return <TouchableOpacity
    style={{
     // width: width * 0.23,
      //height: height * 0.045,
      borderBottomWidth: 1,
      //borderRadius: 5,
     marginBottom:5,
      borderBottomColor:'#fff',
      borderColor: 'gray',
     // marginRight: width * 0.1,
      justifyContent: 'center',
      alignItems: 'center',
      alignSelf: 'center',
    }}
    onPress={onpressButton}>
    <Text style={{fontSize:14,color:'#fff'}}>{buttontext.toUpperCase()}</Text>
  </TouchableOpacity>
  }

  componentWillUnmount(){
    this.setState({ isloading: false });
  }

  render() {
    return (
      <View style={styles.container}>
         <StatusBar translucent backgroundColor="transparent" barStyle="light-content"/>
         {/* <StatusBar backgroundColor={'#4db79b'} barStyle="light-content"/> */}
      <Image source={require('../../Assets/gradientbackground.png')} style={styles.backgroundImage} />
        
        <View style={{borderWidth:0,position:'absolute',
        alignSelf:'center',alignItems:'center',
        height:height*1,zIndex:2,width:width*1,top:0}}>
        
        <View style={{flex:1,alignSelf:'stretch'}}>
        <View style={{flex:2,backgroundColor:'transparent',alignItems:'center',justifyContent:'center'}}>
        <Image source={require('../../Assets/logo.png')} style={{height:height*0.08,width:width*0.7,resizeMode:'contain'}}/>
        </View>
        <View style={{flex:4,backgroundColor:'transparent',alignItems:'center',justifyContent:'center'}}>
        <Image source={require('../../Assets/splash.png')} style={{height:height*0.4,width:width*1,resizeMode:'contain'}}/>
        </View>
        <View style={{flex:2,alignItems:'center',justifyContent:'center',backgroundColor:'transparent'}}>
        {this.state.isloading ? 
        
        <TouchableOpacity style={[styles.button,{position:'relative',marginBottom:height*0.04}]}>
        <Spinner color={'red'}/> 
      </TouchableOpacity>
        :  <TouchableOpacity style={[styles.button,{position:'relative',marginBottom:height*0.04}]} onPress={()=> Actions.login()}>
            <Text style={styles.buttonText}>LOGIN</Text>
         </TouchableOpacity>
         }

{this.renderSignupdButton(()=> Actions.signup(),'Signup with Email')}
          
        </View>
        <View style={{ backgroundColor: '#000',
    height: 6,
    //bottom: 5,
    width: width * 0.4,
    alignSelf: 'center',
    borderRadius: 10,
    marginBottom:5
    
    }}/>
        </View>
        

        </View>
 
       
        {/* <View style={styles.blackLine}/> */}
      </View>
    );
  }
}

//export default SplashScreen;

const mapStateToProps = ({auth}) => {
  const {user,loading} = auth;

  return {user,loading};
};
export default connect(mapStateToProps, {loginUser,UpdateUserFormData})(SplashScreen);