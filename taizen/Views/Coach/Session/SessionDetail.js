import React, {Component} from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  Modal,
  PanResponder,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Left,
  Body,
  Right,
  Thumbnail,
  Icon,
  Spinner,
} from 'native-base';
import {CustomHeaderWithText} from '../../CommonViews';
import Animated from 'react-native-reanimated';
import CustomFooter from '../../CommonViews/CustomFooter';
import Messsages from '../../EmployeeDashboard/Messsages';
import CustomFooterCoach from '../../CommonViews/CustomFooterCoach';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
const image = require('../../../Assets/coach.png');
class SessionDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {isloading: false};
    // ...
  }
  componentDidMount() {
    // Initialize Firebase
    // setTimeout(chnagepage, 2000);
    //chnagepage();
  }



  renderSessiontimimgRow() {
    return (
      <View
        style={{
          //width: width * 0.95,
          borderColor: '#e0e0e0',
          borderBottomWidth: 1,
          backgroundColor: 'white',
          borderRadius: 8,
          //marginBottom: 15,
          alignSelf: 'center',
          justifyContent: 'center',
          flexDirection: 'row',
          //marginBottom: 10,
        }}>
        <View
          style={{
            flex: 6.5,
            backgroundColor: 'transparent',
            padding: 20,
            paddingTop:6
          }}>
          <Text style={{fontSize: 16, marginTop: 1,color:'gray'}}>Mon, 7th September</Text>
          <Text style={{fontSize: 16, marginTop: 1,color:'gray'}}>
            8:00 am - 8:15 am 
          </Text>
          <Text style={{fontSize: 16, marginBottom: 0, marginTop: 1,color:'gray'}}>
            15 min
          </Text>
        </View>
        <View
          style={{
            flex: 3.5,
            backgroundColor: 'transparent',
            justifyContent: 'center',
            alignItems: 'center',
          }}/>
      </View>
    );
  }
  ReturnROw(el) {
    return (
      <View key={el} avatar  style={{flex:1,marginLeft:0,backgroundColor:'transparent',borderWidth:0,flexDirection:'row',width:width*1,marginTop:0}}>
        <View style={{flex:2,backgroundColor:'transparent',alignItems:'center',justifyContent:'center'}}>
          <Thumbnail source={image} />
        </View>
        <View style={{flex:6,justifyContent:'center'}}>
          <Text style={{fontWeight:'900'}}>Kumar Pratik</Text>
          <Text style={{color:'gray'}} >View profile</Text>
        </View>
        <View style={{flex:2,alignItems:'center',justifyContent:'center'}}>
        <Icon
            name="arrow-forward"
            style={{color: '#000', fontSize: height*0.035, marginLeft: 10}}
          />
        </View>
      </View>
    );
  }

  renderUpComingSessionGridRow() {
    return (
      <View
        style={{
          width: width * 1,
          borderBottomColor: '#e0e0e0',
          borderBottomWidth: 1,
          backgroundColor: 'white',
          //borderRadius: 8,
          //marginBottom: 15,
          alignSelf: 'center',
          justifyContent: 'center',
          flexDirection: 'row',
         // marginBottom: 10,
          height: height * 0.15,
        }}>
        <View
          style={{
            flex: 6.5,
            backgroundColor: 'transparent',
            padding: 20,
            paddingRight: 0,
            justifyContent: 'center',
          }}>
          <Text style={{fontSize: 15, marginTop: 1}}>
            Complete the Enlighten Exercise befire next session
          </Text>
        </View>
        <View
          style={{
            flex: 3.5,
            backgroundColor: 'transparent',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            onPress={() => Actions.sessionDetail()}
            style={{
              width: width * 0.23,
              height: height * 0.05,
              borderRadius: 2,
              borderWidth: 1,
              borderColor: '#c4c0c0',
              marginTop: 5,
              backgroundColor: 'transparent',
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
            }}>
            <Text style={{color: 'gray', fontSize: 14, fontWeight: 'bold'}}>
              {'JOIN'}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  onPressLoginButton() {}
  swapButtonAndSpinner() {
    return this.state.isloading ? (
      <Spinner />
    ) : (
      <TouchableOpacity
        style={{
          width: width * 0.5,
          height: height * 0.08,
          borderWidth: 0,
          marginTop: 20,
          borderRadius: 3,
          justifyContent: 'center',
          alignItems: 'center',
          alignSelf: 'center',
          backgroundColor: '#4ab865',
        }}
        onPress={() => this.onPressLoginButton()}>
        <Text style={{color: '#fff'}}>Logout</Text>
      </TouchableOpacity>
    );
  }

  renderButton(buttonText)
  {
    return <View style={{width:width*1,backgroundColor:'#fff',borderTopWidth:0.5,borderTopColor:'gray'}}>
      <View style={{padding:10}}>

      </View>
      <View style={{padding:20}}>
        <Text style={{fontSize:14,color:'gray',fontWeight:'normal'}}>
          Please note that you are required to give at least 48-hours notice if you have to cancel your session
        </Text>
      </View>
      <View style={{flexDirection:'row',justifyContent:'space-evenly',marginBottom:20}}>
      <TouchableOpacity onPress={() => Actions.scheduleSession()}
    style={{
      height: height * 0.058,
      width: width*0.43,
      borderRadius:3,
      borderWidth:1,
      backgroundColor: '#fff',
      alignItems: 'center',
      borderColor:'gray',
      justifyContent: 'center',
      alignSelf:'center'
    }}>
    <Text style={{  color:'gray',
      fontSize:15}}>{"CANCEL"}</Text>
  </TouchableOpacity>
  <TouchableOpacity onPress={() => Actions.scheduleSession()}
    style={{
      width: width*0.43,
      height: height * 0.058,
      borderRadius:3,
      backgroundColor: '#4ab865',
      alignItems: 'center',
      justifyContent: 'center',
      alignSelf:'center'
    }}>
    <Text style={{  color:'#fff',
      fontSize:15}}>{"ADD TO CALL"}</Text>
  </TouchableOpacity>
      </View>
    </View>
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#f9f9f9', alignItems: 'center'}}>
        <StatusBar backgroundColor={'#4db79b'} barStyle="light-content" />
        <CustomHeaderWithText leftIcon={true} text="SESSION DETAIL" />
        <ScrollView scrollEnabled>
          <View
            style={{
             // backgroundColor: '#f9f9f9',
              width: width * 1,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: 'transparent',
              flex: 1,
            }}>
         {/* session name area */}
            <View
              style={{
                borderBottomWidth: 0,
               
                marginTop: 20,
                
              backgroundColor: '#fff',
              flexDirection: 'row',
              }}>
              <View
                style={{
                  flex: 6.5,
                  backgroundColor: 'transparent',
                  justifyContent: 'center',
                  paddingLeft:20
                }}>
                <Text style={{fontSize: 22, fontWeight: 'bold'}}>
                  Chemistry Coaching Session
                </Text>
              </View>

              <View
                style={{
                  flex: 3.5,
                  backgroundColor: 'transparent',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              />
            </View>

            {this.renderSessiontimimgRow()}

<View style={{height:height*0.15,borderBottomWidth:1,alignItems:'stretch',justifyContent:'center',backgroundColor:'#fff',borderBottomColor: '#e0e0e0'}}>

  {this.ReturnROw(1)}

</View>


{/* Activities title area */}
            <View
              style={{
                marginTop: 20,
                borderWidth: 0,
                flexDirection: 'row',
                justifyContent: 'center',
              }}>
             
              <View
                style={{
                  padding:20,
                  paddingTop:5,
                  margin: 10,
                  backgroundColor: 'transparent',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{fontSize: 22, fontWeight: 'bold'}}>Activities you set</Text>
              </View>
             
            </View>

            {this.renderUpComingSessionGridRow()}
            {this.renderUpComingSessionGridRow()}
            {this.renderUpComingSessionGridRow()}
          
          </View>
        </ScrollView>
       {this.renderButton("DONE")}
      </View>
    );
  }
}

export default SessionDetail;

function chnagepage() {
  Actions.login();
}
