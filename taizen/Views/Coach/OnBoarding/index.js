import React, {Component} from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  TouchableOpacity,
  StatusBar,
  ScrollView,
} from 'react-native';
import axios from 'axios';
import {CustomHeaderWithText}  from '../../CommonViews';
import styles from '../../../Assets/Css/style';
import {Actions} from 'react-native-router-flux';
import {Button, CheckBox, Spinner} from 'native-base';
import {connect} from 'react-redux';
import firebase from 'firebase';
import {loginUser, UpdateUserFormData} from '../../../actions';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
const image1 = require('../../../Assets/google-calendar.png');
const image2 = require('../../../Assets/outlook.png');
const image3 = require('../../../Assets/icloud.png');
class OnBoarding extends Component {
  constructor(props) {
    super(props);
    // ...
    this.state = {fetchingSession: false,
    fromProfile: this.props.fromProfile ? this.props.fromProfile : false  
    };
  }

  componentDidMount() {
    console.log('user on index page',this.props.user);
    // Initialize Firebase
    // setTimeout(chnagepage, 2000);
    //chnagepage();
    debugger;
    if (this.props.token) {
      debugger;
      this.sendApicall(this.props.token);
    }
  }




  updateUserDataOnFirebase(workCalendar,me) {

    const {user} = this.props;
    const {id} = user;
    console.log('id is', id);
    var db = firebase.firestore();
    firebase.firestore().settings({experimentalForceLongPolling: true});
    var washingtonRef = db
      .collection('users')
      .doc(id);

    // Set the "capital" field of the city 'DC'
    return washingtonRef
      .update({
        workCalendar:workCalendar,
      })
      .then(function () {
        debugger;
        me.setState({fetchingSession: false});
        Actions.reset('coachDashboard');
        console.log('Document successfully updated!');
      })
      .catch(function (error) {
        // The document probably doesn't exist.
        debugger;
        me.setState({fetchingSession: false});
        console.error('Error updating document: ', error);
      });
  }

  hdnleUpdate() {
    debugger;
  }

  sendApicall(code) {
    var me = this;
    //code = 'xK96A40wHm5x5KxSkKOYcXip22GJQg';
    var url = `https://api.kloudless.com/v1/oauth/token?
grant_type=authorization_code&redirect_uri=https://
www.taizen.io&client_id=TKwl_gQLDRdLiyx2uiaWgzoeexmbVPsLaJ54jmvqJCa6YMqi
&client_secret=Pv5iKF1K4EtwfDW3H92g3cwI5uwiPXdKvoBgs606GhLjnoVP&code=${code}`;
    debugger;

    this.setState({fetchingSession: true});
    let apiconfig = {
      headers: {
        'Content-Type': 'application/json;',
      },
    };

    axios
      .post(url, null, apiconfig)
      .then((response) => {
        debugger;
        const baseModel = response.data;
        //me.setState({fetchingSession: false});
        if (response.status == 200) {
          //var workCalendar = {accessToken:'rXFhs35L8trlviIoTWV3BT945090A0',accountId:'408081468'};
          var workCalendar = {
            accessToken: baseModel.access_token,
            accountId: String(baseModel.account_id),
          };
          me.updateUserDataOnFirebase(workCalendar,me);
        }
        console.log('response:', response);
      })
      .catch((error) => {
        debugger;
        me.setState({fetchingSession: false});
        var err = String(error) == '' ? 'no error ' : String(error);
        console.log('error', error);
      });
  }

  renderButton(onpressButton) {
    return (
      <TouchableOpacity
        style={{
          width: width * 0.23,
          height: height * 0.045,
          borderWidth: 1,
          borderRadius: 5,
          margin: 20,

          borderColor: 'gray',
          marginRight: width * 0.1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#fff',
          alignSelf: 'flex-end',
        }}
        onPress={onpressButton}>
        <Text>SKIP</Text>
      </TouchableOpacity>
    );
  }

  render() {
    const {user} = this.props;
    const {fromProfile} = this.state;
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center',marginTop:0}}>
        {/* <StatusBar backgroundColor={'#fff'} /> */}
        <StatusBar translucent backgroundColor="transparent" barStyle="dark-content"  />
       
       {fromProfile ?  <CustomHeaderWithText headerStyle={{height:height*0.15}} leftIcon={true} text="Sync Working Calender"/> : <View/>}
        {this.state.fetchingSession ? (
          <View
            style={{
              width: width * 1,
              height: height * 1,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: 'gray',
              opacity: 0.3,
              position: 'absolute',
              zIndex: 0,
            }}>
            <Spinner color={'red'} style={{height: 60, width: 60}} />
            <Text style={{color: 'red', fontWeight: 'bold'}}>
              Fetching Session!
            </Text>
          </View>
        ) : (
          <View />
        )}

        <View style={{height: height * 0.08}}></View>
       
       
       {fromProfile ? <View/> : <View
          style={{
            width: width * 0.5,
            backgroundColor: 'transparent',
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
          }}>
          <Text
            style={{
              fontSize: height * 0.04,
              fontFamily: 'Ariel',
              fontWeight: 'bold',
              textAlign: 'center',
            }}>
            Welcome To Taizen, {user.firstName ? user.firstName : ''}!
          </Text>
        </View>
       }
       
        <View
          style={{
            width: width * 0.84,
            backgroundColor: 'transparent',
            alignItems: 'center',
            justifyContent: 'center',
            marginBottom: 20,
            marginTop: 20,
            padding: 10,
          }}>
          <Text style={{textAlign: 'center', fontSize: fromProfile ? 15:12}}>
          {fromProfile ? "Sync Your working calender" :"Lets start with syncing your working calender to cut down the heck" }
          </Text>
        </View>

        <View
          style={{
            height: height * 0.45,
            backgroundColor: 'transparent',
            width: width * 0.95,
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <TouchableOpacity
            onPress={() => Actions.browser({calenderType: 'outlook_calendar'})}
            style={{alignItems: 'center', backgroundColor: 'transparent'}}>
            <Image
              source={image2}
              style={{height: height * 0.08, width: height * 0.08}}
            />
            <Text style={{fontSize: 13, marginTop: 4}}>OutLook Calender</Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => Actions.browser({calenderType: 'google_calendar'})}
            style={{alignItems: 'center', backgroundColor: 'transparent'}}>
            <Image
              source={image1}
              style={{height: height * 0.08, width: height * 0.08}}
            />
            <Text style={{fontSize: 13, marginTop: 4}}>Google Calender</Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => Actions.browser({calenderType: 'icloud_calendar'})}
            style={{alignItems: 'center', backgroundColor: '#transparent'}}>
            <Image
              source={image3}
              style={{height: height * 0.08, width: height * 0.08}}
            />
            <Text style={{fontSize: 13, marginTop: 4}}>Icloud Calender</Text>
          </TouchableOpacity>
        </View>

        {fromProfile ? <View style={{ width: width * 0.23,
          height: height * 0.045,
          //borderWidth: 1,
          borderRadius: 5,
          margin: 20}}/> :
        this.renderButton(()=> Actions.coachDashboard())
        }
        <View
          style={{
            alignSelf: 'center',
            justifyContent: 'flex-end',
            backgroundColor: '#transparent',
            height: height * 0.1,
          }}>
          <View
            style={{
              height: 5,
              backgroundColor: 'black',
              width: width * 0.3,
              borderRadius: 5,
              marginBottom:10
            }}
          />
        </View>
      </View>
    );
  }
}

//export default OnBoarding;
const mapStateToProps = ({auth}) => {
  const {user} = auth;

  return {user};
};
export default connect(mapStateToProps, {loginUser, UpdateUserFormData})(
  OnBoarding,
);

function chnagepage() {
  Actions.login();
}
