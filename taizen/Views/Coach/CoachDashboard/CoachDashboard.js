import React, {Component} from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  Modal,
  PanResponder,
  Alert,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {
  Container,
  Header,
  Tab,
  Tabs,
  TabHeading,
  Icon,
  Thumbnail,
  Spinner,
  Button,
} from 'native-base';
import firebase from 'firebase';
import {connect} from 'react-redux';
import {CustomHeaderWithText} from '../../CommonViews';
import Animated from 'react-native-reanimated';
import CustomFooter from '../../CommonViews/CustomFooter';
import Messsages from './../../EmployeeDashboard/Messsages';
import CustomFooterCoach from '../../CommonViews/CustomFooterCoach';
import requestCameraAndAudioPermission from '../../VideoCall/permission';
import moment from 'moment';
const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

class CoachDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      AppID: '19e5b00e21dc4d1b8fcffc52e7159fa7', //Set your APPID here
      isloading:false, sessionArray:[],
      selectedTab:'SESSION'
      //ChannelName: '', //Set a default channel or leave blank
    };
    if (Platform.OS === 'android') {
      //Request required permissions from Android
      requestCameraAndAudioPermission().then(_ => {
        console.log('requested!');
      });
    }
   // this.state = {isloading:false, sessionArray:[]}
    // ...
  }


  retiriveUpComingSessions()
  {
    debugger;
    const {user} = this.props;
    const {id} = user;
    var firebaseConfig = {
      apiKey: "AIzaSyDlfb35etfsO24wlrghJ_dzpgIRSPhff94",
      authDomain: "taizen-cf195.firebaseapp.com",
      databaseURL: "https://taizen-cf195.firebaseio.com",
      projectId: "taizen-cf195",
      storageBucket: "taizen-cf195.appspot.com",
      messagingSenderId: "814929083198",
      appId: "1:814929083198:web:9465639048338ee3654a6f",
      measurementId: "G-3S2TR3Z0W1"
    };
    var me = this;
    this.setState({isloading:true});
    // Initialize Firebase
    !firebase.apps.length ? firebase.initializeApp(firebaseConfig): firebase.app();
    debugger;
    var db2 = firebase.firestore();
    firebase.firestore().settings({experimentalForceLongPolling: true});
    var result = []
    db2.collection("sessions").where("coachId", "==", id)//"1a392I0XGAWkS6TjOBvLArg9psB2")
    .get()
    .then(function(querySnapshot) {
      debugger;  
      querySnapshot.forEach(function(doc) {
          debugger;
          var rec= {sessionid:doc.id,...doc.data()};
          var sessiondate = rec.date.toDate();
          console.log('sessiondate',sessiondate);
          var sesssiondatemom = moment(sessiondate);
          var currentdate = moment(new Date());
          if(sesssiondatemom.isAfter(currentdate))
             result.push(rec)
            // doc.data() is never undefined for query doc snapshots
            ///console.log(doc.id, " => ", doc.data());
        });
        debugger;
        result.sort(function(a, b) {
          var c = new Date(a.date);
          var d = new Date(b.date);
          return c-d;
      });
me.setState({isloading:false,sessionArray:result});
    })
    .catch(function(error) {
      debugger;
      me.setState({isloading:false});
        console.log("Error getting documents: ", error);
    });

  }
  componentDidMount() {
    // Initialize Firebase
    // setTimeout(chnagepage, 2000);
    //chnagepage();
    debugger;
    this.retiriveUpComingSessions();
  }

  _renderCards(text, imagename, onpressButton) {
    debugger;
    var image = require('../../../Assets/onBoardimage1.png');
    image =
      imagename == 'onBoardimage1.png'
        ? require('../../../Assets/onBoardimage1.png')
        : imagename == 'onBoardimage2.png'
        ? require('../../../Assets/onBoardimage2.png')
        : require('../../../Assets/onBoardimage3.png');

    return (
      <View
        style={{
          height: height * 0.16,
          backgroundColor: '#4ab865',
          borderRadius: 10,
          marginBottom: 10,
          justifyContent: 'flex-end',
          overflow: 'hidden',
        }}>
        <Image
          source={image}
          style={{
            height: height * 0.16,
            width: width * 0.95,
            resizeMode: 'cover',
          }}
        />

        <View
          style={{
            position: 'absolute',
            height: height * 0.16,
            backgroundColor: 'transparent',
            flexDirection: 'column',
            justifyContent: 'flex-end',
            alignItems: 'center',
            width: width * 0.95,
            bottom: height * 0.16 * 0.2,
          }}>
          <Text style={{color: 'white', fontSize: 23, fontWeight: '900'}}>
            Add new session
          </Text>
        </View>
      </View>
    );
  }

  renderActionsCards() {
    return (
      <View
        style={{
          flexDirection: 'row',
          height: height * 0.35,
          borderWidth: 0,
          margin: 10,
          borderRadius: 10,
        }}>
        <View
          style={{
            backgroundColor: 'white',
            borderRadius: 10,
            padding: 15,
            width: width * 0.5,
            overflow: 'hidden',
          }}>
          <View
            style={{
              flex: 7,
              borderWidth: 0,
              backgroundColor: '',
              backgroundColor: '#4db79b',
              borderRadius: 10,
            }}>
            <View style={{flex: 5}} />
            <View style={{flex: 5, padding: 5, paddingLeft: 15}}>
              <Text style={{fontSize: 16, color: 'white', fontWeight: '900'}}>
                Practice daily medicaton
              </Text>
            </View>
          </View>
          <View style={{flex: 3.1, borderWidth: 0, padding: 10, marginTop: 10}}>
            <Text style={{fontSize: 16, fontWeight: '900'}}>7:30 am</Text>
            <Text style={{fontSize: 15, color: 'gray'}}>Repeat daily</Text>
          </View>
        </View>
      </View>
    );
  }

  renderUpComingSessionGridRow() {
    return   this.state.isloading ? <Spinner/> :
      this.state.sessionArray.length < 1 ? <Text style={{margin:10, textAlign:'center'}}>No upcoming session</Text>:
      this.state.sessionArray.map(el=>
        <TouchableOpacity
        onPress={()=> Actions.sessionDetail({selectedSession:el,sessionArray:this.state.sessionArray})}
        key={el.sessionid}
        style={{
          width: width * 0.95,
          borderColor: '#e0e0e0',
          borderWidth: 1,
          backgroundColor: 'white',
          borderRadius: 8,
          marginBottom: 15,
          alignSelf: 'center',
          justifyContent: 'center',
          flexDirection: 'row',
          marginBottom: 10,
        }}>
        <View
          style={{
            flex: 6.5,
            backgroundColor: 'transparent',
            padding: 20,
          }}>
          <Text style={{fontSize: 14, marginTop: 1}}>{moment(el.date.toDate()).format('dddd DD MMM yyyy')}</Text>
          <Text style={{fontSize: 14, marginTop: 1}}>
            {`${el.timeWindow} w/${el.employeeName}`}
          </Text>
          <Text style={{fontSize: 14, marginBottom: 0, marginTop: 1}}>
            30 min
          </Text>
        </View>
        <View
          style={{
            flex: 3.5,
            backgroundColor: 'transparent',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            onPress={() =>  Actions.video({AppID:this.state.AppID, ChannelName:el.channelName})}
            style={{
              width: width * 0.23,
              height: height * 0.05,
              borderRadius: 5,
              borderWidth: 1,
              borderColor: '#c4c0c0',
              marginTop: 5,
              backgroundColor: 'transparent',
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
            }}>
            <Text style={{color: 'gray', fontSize: 14}}>
              {'JOIN'}
            </Text>
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
        )
   
  
  }

renderCustomTab()
{
  return <View style={{marginTop: 0, borderWidth: 0}}>
  <Image
    source={require('../../../Assets/Rectangle.png')}
    style={{
      height: height * 0.15,
      width: width * 1,
      resizeMode: 'cover',
    }}
  />
  <View
    style={{
      position: 'absolute',
      width: width * 1,
      borderWidth: 0,
      justifyContent: 'space-evenly',
      alignItems: 'center',
      borderWidth: 0,
      borderColor: 'red',
    }}>
    {/* <CustomHeaderWithText  /> */}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor:'tranaprent',
        }}>
        {this.renderCoachHeaderImage('SESSION')}
        {this.renderCoachHeaderImage('MESSAGES')}
      </View>
    
  </View>
</View>
}

renderCoachHeaderImage(el) {
  return el ? (
    <TouchableOpacity
      onPress={() => this.setState({selectedTab: el})}
      style={{
        flex: 5,
        padding: 5,
        height:height*0.15,
        borderBottomWidth:el == this.state.selectedTab ?4:0,
        borderBottomColor:'#fff',
        alignItems: 'center',
        justifyContent: 'center',
      }}>
     <Text style={{color:'#fff',fontSize:16}}>{el}</Text>
    </TouchableOpacity>
  ) : (
    <View style={{flex: 5}} />
  );
}

swapViews()
{
return this.state.selectedTab=='SESSION' ? this.renderHomeView():
<View style={{flex:1}}>
  <Messsages/>
</View>

}

renderHomeView()
{
return <ScrollView scrollEnabled showsVerticalScrollIndicator={false}>
<View style={{flex: 1}}>
  <View style={{margin: 20}}>
    {/* <Text style={{fontSize: 17, marginTop: 10}}>
      48 Total Hours
    </Text>
    <Text style={{fontSize: 17, marginTop: 5}}>
      30 minutes remaining this month
    </Text> */}
  </View>

  <View
    style={{
      marginTop: 20,
      borderWidth: 0,
      flexDirection: 'row',
      justifyContent: 'center',
    }}>
    <View style={{flex: 3.3, backgroundColor: 'transparent'}} />
    <View
      style={{
        flex: 3.3,
        margin: 10,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
    {this.state.sessionArray.length < 1 ? <View/> : 
     <Text style={{fontSize: 18, fontWeight: '900'}}>
        Upcoming
      </Text>
      }
    </View>
    <View style={{flex: 3.3}} />
  </View>
  {this.renderUpComingSessionGridRow()}
  <View></View>
</View>
</ScrollView>
}
  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#f9f9f9'}}>
        <StatusBar translucent backgroundColor={'transparent'} barStyle="light-content" />
{this.renderCustomTab()}
        <View style={{backgroundColor: '#f9f9f9', flex: 1,marginTop:10}}>
      {this.swapViews()}
        </View>

        <CustomFooterCoach />
      </View>
    );
  }
}

//export default CoachDashboard;
const mapStateToProps = ({auth}) => {
  const {user} = auth;

  return {user};
};
export default connect(mapStateToProps, null)(CoachDashboard);

function chnagepage() {
  Actions.login();
}
