import React, {Component} from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  Modal,
  PanResponder,
} from 'react-native';
import firebase from 'firebase';
import {connect} from 'react-redux';
import moment from 'moment';
import {Actions} from 'react-native-router-flux';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail,Icon, Spinner } from 'native-base';
import {CustomHeaderWithText} from '../../CommonViews';
import Animated from 'react-native-reanimated';
import CustomFooter from '../../CommonViews/CustomFooter';
import Messsages from '../../EmployeeDashboard/Messsages';
import CustomFooterCoach from '../../CommonViews/CustomFooterCoach';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
const image = require('../../../Assets/coach.png');
class Clients extends Component {
  constructor(props) {
    super(props);
    // ...
    this.state = {
      isloading:false,
      AppID: '19e5b00e21dc4d1b8fcffc52e7159fa7',
      selectedSession:this.props.selectedSession,
       sessionArray:this.props.sessionArray,
       selectedClient:null,
       isFetchingClientData:true,
    };
  }
  componentDidMount() {
    // Initialize Firebase
    // setTimeout(chnagepage, 2000);
    //chnagepage();
    this.getClientData()
  }

  getClientData = ()=> {
    const {selectedSession} = this.state;
    var me = this;
    me.setState({isFetchingClientData:true});
    debugger;
    var db = firebase.firestore();
    firebase.firestore().settings({experimentalForceLongPolling: true});
    var docRef = db.collection('users').doc(selectedSession.employeeId);
  
    docRef
      .get()
      .then(function (doc) {
        debugger;
  
        if (doc.exists) {
          me.setState({isFetchingClientData:false,selectedClient:doc.data()});
        } else {
          me.setState({isFetchingClientData:false});
        }
      })
      .catch(function (error) {
        debugger;
        me.setState({isFetchingClientData:false});
        console.log('Error getting document:', error);
        
      });
  }

  renderUpComingSessionGridRow() {
    return   this.state.isloading ? <Spinner/> :
      this.state.sessionArray.length < 1 ? <Text style={{margin:10, textAlign:'center'}}>No upcoming session</Text>:
      this.state.sessionArray.map(el=>
        <View
        key={el.sessionid}
        style={{
          width: width * 0.95,
          borderColor: '#e0e0e0',
          borderWidth: 1,
          backgroundColor: 'white',
          borderRadius: 8,
          marginBottom: 15,
          alignSelf: 'center',
          justifyContent: 'center',
          flexDirection: 'row',
          marginBottom: 10,
        }}>
        <View
          style={{
            flex: 6.5,
            backgroundColor: 'transparent',
            padding: 20,
          }}>
          <Text style={{fontSize: 14, marginTop: 1}}>{moment(el.date.toDate()).format('dddd DD MMM yyyy')}</Text>
          <Text style={{fontSize: 14, marginTop: 1}}>
            {`${el.timeWindow} w/${el.employeeName}`}
          </Text>
          <Text style={{fontSize: 14, marginBottom: 0, marginTop: 1}}>
            30 min
          </Text>
        </View>
        <View
          style={{
            flex: 3.5,
            backgroundColor: 'transparent',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            onPress={() =>  Actions.video({AppID:this.state.AppID, ChannelName:el.channelName})}
            style={{
              width: width * 0.23,
              height: height * 0.05,
              borderRadius: 5,
              borderWidth: 1,
              borderColor: '#c4c0c0',
              marginTop: 5,
              backgroundColor: 'transparent',
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
            }}>
            <Text style={{color: 'gray', fontSize: 14}}>
              {'JOIN'}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
        )
   
  
  }


  renderUpcomingTextRow()
  {
    return   <View
    style={{
      marginTop: 20,
      borderWidth: 0,
      flexDirection: 'row',
      justifyContent: 'center',
    }}>
    <View style={{flex: 3.3, backgroundColor: 'transparent'}} />
    <View
      style={{
        flex: 3.3,
        margin: 10,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Text style={{fontSize: 18, fontWeight: '900'}}>{'Upcoming'.toUpperCase()}</Text>
    </View>
    <View style={{flex: 3.3}}>
      <TouchableOpacity
        onPress={() => Actions.allSessions()}
        style={{
          width: width * 0.23,
          height: height * 0.05,
          borderRadius: 5,
          borderWidth: 1,
          borderColor: '#c4c0c0',
          marginTop: 5,
          backgroundColor: 'transparent',
          alignItems: 'center',
          justifyContent: 'center',
          alignSelf: 'center',
        }}>
        <Text
          style={{color: 'gray', fontSize: 14}}>
          {'VIEW ALL'}
        </Text>
      </TouchableOpacity>
    </View>
  </View>
  }
 renderAvtivities()
 {
 return <View>
         <View
              style={{
                marginTop: 20,
                borderWidth: 0,
                flexDirection: 'row',
                justifyContent: 'center',
              }}>
              <View style={{flex: 2, backgroundColor: 'transparent'}} />
              <View
                style={{
                  flex: 6,
                  margin: 10,
                  backgroundColor: 'transparent',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{fontSize: 18, fontWeight: '900'}}>
                  {'Activities you set'.toUpperCase()}
                </Text>
              </View>
              <View style={{flex: 2}}/>
            </View>

            <View
              style={{
                width: width * 0.95,
                borderBottomColor: '#c4c0c0',
                borderBottomWidth: 1,
                backgroundColor: 'white',
                borderRadius: 6,
                marginTop: 10,
                alignSelf: 'center',
                justifyContent: 'center',
                flexDirection: 'row',
              }}>
              <View
                style={{
                  flex: 4.7,
                  backgroundColor: 'transparent',
                  padding: 20,
                }}>
                <Text style={{fontSize: 14, marginTop: 1}}>
                  Complete the Enlighten Excercise before next session
                </Text>
              </View>
              <View style={{flex: 1.8, backgroundColor: 'transparent'}} />
              <View
                style={{
                  flex: 3.5,
                  backgroundColor: 'transparent',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  onPress={() => this.setState({modalVisible: true})}
                  style={{
                    width: width * 0.23,
                    height: height * 0.05,
                    borderRadius: 5,
                    borderWidth: 1,
                    borderColor: '#c4c0c0',
                    marginTop: 5,
                    backgroundColor: 'transparent',
                    alignItems: 'center',
                    justifyContent: 'center',
                    alignSelf: 'center',
                  }}>
                  <Text
                    style={{color: 'gray', fontSize: 14}}>
                    {'JOIN'}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
 </View>

 }

  ReturnROw(el) {
    debugger;
    //let Image_Http_URL = {uri: el.picture};
    return  this.state.isFetchingClientData ? <Spinner style={{alignSelf:'center'}}/> : 
      <TouchableOpacity
      onPress={()=> Actions.clientDetail({selectedClient:el})}
      key={el.id} avatar  style={{height:height*0.13,marginLeft:0,backgroundColor:'transparent',borderWidth:0,flexDirection:'row',width:width*1,marginTop:0,
      borderBottomWidth:1,borderBottomColor:'#e9e9e9'}}>
        <View style={{flex:2,backgroundColor:'transparent',alignItems:'center',justifyContent:'center'}}>
          <Thumbnail source={{uri: el.picture}} />
        </View>
        <View style={{flex:6,justifyContent:'center'}}>
    <Text style={{fontWeight:'900'}}>{`${el.firstName.toUpperCase()} ${el.lastName.toUpperCase()}`}</Text>
          <Text style={{color:'gray'}} >View profile</Text>
        </View>
        <View style={{flex:2,alignItems:'center',justifyContent:'center'}}>
        <Icon
            name="arrow-forward"
            style={{color: '#000', fontSize: height*0.035, marginLeft: 10}}
          />
        </View>
      </TouchableOpacity>
    
  }

  renderSessionInfo(el)
  {
    return <View
style={{
  flex: 6.5,
  backgroundColor: 'transparent',
  padding: 20,
}}>
<Text style={{fontSize:18,fontWeight:'900',width:width*0.6,marginBottom:10}}>Chemistry Coaching Session</Text>
<Text style={{fontSize: 14, marginTop: 1}}>{moment(el.date.toDate()).format('dddd DD MMM yyyy')}</Text>
<Text style={{fontSize: 14, marginTop: 1}}>
  {`${el.timeWindow} w/${el.employeeName}`}
</Text>
<Text style={{fontSize: 14, marginBottom: 0, marginTop: 1}}>
  30 min
</Text>
</View>
  }

  render() {
    const {selectedClient,selectedSession} = this.state;
    return (
      <View style={{flex: 1, backgroundColor: '#f9f9f9', alignItems: 'center'}}>
        <StatusBar translucent backgroundColor={'transparent'} barStyle="light-content" />
        <CustomHeaderWithText leftIcon={true} text={`SESSION DETAIL`} />
       <ScrollView scrollEnabled showsVerticalScrollIndicator={false}>
        <View style={{backgroundColor: '#f9f9f9',
        width:width*1,
        justifyContent:'center',alignItems:'stretch',backgroundColor:"transparent",flex:1}}>

            {this.renderSessionInfo(selectedSession)}
       
          <View style={{borderBottomWidth:1,alignItems:'stretch',justifyContent:'center',backgroundColor:'#fff',borderBottomColor: '#e0e0e0'}}>

           {this.ReturnROw(selectedClient)}
                     
          </View>

{this.renderAvtivities()}
        </View>
</ScrollView>
        <CustomFooterCoach />
      </View>
    );
  }
}

//export default Clients;
const mapStateToProps = ({auth}) => {
  const {user} = auth;

  return {user};
};
export default connect(mapStateToProps, null)(Clients);
function chnagepage() {
  Actions.login();
}
