import React, {Component} from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  Modal,
  PanResponder,
} from 'react-native';
import firebase from 'firebase';
import {connect} from 'react-redux';
import moment from 'moment';
import {Actions} from 'react-native-router-flux';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail,Icon, Spinner } from 'native-base';
import {CustomHeaderWithText} from '../../CommonViews';
import Animated from 'react-native-reanimated';
import CustomFooter from '../../CommonViews/CustomFooter';
import Messsages from '../../EmployeeDashboard/Messsages';
import CustomFooterCoach from '../../CommonViews/CustomFooterCoach';
import SessionDetail from './SessionDetail';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
const image = require('../../../Assets/coach.png');
class Clients extends Component {
  constructor(props) {
    super(props);
    // ...
    this.state = {
      isloading:false,
      AppID: '19e5b00e21dc4d1b8fcffc52e7159fa7',
       sessionArray:[],
       selectedClient:this.props.selectedClient,
       chatDetail:''
    };
  }
  componentDidMount() {
    // Initialize Firebase
    // setTimeout(chnagepage, 2000);
    //chnagepage();
    this.retiriveUpComingSessions()
  }
  retriveChatData(me,sessionArray)
  {
    debugger;
    const {user} = me.props;
    const {selectedClient} = me.props;
    const {id} = user;
    var firebaseConfig = {
      apiKey: "AIzaSyDlfb35etfsO24wlrghJ_dzpgIRSPhff94",
      authDomain: "taizen-cf195.firebaseapp.com",
      databaseURL: "https://taizen-cf195.firebaseio.com",
      projectId: "taizen-cf195",
      storageBucket: "taizen-cf195.appspot.com",
      messagingSenderId: "814929083198",
      appId: "1:814929083198:web:9465639048338ee3654a6f",
      measurementId: "G-3S2TR3Z0W1"
    };
    var me = this;
    this.setState({isloading:true});
    // Initialize Firebase
    !firebase.apps.length ? firebase.initializeApp(firebaseConfig): firebase.app();
    debugger;
    var db2 = firebase.firestore();
    firebase.firestore().settings({experimentalForceLongPolling: true});
    var result = []
    db2.collection("chats").where("users", "array-contains", id)//"1a392I0XGAWkS6TjOBvLArg9psB2")
    .onSnapshot(function(querySnapshot) {
      var result = []
         querySnapshot.forEach(function(doc) {
          debugger;
          var rec= {chatModelid:doc.id,...doc.data()};
          if(rec.users.includes(selectedClient.id))
             result.push(rec)
        });
        debugger;
        //Actions.chat({chatDetail:el,reciverDetail:reciverDetail})
         me.setState({isloading:false,chatDetail:result.length>0?result[0]:'',sessionArray:sessionArray});
         //me.props.UpdateUserFormData({prop: 'upComingSessions', value: result});
}, function(error) {
    me.setState({isloading:false});
    console.log("Error getting documents: ", error);
  });  
  }



  retiriveUpComingSessions()
  {
    debugger;
    const {selectedClient} = this.props;
    const {user} = this.props;
    let coachId = user.id;
    const {id} = selectedClient;
    var firebaseConfig = {
      apiKey: "AIzaSyDlfb35etfsO24wlrghJ_dzpgIRSPhff94",
      authDomain: "taizen-cf195.firebaseapp.com",
      databaseURL: "https://taizen-cf195.firebaseio.com",
      projectId: "taizen-cf195",
      storageBucket: "taizen-cf195.appspot.com",
      messagingSenderId: "814929083198",
      appId: "1:814929083198:web:9465639048338ee3654a6f",
      measurementId: "G-3S2TR3Z0W1"
    };
    var me = this;
    this.setState({isloading:true});
    // Initialize Firebase
    !firebase.apps.length ? firebase.initializeApp(firebaseConfig): firebase.app();
    debugger;
    var db2 = firebase.firestore();
    firebase.firestore().settings({experimentalForceLongPolling: true});
    var result = []
    db2.collection("sessions").where("employeeId", "==", id)//"1a392I0XGAWkS6TjOBvLArg9psB2")
    .get()
    .then(function(querySnapshot) {
      debugger;  
      querySnapshot.forEach(function(doc) {
          debugger;
          var rec= {sessionid:doc.id,...doc.data()};
          var sessiondate = rec.date.toDate();
          console.log('sessiondate',sessiondate);
          var sesssiondatemom = moment(sessiondate);
          var currentdate = moment(new Date());
          if(sesssiondatemom.isAfter(currentdate) && rec.coachId == coachId)
          {
            result.push(rec)
          }
            // doc.data() is never undefined for query doc snapshots
            ///console.log(doc.id, " => ", doc.data());
        });
        debugger;
        result.sort(function(a, b) {
          var c = new Date(a.date);
          var d = new Date(b.date);
          return c-d;
      });
      me.setState({isloading:false,sessionArray:result});
      me.retriveChatData(me,result);
    })
    .catch(function(error) {
      debugger;
      me.setState({isloading:false});
        console.log("Error getting documents: ", error);
    });

  }

  renderUpComingSessionGridRow() {
    return   this.state.isloading ? <Spinner/> :
      this.state.sessionArray.length < 1 ? <Text style={{margin:10, textAlign:'center'}}>No upcoming session</Text>:
      this.state.sessionArray.slice(0,1).map(el=>
        <View
        key={el.sessionid}
        style={{
          width: width * 0.95,
          borderColor: '#e0e0e0',
          borderWidth: 1,
          backgroundColor: 'white',
          borderRadius: 8,
          marginBottom: 15,
          alignSelf: 'center',
          justifyContent: 'center',
          flexDirection: 'row',
          marginBottom: 10,
        }}>
        <View
          style={{
            flex: 6.5,
            backgroundColor: 'transparent',
            padding: 20,
          }}>
          <Text style={{fontSize: 14, marginTop: 1}}>{moment(el.date.toDate()).format('dddd DD MMM yyyy')}</Text>
          <Text style={{fontSize: 14, marginTop: 1}}>
            {`${el.timeWindow} w/${el.employeeName}`}
          </Text>
          <Text style={{fontSize: 14, marginBottom: 0, marginTop: 1}}>
            30 min
          </Text>
        </View>
        <View
          style={{
            flex: 3.5,
            backgroundColor: 'transparent',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            onPress={() =>  Actions.video({AppID:this.state.AppID, ChannelName:el.channelName})}
            style={{
              width: width * 0.23,
              height: height * 0.05,
              borderRadius: 5,
              borderWidth: 1,
              borderColor: '#c4c0c0',
              marginTop: 5,
              backgroundColor: 'transparent',
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
            }}>
            <Text style={{color: 'gray', fontSize: 14}}>
              {'JOIN'}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
        )
   
  
  }


  renderUpcomingTextRow()
  {
    return   <View
    style={{
      marginTop: 20,
      borderWidth: 0,
      flexDirection: 'row',
      justifyContent: 'center',
    }}>
    <View style={{flex: 3.3, backgroundColor: 'transparent'}} />
    <View
      style={{
        flex: 3.3,
        margin: 10,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Text style={{fontSize: 18, fontWeight: '900'}}>{'Upcoming'.toUpperCase()}</Text>
    </View>
    <View style={{flex: 3.3}}>
      <TouchableOpacity
        onPress={() => Actions.allSessions({sessionArray:this.state.sessionArray})}
        style={{
          width: width * 0.23,
          height: height * 0.05,
          borderRadius: 5,
          borderWidth: 1,
          borderColor: '#c4c0c0',
          marginTop: 5,
          backgroundColor: 'transparent',
          alignItems: 'center',
          justifyContent: 'center',
          alignSelf: 'center',
        }}>
        <Text
          style={{color: 'gray', fontSize: 14}}>
          {'VIEW ALL'}
        </Text>
      </TouchableOpacity>
    </View>
  </View>
  }

handleMessageButton()
{
  const {selectedClient} = this.props;
  const {chatDetail} = this.state;
Actions.chat({chatDetail:chatDetail,reciverDetail:selectedClient})
}

  ReturnROw(el) {
    let Image_Http_URL = {uri: el.picture};
    return (
      <View key={el.id} avatar  style={{height:height*0.16,marginLeft:0,backgroundColor:'transparent',borderWidth:0,flexDirection:'row',width:width*1,marginTop:0,
      borderBottomWidth:0,borderBottomColor:'#e9e9e9'}}>
        <View style={{flex:3.3,backgroundColor:'transparent',alignItems:'center',justifyContent:'center'}}>
          <Thumbnail large source={Image_Http_URL} />
        </View>

<View style={{flex:3.7}}/>
        <View style={{flex:3,alignItems:'center',justifyContent:'center'}}>
      {this.state.isloading ? <View/> :  <TouchableOpacity
            onPress={() => this.handleMessageButton() }
            style={{
              width: width * 0.26,
              height: height * 0.05,
              borderRadius: 5,
              borderWidth: 1,
              //padding:5,
              borderColor: '#c4c0c0',
              marginTop: 5,
              backgroundColor: 'transparent',
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
            }}>
            <Text style={{color: 'gray', fontSize: 14}}>
              {'MESSAGE'}
            </Text>
          </TouchableOpacity>
          }
        </View>
      </View>
    );
  }

  render() {
    const {selectedClient} = this.props;
    return (
      <View style={{flex: 1, backgroundColor: '#f9f9f9', alignItems: 'center'}}>
        <StatusBar translucent backgroundColor={'transparent'} barStyle="light-content" />
        <CustomHeaderWithText leftIcon={true} text={`${selectedClient.firstName.toUpperCase()} ${selectedClient.lastName.toUpperCase()}`} />
       <ScrollView scrollEnabled showsVerticalScrollIndicator={false}>
        <View style={{backgroundColor: '#f9f9f9',
        width:width*1,
        justifyContent:'center',alignItems:'stretch',backgroundColor:"transparent",flex:1}}>

       
          <View style={{borderBottomWidth:1,alignItems:'stretch',justifyContent:'center',backgroundColor:'#fff',borderBottomColor: '#e0e0e0'}}>

           {this.ReturnROw(selectedClient)}
                     
          </View>
<View style={{margin:10,padding:10}}>
  <Text style={{fontSize:18,fontWeight:'900'}}>ABOUT</Text>
  <Text style={{fontSize:14,color:'gray'}}>Position: COO</Text>
  <Text style={{fontSize:14,color:'gray'}}>Company: Apple Inc</Text>
  <Text style={{fontSize:14,color:'gray'}}>Country: UK</Text>
</View>
          {this.renderUpcomingTextRow()}
          {this.renderUpComingSessionGridRow()}
        </View>
</ScrollView>
        <CustomFooterCoach />
      </View>
    );
  }
}

//export default Clients;
const mapStateToProps = ({auth}) => {
  const {user} = auth;

  return {user};
};
export default connect(mapStateToProps, null)(Clients);
function chnagepage() {
  Actions.login();
}
