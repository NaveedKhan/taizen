import React, {Component} from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  Modal,
  PanResponder,
} from 'react-native';
import firebase from 'firebase';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail,Icon, Spinner } from 'native-base';
import {CustomHeaderWithText} from '../../CommonViews';
import Animated from 'react-native-reanimated';
import CustomFooter from '../../CommonViews/CustomFooter';
import Messsages from './../../EmployeeDashboard/Messsages';
import CustomFooterCoach from '../../CommonViews/CustomFooterCoach';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
const image = require('../../../Assets/coach.png');
class Clients extends Component {
  constructor(props) {
    super(props);
    // ...
    this.state = {
      isloading:false,
       clientArray:[]
    };
  }
  componentDidMount() {
    // Initialize Firebase
    // setTimeout(chnagepage, 2000);
    //chnagepage();
    this.retiriveUpComingSessions()
  }

  retiriveUpComingSessions()
  {
    debugger;
    const {user} = this.props;
    const {id} = user;
    var firebaseConfig = {
      apiKey: "AIzaSyDlfb35etfsO24wlrghJ_dzpgIRSPhff94",
      authDomain: "taizen-cf195.firebaseapp.com",
      databaseURL: "https://taizen-cf195.firebaseio.com",
      projectId: "taizen-cf195",
      storageBucket: "taizen-cf195.appspot.com",
      messagingSenderId: "814929083198",
      appId: "1:814929083198:web:9465639048338ee3654a6f",
      measurementId: "G-3S2TR3Z0W1"
    };
    var me = this;
    this.setState({isloading:true});
    // Initialize Firebase
    !firebase.apps.length ? firebase.initializeApp(firebaseConfig): firebase.app();
    debugger;
    var db2 = firebase.firestore();
    firebase.firestore().settings({experimentalForceLongPolling: true});
    var result = []
    db2.collection("users").where("assignedCoachId", "==", id)//"1a392I0XGAWkS6TjOBvLArg9psB2")
    .get()
    .then(function(querySnapshot) {
      debugger;  
      querySnapshot.forEach(function(doc) {
          debugger;
          var rec= doc.data();
          result.push(rec)
            // doc.data() is never undefined for query doc snapshots
            console.log(doc.id, " => ", doc.data());
        });
        debugger;
        me.setState({isloading:false,clientArray:result});
    })
    .catch(function(error) {
      debugger;
      me.setState({isloading:false});
        console.log("Error getting documents: ", error);
    });

  }

  ReturnROw(el) {
    let Image_Http_URL = {uri: el.picture};
    return (
      <TouchableOpacity
      onPress={()=> Actions.clientDetail({selectedClient:el})}
      key={el.id} avatar  style={{height:height*0.13,marginLeft:0,backgroundColor:'transparent',borderWidth:0,flexDirection:'row',width:width*1,marginTop:0,
      borderBottomWidth:1,borderBottomColor:'#e9e9e9'}}>
        <View style={{flex:2,backgroundColor:'transparent',alignItems:'center',justifyContent:'center'}}>
          <Thumbnail source={Image_Http_URL} />
        </View>
        <View style={{flex:6,justifyContent:'center'}}>
    <Text style={{fontWeight:'900'}}>{`${el.firstName} ${el.lastName}`}</Text>
          <Text style={{color:'gray'}} >View profile</Text>
        </View>
        <View style={{flex:2,alignItems:'center',justifyContent:'center'}}>
        <Icon
            name="arrow-forward"
            style={{color: '#000', fontSize: height*0.035, marginLeft: 10}}
          />
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#f9f9f9', alignItems: 'center'}}>
        <StatusBar translucent backgroundColor={'transparent'} barStyle="light-content" />
        <CustomHeaderWithText text="CLIENTS" />
       <ScrollView scrollEnabled showsVerticalScrollIndicator={false}>
        <View style={{backgroundColor: '#f9f9f9',
        width:width*1,
        justifyContent:'center',alignItems:'stretch',backgroundColor:"transparent",flex:1}}>

       
          <View style={{borderBottomWidth:1,alignItems:'stretch',justifyContent:'center',backgroundColor:'#fff',borderBottomColor: '#e0e0e0'}}>
{this.state.isloading ? <Spinner/> :
this.state.clientArray.length < 1 ? <Text style={{margin:10,textAlign:'center'}}>No client added yet!</Text> :
this.state.clientArray.map(el=>
            this.ReturnROw(el)
            )

}
         

</View>
        </View>
</ScrollView>
        <CustomFooterCoach />
      </View>
    );
  }
}

//export default Clients;
const mapStateToProps = ({auth}) => {
  const {user} = auth;

  return {user};
};
export default connect(mapStateToProps, null)(Clients);
function chnagepage() {
  Actions.login();
}
