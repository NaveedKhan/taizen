const React = require('react-native');
const {Platform, Dimensions} = React;

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default {
  drawerCover: {
    alignSelf: 'stretch',
    height: deviceHeight / 3.5,
    width: null,
    position: 'relative',
    marginBottom: 10,
    resizeMode: 'contain',
    backgroundColor: '#4ab865',
  },
  drawerImage: {
    position: 'absolute',
    left: Platform.OS === 'android' ? deviceWidth / 10 : deviceWidth / 9,
    top: Platform.OS === 'android' ? deviceHeight / 13 : deviceHeight / 12,
    width: 210,
    height: 75,
    resizeMode: 'cover',
  },
  text: {
    fontWeight: Platform.OS === 'ios' ? '500' : '400',
    fontSize: 16,
    marginLeft: 20,
  },
  badgeText: {
    fontSize: Platform.OS === 'ios' ? 13 : 11,
    fontWeight: '400',
    textAlign: 'center',
    marginTop: Platform.OS === 'android' ? -3 : undefined,
  },
  container: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: '#F5FCFF',
    flexDirection: 'column',
  },
  buttonText: {
    fontSize: 14,
    color: '#fff',
    //fontWeight: 'bold',
  },
  button: {
    width: deviceWidth * 0.7,
    height: 50,
    borderRadius: 0,
   // bottom: deviceHeight * 0.09,
    margin: 10,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    borderColor: '#fff',
    borderWidth: 1.2,
    position: 'absolute'
  },
  blackLine: {
    backgroundColor: '#000',
    height: 6,
    //bottom: 5,
    width: deviceWidth * 0.4,
    alignSelf: 'center',
    borderRadius: 10,
  },
  backgroundImage: {
    width: deviceWidth * 1,
    height: deviceHeight * 1,
  },
};
