import { combineReducers }  from 'redux';
import AuthReducer from './AuthReducer';
import FooterReducer from './FooterReducer';

const INITIAL_STATE = {
  current: [],
  possible: [
    'Allie',
    'Gator',
    'Lizzie',
    'Reptar',
  ],
};

const friendReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    default:
      return state
  }
};


export default combineReducers({
  auth: AuthReducer,
  footer:FooterReducer
});
