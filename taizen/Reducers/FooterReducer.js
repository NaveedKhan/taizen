import {
 FOOTER_DETAIL_UPDATE
} from '../actions/types';
const INITIAL_STATE = {
  employeeSelectedTab:'HOME',coachSelectedTab:'CoachDashboard'};
//christie@gmail.com
export default (state = INITIAL_STATE, action) => {
   //debugger;
  switch (action.type) {
    case FOOTER_DETAIL_UPDATE:
      return { ...state, [action.payload.prop]: action.payload.value }
    default:
      return state;
  }
}

//return {( ...state, ...INITIAL_STATE,)}
