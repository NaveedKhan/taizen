import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER_SUCCESSFUL,
  LOGIN_USER,
  LOGIN_USER_FAIL,
  LOGOUT_USER,
  LOGOUT_USER_FAIL,
  LOGOUT_USER_SUCCESSFUL,
  USER_DETAIL_UPDATE,
  USER_PASSWORD_CHANGED,
  USER_SIGNUP,
  USER_SIGNUP_FAILED,
  USER_SIGNUP_SUCCESSFULE,
} from '../actions/types';
const INITIAL_STATE = {
  username: '',
  password: '',
  firstName: '',
  lastName: '',
  jobTitle: '',
  newPassword: '',
  oldPassword: '',
  savedPassword:'',
  selectedCoach: null,
  upComingSessions: [],
  user: null,
  error: '',
  loading: false,
  DeviceTocken: '',
  DeviceID: '',
};
//christie@gmail.com
export default (state = INITIAL_STATE, action) => {
  //debugger;
  switch (action.type) {
    case USER_DETAIL_UPDATE:
      return {...state, [action.payload.prop]: action.payload.value};
    case EMAIL_CHANGED:
      return {...state, email: action.payload};
    case PASSWORD_CHANGED:
      return {...state, password: action.payload};
    case USER_PASSWORD_CHANGED:
      return {...state, oldPassword: '', loading: false};
    case LOGIN_USER_SUCCESSFUL:
    case USER_SIGNUP_SUCCESSFULE:
      debugger;
      console.log('payload', action.payload);
      return {
        ...state,
        user: action.payload.data,
        password: '',//action.payload.signinModel.Password,
        username: '',//action.payload.signinModel.UserName,
        loading: false,
        error: '',
        firstName:'',
        lastName:'',
        jobTitle:''
      };
    case LOGIN_USER_FAIL:
      // debugger;
      return {...state, error: action.payload, loading: false};
    case LOGIN_USER:
    case USER_SIGNUP:
    case LOGOUT_USER:
      return {...state, loading: true, error: ''};
    case LOGOUT_USER_FAIL:
    case USER_SIGNUP_FAILED:
    return {...state, loading: false, error: 'Error occured'};
    case LOGOUT_USER_SUCCESSFUL:
      debugger;
      return {
        ...state,
        user: null,
        password: '',//action.payload.signinModel.Password,
        username: '',//action.payload.signinModel.UserName,
        loading: false,
        error: '',
        firstName:'',
        lastName:'',
        jobTitle:''
      };
    default:
      return state;
  }
};

//return {( ...state, ...INITIAL_STATE,)}
