export const EMAIL_CHANGED = 'email_changed';
export const PASSWORD_CHANGED = 'password_changed';
export const LOGIN_USER_SUCCESSFUL = 'login_successful';
export const LOGIN_USER_FAIL = 'login_fail';
export const LOGIN_USER = 'login_user';
export const LOGOUT_USER = 'logout_user';
export const LOGOUT_USER_SUCCESSFUL = 'logout_user_success';
export const LOGOUT_USER_FAIL = 'logout_user_fail';
export const USER_DETAIL_UPDATE = 'user_detail_update';
export const USER_PASSWORD_CHANGED = 'user_password_changed';
export const USER_SIGNUP = 'user_signup';
export const USER_SIGNUP_SUCCESSFULE = 'user_signup_successful';
export const USER_SIGNUP_FAILED = 'user_signup_failed';

export const FOOTER_DETAIL_UPDATE = 'user_detail_update';


