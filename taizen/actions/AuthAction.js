import firebase from 'firebase';
import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER_SUCCESSFUL,
  LOGIN_USER,
  LOGIN_USER_FAIL,
  LANDING_DATAFECTHED,
  LOGOUT_USER,
  LOGOUT_USER_FAIL,
  LOGOUT_USER_SUCCESSFUL,
  USER_DETAIL_UPDATE,
  USER_SIGNUP,USER_SIGNUP_FAILED,USER_SIGNUP_SUCCESSFULE
} from './types';
import {Alert} from 'react-native';
import {Actions} from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import DeviceInfo from 'react-native-device-info';
import {Toast} from 'native-base';

export const UpdateUserFormData = ({prop, value}) => {
  debugger;
  return {
    type: USER_DETAIL_UPDATE,
    payload: {prop, value},
  };
};

export const logOutUserSucess = () => {
  this.removeItemValue('userData');
  return {
    type: LOGOUT_USER_SUCCESSFUL
  };
  
  //Actions.reset('splash2');
};

export const singupUser = ({email, password,firstName,lastName,jobTitle,DeviceTocken}) => {
  //debugger;
  let me = this;
  var sampleImage='https://firebasestorage.googleapis.com/v0/b/taizen-cf195.appspot.com/o/assets%2F1600073955271-media.png?alt=media&token=71863a47-77b4-481c-9edb-59b7697b9bd0';
  const SigninModel = {UserName: email, Password: password};
  const UserModel = {jobTitle:jobTitle,firstName:firstName,lastName:lastName,email:email,picture:sampleImage,userType:'Normal'}
  let apiconfig = {
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
    },
  };
debugger;
  return dispatch => {
    dispatch({
      type: USER_SIGNUP,
    });
    firebase
    .auth()
    .createUserWithEmailAndPassword(email,password)
    .then((user) => {
      debugger;
      const userid = user.user.uid;
      UserModel.id = userid;
      userModel.deviceToken = DeviceTocken;
      this.addUserIntoDB(dispatch,UserModel,SigninModel)
    })
    .catch((error) => {
      debugger;
      console.log(error);
     // me.setState({isloading: false});
     var err = String(error) == '' ? 'no error ' : String(error);
     signUpUserFail(
      dispatch,
     err
    );
      Toast.show({
        text: 'Signup Failed!',
        buttonText: 'Okay',
        position: 'bottom',
        type: 'danger',
        duration: 4000,
      });
    });

   
  };
};

addUserIntoDB = (dispatch,userModel,signInModel) =>
{
  debugger;
  //var me  =this;
  var db = firebase.firestore();
  firebase.firestore().settings({experimentalForceLongPolling: true})
  db.collection("users").doc(userModel.id).set(userModel)
.then(function(docRef) {
  debugger;
    console.log("Document written with ID: ", docRef);
    signupUserSucess(dispatch,userModel,signInModel)
})
.catch(function(error) {
    console.error("Error adding document: ", error);
    var err = String(error) == '' ? 'no error ' : String(error);
    signUpUserFail(
      dispatch,
     err
    );
});
}

export const logOut = () => {
  let apiconfig = {
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
    },
  };

  return dispatch => {
    dispatch({
      type: LOGOUT_USER,
    });
firebase
.auth()
.signOut()
.then(function () {
  // Sign-out successful.
  logoutSuccess(dispatch)
  //me.setState({isloading: false});
 // Actions.reset('splashScreen');
})
.catch(function (error) {
 // me.setState({isloading: true});
  // An error happened
  logoutFail(dispatch,'error')
});
  };
};


updateUserDataOnFirebase = (dispatch,data,SigninModel,DeviceTocken) => {
debugger;
  console.log('id is', data.id);
  var db = firebase.firestore();
  db.settings({experimentalForceLongPolling: true});
  var washingtonRef = db
    .collection('users')
    .doc(data.id);

  // Set the "capital" field of the city 'DC'
  return washingtonRef
    .update({
      deviceToken:DeviceTocken
    })
    .then(function () {
      debugger;
      data.deviceToken = DeviceTocken;
      loginUserSucess(dispatch, data ,SigninModel);
    })
    .catch(function (error) {
      // The document probably doesn't exist.
      debugger;
      console.log('Error updating document:', error);
      loginUserFail(
        dispatch,
       'no error caught'
      );
    });
}

getUserData = (dispatch,uid,SigninModel,DeviceTocken)=> {
  debugger;
  var db = firebase.firestore();
  db.settings({experimentalForceLongPolling: true});
  var docRef = db.collection('users').doc(uid);

  docRef
    .get()
    .then(function (doc) {
      debugger;

      if (doc.exists) {
        if(doc.data().deviceToken != DeviceTocken)
        {
          this.updateUserDataOnFirebase(dispatch,doc.data(),SigninModel,DeviceTocken)
        }else
        {
          loginUserSucess(dispatch, doc.data(), SigninModel);
        }
        
      } else {
        loginUserFail(
          dispatch,
         'no error caught'
        );
      }
    })
    .catch(function (error) {
      debugger;
      // Toast.show({
      //   text: 'data fetched error!',
      //   buttonText: 'Okay',
      //   position: 'bottom',
      //   type: 'danger',
      //   duration: 4000,
      // });
      console.log('Error getting document:', error);
      loginUserFail(
        dispatch,
       'no error caught'
      );
    });
}

export const loginUser = ({email, password,DeviceTocken}) => {
  //debugger;
  let me = this;
  const SigninModel = {UserName: email, Password: password};
  let apiconfig = {
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
    },
  };
debugger;
  return dispatch => {
    dispatch({
      type: LOGIN_USER,
    });
//return;
    firebase
    .auth()
    .signInWithEmailAndPassword(email, password)
    .then((user) => {
      debugger;
      const userid = user.user.uid;
      me.getUserData(dispatch,userid,SigninModel,DeviceTocken);
    })
    .catch((error) => {
      debugger;
      console.log(error);
     // me.setState({isloading: false});
     var err = String(error) == '' ? 'no error ' : String(error);
     loginUserFail(
      dispatch,
     err
    );
      Toast.show({
        text: 'Login Failed!',
        buttonText: 'Okay',
        position: 'bottom',
        type: 'danger',
        duration: 4000,
      });
    });

   
  };
};

storeData = async (key, value) => {
  // debugger;
  try {
    await AsyncStorage.setItem(key, value);
  } catch (e) {
    // saving error
  }
};

removeItemValue = async key => {
  try {
    await AsyncStorage.removeItem(key);
    return true;
  } catch (exception) {
    return false;
  }
};

getdeviceId = async (dispatch) => {
  //Getting the Unique Id from here
  var id = DeviceInfo.getUniqueId();
  //this.setState({ deviceId: id, });

  dispatch({
    type: USER_DETAIL_UPDATE,
    payload: {prop: 'DeviceID', value: id},
  });
};


const logoutSuccess = (dispatch) => {
  debugger;

  dispatch({
    type: LOGOUT_USER_SUCCESSFUL
  });
  this.removeItemValue('userData');
  Actions.reset('splashScreen');
  
};
const logoutFail = (dispatch,error) => {
  debugger;

  dispatch({
    type: LOGOUT_USER_FAIL,
    payload: error,
  });
  this.removeItemValue('userData');
  Actions.reset('splashScreen');
  //Alert.alert('Alert!', error);
  
};



const loginUserSucess = (dispatch, data,SigninModel) => {
   debugger;
  dispatch({
    type: LOGIN_USER_SUCCESSFUL,
    payload: {data,signinModel:SigninModel},
  });
console.log('user in reducer',data);
  this.getdeviceId(dispatch);
  this.storeData('userData', JSON.stringify(SigninModel));
  //var data = doc.data();
  if (data.userType == 'Coach') {
    if (data.isNewPasswordSet) {
      if(data.workCalendar)
      {
        Actions.reset('coachDashboard');
      }else
      {
        Actions.reset('coachOnBoarding');
      }
    } else {
      Actions.reset('setNewPassword');
    }
  } else {
    if(data.assignedCoachId)
    {
    Actions.reset('employeeDashboard');
    }else
    {
      Actions.reset('onBoarding');
    }
  }
 // Actions.reset('userProfile');
};


const signupUserSucess = (dispatch, data,SigninModel) => {
  debugger;
 dispatch({
   type:USER_SIGNUP_SUCCESSFULE,
   payload: {data,signinModel:SigninModel},
 });
console.log('user in reducer',data);
 //this.getdeviceId(dispatch);
 this.storeData('userData', JSON.stringify(SigninModel));
 //var data = doc.data();
       if (data.userType == 'Coach') {
         if (data.isNewPasswordSet) {
           if(data.workCalendar)
           {
             Actions.reset('coachDashboard');
           }else
           {
             Actions.reset('coachOnBoarding');
           }
         } else {
           Actions.reset('setNewPassword');
         }
       } else {
         if(data.assignedCoachId)
         {
         Actions.reset('employeeDashboard');
         }else
         {
           Actions.reset('onBoarding');
         }
       }
// Actions.reset('userProfile');
};
const loginUserFail = (dispatch, error, fromSplashscreen) => {
  debugger;

  dispatch({
    type: LOGIN_USER_FAIL,
    payload: error,
  });
  this.removeItemValue('userData');
  Alert.alert('Alert!', error);
  
};

const signUpUserFail = (dispatch, error) => {
  debugger;

  dispatch({
    type: USER_SIGNUP_FAILED,
    payload: error,
  });
  // this.removeItemValue('userData');
  Alert.alert('Alert!', error);
  
};
