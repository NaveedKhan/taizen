import firebase from 'firebase';
import {
  FOOTER_DETAIL_UPDATE
} from './types';
import {Alert} from 'react-native';
import {Actions} from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import DeviceInfo from 'react-native-device-info';
import {Toast} from 'native-base';

export const UpdateFooterData = ({prop, value}) => {
  debugger;
  return {
    type: FOOTER_DETAIL_UPDATE,
    payload: {prop, value},
  };
};

