import React, {Component} from 'react';
import {
  Text,
  Dimensions,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
import {Provider} from 'react-redux';
import {Root} from 'native-base';
import {createStore, applyMiddleware} from 'redux';
import Reducer from './Reducers';
import ReduxThunk from 'redux-thunk';
//import firebase, {RNFirebase} from 'react-native-firebase';
import firebase from 'firebase';
import styles from './Assets/Css/style';
import Router from './Routers/Router';

// This is the default FCM Channel Name (required by Android)
const fcmChannelID = 'fcm_default_channel';
// This is the Firebase Server Key (for test purposes only - DO NOT SHARE IT!)
const firebase_server_key =
  'AAAAMnQYUGk:APA91bFUKK2iw7NJT-5_LhAS_SfcqfBqBZC4ht7Nt-S8CQdmARQ_fHwUZTNxtWxcpWnndkF3mdahkOmhInyLWweZ1pX2YTiK8A7h6UrljEmQ3HFVNrumPjrBF6CfODZm7KglArf6OtxH';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

class App extends Component {
  constructor() {
    super();
    this.state = {
      firebase_messaging_token: '',
      firebase_messaging_message: '',
      firebase_notification: '',
      firebase_send: '',
    };
  }



  componentDidMount() {
    debugger;
    configureFirebase();
  }

  render() {
    const storeInit = createStore(Reducer, {}, applyMiddleware(ReduxThunk));
    return (
      <Provider store={storeInit}>
        <Root>
          <View style={{flex: 1, backgroundColor: 'rgb(61,91,151)'}}>
            <Router />
          </View>
        </Root>
      </Provider>
    );
  }
}

export default App;

function configureFirebase() {
  debugger;
  var firebaseConfig = {
    apiKey: 'AIzaSyDlfb35etfsO24wlrghJ_dzpgIRSPhff94',
    authDomain: 'taizen-cf195.firebaseapp.com',
    databaseURL: 'https://taizen-cf195.firebaseio.com',
    projectId: 'taizen-cf195',
    storageBucket: 'taizen-cf195.appspot.com',
    messagingSenderId: '814929083198',
    appId: '1:814929083198:web:9465639048338ee3654a6f',
    measurementId: 'G-3S2TR3Z0W1',
  };
  // Initialize Firebase
  !firebase.apps.length
    ? firebase.initializeApp(firebaseConfig)
    : firebase.app();
  ///firebase.analytics();
}
