import React, { Component } from 'react';
import { Scene, Router, Stack, Actions, Drawer, ActionConst } from 'react-native-router-flux';
import SplashScreens from '../Views/SplashScreen/SplashScreen';
import Login from '../Views/Login/Login';
import Signup from '../Views/Login/Signup';
import ExploreStrengthWeakness from '../Views/ExploreStrengthWeakness';
import SetNewPassword from '../Views/Auth/SetNewPassword';
import EditProfile from '../Views/Auth/EditProfile';
import ForgetPassword from '../Views/Auth/ForgetPassword';
import ChangePassword from '../Views/Auth/ChangePassword';
import OnBoarding from '../Views/OnBoarding/OnBoarding';
import PickCoach from '../Views/PickCoachAndFirstSession/PickCoach';
import CoachingTopics from '../Views/CoachingTopics/CoachingTopics';
import CoachingTopicsDetails from '../Views/CoachingTopics/CoachingTopicsDetails';
import ScheduleSession from '../Views/PickCoachAndFirstSession/ScheduleSession';
import EmployeeDashboard from '../Views/EmployeeDashboard/EmployeeDashboard';
import Journey from '../Views/EmployeeDashboard/Journey';
import Coaching from '../Views/EmployeeDashboard/Coaching';
import Messsages from '../Views/EmployeeDashboard/Messsages';
import Chat from '../Views/EmployeeDashboard/Chat';
import AllSessions from '../Views/EmployeeDashboard/AllSessions';
import ClientProfile from '../Views/EmployeeDashboard/ClientProfile';
import EditEmployeeProfile from '../Views/EmployeeDashboard/EditEmployeeProfile';
import CoachDashboard from '../Views/Coach/CoachDashboard/CoachDashboard';
import Clients from '../Views/Coach/CoachDashboard/Clients';
import ClientDetail from '../Views/Coach/CoachDashboard/ClientDetail';
import SessionDetail from '../Views/Coach/CoachDashboard/SessionDetail';
import Profile from '../Views/Coach/CoachDashboard/Profile';
import CoachOnBoarding from '../Views/Coach/OnBoarding/';
import Browser from '../Views/Coach/OnBoarding/Browser';
import Home from '../Views/VideoCall/Home';
import Video from '../Views/VideoCall/Video';
import PushNotificationConfig from '../Views/PushNotification/App_FireBase';
import Questionier from '../Views/ExploreStrengthWeakness/Questionier';
import Questionier2 from '../Views/ExploreStrengthWeakness/Questionier2';
import FirstResult from '../Views/ExploreStrengthWeakness/FirstResult';
import ResultReport from '../Views/ExploreStrengthWeakness/ResultReport';


//router flux latest version is having an issue
//to fix this  node_modules/@react-navigation/native/lib/module/createKeyboardAwareNavigator.js
// replace currentlyFocusedField with currentlyFocusedInput
const RouterComponent = () => {
  return (
    <Router hideNavBar sceneStyle={{ paddingTop: 0, backgroundColor: '#fff' }}>
      <Stack hideNavBar key="root">
      <Scene key="splashScreen"  component={SplashScreens} initial title="" hideNavBar  swipeEnabled={false} panHandlers={null} type={ActionConst.RESET}/>
      <Scene key="login" component={Login} title="" hideNavBar />
      <Scene key="signup" component={Signup} title="" hideNavBar />
      <Scene key="PushNotificationConfig" component={PushNotificationConfig}  title="" hideNavBar />
      <Scene key="setNewPassword" component={SetNewPassword} title="" hideNavBar />
      <Scene key="editProfile" component={EditProfile} title="" hideNavBar />
      <Scene key="exploreStrengthWeakness"  component={ExploreStrengthWeakness} title="" hideNavBar />
      <Scene key="forgetPassword" component={ForgetPassword} title="" hideNavBar />
      <Scene key="changePassword" component={ChangePassword} title="" hideNavBar />
      <Scene key="onBoarding" component={OnBoarding} title="" hideNavBar />
      <Scene key="pickCoach" component={PickCoach} title="" hideNavBar />
      <Scene key="coachingTopics"  component={CoachingTopics} title="" hideNavBar />
      <Scene key="coachingTopicsDetails"  component={CoachingTopicsDetails} title="" hideNavBar />
      <Scene key="scheduleSession"  component={ScheduleSession} title="" hideNavBar />
      <Scene key="employeeDashboard"  component={EmployeeDashboard} title="" hideNavBar />
      <Scene key="editEmployeeProfile"  component={EditEmployeeProfile} title="" hideNavBar />
      <Scene key="clientProfile"  component={ClientProfile} title="" hideNavBar />
      <Scene key="coaching"  component={Coaching} title="" hideNavBar />
      <Scene key="messsages"  component={Messsages} title="" hideNavBar />
      <Scene key="chat"  component={Chat} title="" hideNavBar />
      <Scene key="allSessions"  component={AllSessions} title="" hideNavBar />
      <Scene key="coachDashboard"  component={CoachDashboard} title="" hideNavBar />
      <Scene key="clients"  component={Clients} title="" hideNavBar />
      <Scene key="clientDetail"  component={ClientDetail} title="" hideNavBar />
      <Scene key="sessionDetail"  component={SessionDetail} title="" hideNavBar />
      <Scene key="profile"  component={Profile} title="" hideNavBar />
      <Scene key="journey"  component={Journey} title="" hideNavBar />
      <Scene key="coachOnBoarding"  component={CoachOnBoarding} title="" hideNavBar />
      <Scene key="browser"  component={Browser} title="" hideNavBar />
      <Scene key="questionier"  component={Questionier} title="" hideNavBar />
      <Scene key="questionier2"  component={Questionier2} title="" hideNavBar />
      <Scene key="firstResult"  component={FirstResult} title="" hideNavBar />
      <Scene key="resultReport"  component={ResultReport} title="" hideNavBar />
      <Scene 
        key="home" 
        component={Home}
        title="Agora Video Call"
        type={ActionConst.RESET}
      />
      <Scene
        key="video"
        component={Video}
        title="Video Feed"
        hideNavBar={true}
      />

      </Stack>
    </Router>
  );
};

export default RouterComponent;
